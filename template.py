'''specify both --filename and --pubdir, or just --rss'''

import argparse,datetime,os,sys
from bs4 import BeautifulSoup as bs
from pathlib import Path


def log(*args,**kwargs):
 print(*args,**kwargs,file=sys.stderr)

MONTHS = ['','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']

def post_html():
 posts={}
 for f in Path('public/posts').glob('*html'):
  f = f.name
  if f[:4].isdigit():
   (year,month,day)=list(map(int,f[:10].split('-')))
   if year not in posts:
    posts[year]={month:{day:f}}
   else:
    if month not in posts[year]:
     posts[year][month]={day:f}
    else:
     posts[year][month][day]=f

 h=''
 current_year,*prev_years=sorted(posts.items(),reverse=True)
 h+=f'<details open><summary class="year"><h2>{current_year[0]}</h2></summary>\n'
 h+=f' <div class="container">\n'
 for m,v1 in sorted(current_year[1].items(),reverse=True):
  for d,v2 in sorted(v1.items(),reverse=True):
   title=v2[11:-5].replace('-',' ')
   h+=f'  <a class="post" href="posts/{v2}"><span>{title}</span><span>{MONTHS[m]} {d}</span></a>\n'
 h+=f' </div>\n</details>\n'
 h+='<details><summary class="year"><h2>Older</h2></summary>\n'
 h+=' <div class="container">\n'
 for y,v0 in prev_years:
  h+=f' <h3>{y}</h3>\n'
  for m,v1 in sorted(v0.items(),reverse=True):
   for d,v2 in sorted(v1.items(),reverse=True):
    title=v2[11:-5].replace('-',' ')
    h+=f'  <a class="post" href="posts/{v2}"><span>{title}</span><span>{MONTHS[m]} {d}</span></a>\n'
 h+=f' </div>\n</details>\n'
 return h


def make_title(p):
 return p.replace('.html','').replace('-',' ')


def make_canon(path):
 return f'<link rel="canonical" href="alexshroyer.com/{path}">'


def make_html(template,path,name):
 with open(path/name,'r+') as f:
  d=f.read()
  if '<!DOCTYPE html>' in d:
   log(path+name, 'already exists')
   return #assume file is already "done"
  f.seek(0)
  dated=name[:10].replace('-','').isdigit() #only add title to dated blog entries
  h1=f'<h1 class="title">{make_title(name[11:])}</h1>' if dated else ''
  h1='<h1 class="title">Alex Shroyer</h1>' if name=='index.html' else h1
  title=f'<title>{"Alex Shroyer" if name.removesuffix(".html") in "index cv".split() else ""}</title>'
  t=template.replace('<!-- CONTENT -->','<article>'+h1+d+'</article>')
  t=t.replace('<!-- TITLE -->',title)
  t=t.replace('<!-- CANON -->',make_canon(name))
  if'<!-- BLOG -->'in t:
   t=t.replace('<!-- BLOG -->',post_html())
  f.write(t)
  f.truncate()


def make_rss():
 rt = f'''
<item>
  <title><!-- TITLE --></title>
  <link><!-- LINK --></link>
  <pubDate><!-- DATE --></pubDate><!-- DESCRIPTION -->
</item>
'''
 items = []
 for p in sorted(Path('public/posts').glob('*.html')):
  words = p.stem.split('-')
  yy,mm,dd = (x[-2:] for x in words[:3])
  r = rt.replace('<!-- TITLE -->','-'.join(words[3:]))
  r = r.replace('<!-- LINK -->',f'https://alexshroyer.com/posts/{p.name}')
  r = r.replace('<!-- DATE -->',f'{dd} {MONTHS[int(mm)]} {yy} 00:00:00 EST')
  with open(p) as f: A = bs(f.read(), features='html.parser').css.select('div .abstract')
  r = r.replace('<!-- DESCRIPTION -->', '\n'+f'  <description>{"".join(A[0].strings).strip()}</description>' if A else '')
  items.append(r)

 with open('template_rss.xml') as ff, open('public/rss.xml','w') as gg:
  gg.write(ff.read().replace('<!-- ITEMS -->','\n'.join(items)))


def parse_args():
 p = argparse.ArgumentParser(description=__doc__,formatter_class=argparse.RawDescriptionHelpFormatter)
 p.add_argument('--filename', type=Path, help='name of html file to post-process')
 p.add_argument('--pubdir', type=Path, help='where to put published html')
 p.add_argument('--rss', action='store_true', help='build rss feed (after building site)')
 return p.parse_args()


if __name__ == "__main__":
 args = parse_args()

 if args.rss:
  make_rss()

 else:
  with open('template.html')as f: template=f.read()
  p = args.filename
  # folder = p.parent.name
  name = p.with_suffix('.html').name
  make_html(template,args.pubdir,name)
