#+SETUPFILE: ~/org/blog.org
#+TITLE: Static Site Generator
#+DATE: 2016-12-08
#+begin_abstract
Originally this site was built using Jekyll, specifically a preconfigured kit designed by Barry Clark called [[http://www.jekyllnow.com/][Jekyll Now]].
#+end_abstract

* {{{EMPTY}}}
I ended up rolling my own version because:

- I didn't want to have to maintain a Ruby project locally just to preview my site
- Jekyll's "Liquid templating engine" is another language to learn
- reinventing wheels can be a fun, educational experience
Currently this software is useful to me, and probably just me. But still, here's some more information about it.

* Features
One script (newpost.ijs) generates a mostly empty blog post, with the filename in the right format. Another script (makesite.ijs) builds the site as a whole:

1. concatenate the header and footer html around every page
2. create previous/next links for each blog post
3. generate a table of contents

Some to-dos for this script include generating a sitemap and atom feed.

* Usage
My workflow is very simple and fairly manual, but it's easy for me to understand.

1. Navigate to the site's repo in the terminal
2. Start an http server locally with python -m SimpleHTTPServer.
3. Create a new post with ./newpost.ijs.
4. Write a post in HTML, occasionally check the build with ./makesite.ijs.
5. Push to GitHub when done.
