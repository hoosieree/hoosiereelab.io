#+SETUPFILE: ~/org/blog.org
#+TITLE: Fantasy Fantasy Football
#+DATE: 2022-04-30
#+FILETAGS: startup-ideas

#+begin_abstract
Higher order fantastic sports.
#+end_abstract

* Football (F)
American football is a sport.
Teams have players, they compete in tournaments.
Over the course of seasons, new players arrive, some retire, and some are traded between teams.

* Fantasy Football (fF)
A game, derived from regular football, with imaginary or "fantasy" teams composed of existing real-world football players.
The players on the fantasy team may come from different real teams.
The score of the fantasy team is based on the aggregate statistics of the real-world players.

* Fantasy Fantasy Football (ffF)
A game, derived from fantasy football, wth "fantasy" teams composed of existing fantasy football players.
The score of the ffF team is based on the aggregate statistics of the fF players.

* Fantasy Fantasy Sport (ffS)
Like ffF, but without the restriction that all members of the team must eventually be traced back to real-world American Football.
Instead, ffS teams may be composed of fantasy football, fantasy basketball, fantasy (any real-world professional sport).

* Fantasy Esports (fE)
Like fF, but with teams composed of existing real-world esports players.

* Fantasy Fantasy Esports (ffE)
Like fE, but teams are composed of fE players.
Since most fantasy sports are played electronically, by definition all fantasy sports players are esports players.
This means ffF is in fact a subset of ffE.

This also means ffE extends indefinitely, allowing players to compose teams from any fantasy league.
