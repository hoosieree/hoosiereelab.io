#+title: FloatVer Hardware Acceleration
#+date: 2024 10 08
#+filetags: versioning "software engineering" "hardware acceleration" FloatVer

#+begin_abstract
A hardware-accelerated alternative to SemVer.
#+end_abstract


* Version System Hardware Acceleration
Version systems do not need hardware acceleration.
So here is a versioning system amenable to hardware acceleration.

Since FloatVer is a subset of IEEE754 32-bit floating point numbers, it is also inherently hardware-accelerated by most compute platforms (including CPU, GPU, and TPU hardware).

** Advantages of FloatVer
- hardware support
- programming language support
- serialization format support

** Disadvantages of FloatVer
- IEEE754 floats do not include all Real numbers
- languages may mask rounding errors (e.g. in Python, =0.10000000000000001= silently rounds to =0.1=)
- possible to conflate with non-version floats

** Why 32 bits?
In practice the most common sizes of floating-point values are 32 or 64 bits, however some platforms support 16-bit, 8-bit, or other sizes for their floats.
This document proposes 32 bits as a compromise between size, portability, and expressiveness.
It should be rare for any software to need more than ~2 billion versions (half of the ~4 billion distinct 32 bits values because we discard the negative numbers), and 16 bits would probably be enough for nearly every software project ever written, but float16 is less common in hardware than float32.

* Discussion
The key advantage of FloatVer is mechanical sympathy: it is natively supported by most programming languages and hardware.
On the other hand, SemVer is popular because it provides value as a [[https://v5.chriskrycho.com/elsewhere/cutting-edge-of-versioning/][communication tool]].
Given these different strengths, when would FloatVer be a better choice than SemVer?

1. when you have a huge amount of version numbers to compare and need hardware acceleration
2. when you have many different "flavors" of SemVer such that comparing two versions requires two parsers
3. for new projects that do not already have any existing ties to a particular flavor of SemVer
4. for versions starting with =0.=, where FloatVer is already a subset of SemVer

But when is SemVer a better choice than FloatVer?

1. when you already use (the same flavor of) SemVer and so do all the other sub-projects in your project
2. when FloatVer would be so controversial that you'd lose more money wasting precious engineering time in meetings debating the merits of versioning than you'd gain by its computational efficiency

How does FloatVer work as a /communication tool/?
Let's use an example to demonstrate.

Your company has a software product called Foo, which uses FloatVer and is currently version 3.14 (no relation to π).
The developers have some additional features so the next logical version could be 3.15.
However, they feel like 3.15 is a "big" difference from 3.14 and the new features are minor.
So instead the new version is 3.141, which implies that there are 9 more similarly-sized additions before upgrading to 3.15 feels appropriate.

If the same team used SemVer, they might instead make a version bump from 3.1.4 to 3.1.5 because the last number is for =PATCH= releases, which are less significant than =MINOR= releases.
Or, they might call this next version 3.1.4r1 (using the metadata I said I wouldn't talk about), indicating that this revision is even less significant of a change than a =PATCH= release.

As a communication tool, FloatVer and SemVer offer the same degrees of freedom in this scenario.
When deciding on a new backward-compatible FloatVer number, developers must balance the /length/ of the number with the /granularity/ of the change.
Incrementing the last digit with each revision (and sticking with decimal digits 0-9) means you only have 10 revisions until you must increment the next most significant digit.
But adding another digit makes the version number longer, which goes against a common preference for small, round numbers.

As a consumer of a product that uses FloatVer, what does the number tell you?
First, a large =BREAK= number implies the same thing that it does with SemVer - the product releases breaking changes frequently.
A /long/ =NOBREAK= value implies that this version of the software has experienced many incremental but backward-compatible upgrades.
A /short/ =NOBREAK= value could mean the software recently went through a significant milestone or that the software is young.

A FloatVer of =3.999001= might mean the developers /thought/ they were nearly done but then changed their minds multiple times, and eventually hedged their bets by adding two zeros.
I think this is one unexpected area FloatVer shines as a communication tool.
Usually, software version numbers only tell a story when you have two or more of them to compare.
But here, a single number is able to convey some of the history of the project and the thought processes of its developers.

In conclusion, FloatVer offers simplicity, performance, and improved usability compared to SemVer.

