#+title: FloatVer
#+date: 2024 07 01
#+filetags: FloatVer "version systems" SemVer

#+begin_abstract
Versioning software with floating point numbers.
#+end_abstract

* Software Versioning
Modern software updates frequently and uses a "versioning system" to let users know which iteration they have.
The most popular of these systems is [[https://semver.org/][SemVer]], which roughly follows a =MAJOR.MINOR.PATCH= format.
For example something like =ExampleWare 3.12.1= is the ExampleWare software with major version 3, minor version 12, and patch version 1.

While SemVer is broadly used, it's not entirely standardized and there are sometimes misunderstandings about the definitions of each of the major/minor/patch terms, as well as the notion of what a "breaking" change is.

Another common version system is [[https://calver.org/][CalVer]], which uses a date-based numbering.

* FloatVer
This document proposes FloatVer as an alternative to SemVer and CalVer.

FloatVer uses IEEE754 32-bit floating point non-negative numbers in base-10 format.
Its syntax consists of two numeric parts separated by a dot: =BREAK.NOBREAK=.

Some example FloatVer version numbers are =0.1=, =123.456=, and =1.000009=.

The definitions of =BREAK= and =NOBREAK= are:
- =BREAK= is for backward-incompatible changes
- =NOBREAK= is for backward-compatible changes
- Purely non-breaking changes *must not* modify the =BREAK= part of the number.
- Breaking changes *may* change the =NOBREAK= part of the number.

Where "backward compatible" means:
- no semantic changes to existing functionality
- an updated feature uses no more memory, time, or cores than its previous version
- the software takes up no more than 1.5 times the space of the previous version

This means larger values of =NOBREAK= are safe to apply automatically.
Conversely, a larger value of =BREAK= requires careful thought and is /never/ safe to automatically upgrade.

The scaling factor of =1.5= (a non-breaking update takes no more than 1.5x storage) is arbitrary but I wanted to choose some value between 1 and 2 since it seems pragmatic.
More important than the scaling factor itself is having a known worst-case scenario you can plan for when upgrading, and that this can be detected and accommodated automatically.

Some other clarifying details:
- leading and trailing zeros are ignored except when =BREAK= or =NOBREAK= is zero, as in examples like =0.1=, =5.0= or =0.0=
- this means =0.1= is the same version as =0.100=
- =0.0= is the first/smallest version number in FloatVer
- =340282346638528859811704183484516925440.0= is the last/largest FloatVer version
- each new version is larger than previous versions according to IEEE754 semantics

* Legitimacy
FloatVer is now included in Andrew Nesbitt's "Comprehensive List of Versioning Schemes" [[https://nesbitt.io/2024/06/24/from-zerover-to-semver-a-comprehensive-list-of-versioning-schemes-in-open-source.html#floatver][here]].
Of course, I submitted the pull request myself so maybe "legitimacy" is too strong of a word.
