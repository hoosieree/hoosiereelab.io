#+TITLE: Alex Shroyer
* News
#+begin_export html
<ol class="news">
  <li><span class="date">2024-11-04</span>New job! I joined <a href="https://cato.digital/about">Cato Digital</a> to help build more sustainable cloud computing</li>
  <li><span class="date">2024-07-03</span><a href="https://github.com/andrew">Andrew</a> <a href="https://github.com/andrew/nesbitt.io/commit/17cd6fa72cd94af3d753708b4a8bccc6a660ab73">merged</a> <a href="https://alexshroyer.com/posts/2024-07-01-FloatVer.html">FloatVer</a> into his <a href="https://nesbitt.io/2024/06/24/from-zerover-to-semver-a-comprehensive-list-of-versioning-schemes-in-open-source.html#floatver">list of versioning schemes</a></li>
  <li><span class="date">2024-03-06</span>Tensorflow merged my <a href="https://github.com/tensorflow/text/pull/1229">contribution</a></li>
  <li><span class="date">2024-02-09</span>Started a new <a href="https://github.com/hoosierEE/element">project</a> (writing an interpreter for a K-like language)</li>
  <li><span class="date">2023-11-26</span>PyTorch merged my <a href="https://github.com/pytorch/examples/pull/1198">contribution</a></li>
</ol>
#+end_export

* Research
Trustworthy computing requires finding vulnerabilities quickly.

We need tools that can spot potential problems in a rapidly-shifting security landscape. I build these kinds of tools, using deep learning to highlight possiblly vulnerable code, so security analysts can focus their attention on what matters.

* Recent Posts
#+begin_export html
<!-- BLOG -->
#+end_export

* About Me
#+begin_export html
<div class="about-section">
 <image alt="photo of me looking to the right" class="pfp" src="/images/pfp.jpeg">
 <div>
#+end_export
Some of my favorite things to do include hiking with my family, cycling in nature, woodworking with hand tools, going on road trips, and solving puzzles.

I appreciate good design in software and the real world. As a systems engineer at Indiana University I build tools for embedded systems security, manage SaaS installations, and maintain high performance clusters. I enjoy learning new programming languages and reading obscure mechanical patents.

Email: contact@<this page's url>.
#+begin_export html
 </div>
</div>
#+end_export
