* Projects
** ObfuscatedBinaryClassifiers ([[https://github.com/hoosierEE/ObfuscatedBinaryClassifiers][GitHub]])
Some of my recent Ph.D. research in reverse engineering for obfuscated binary code.

** HDL Online ([[../hdl][website]])
A basic online editor for the hardware description language called simply HDL from Nand2Tetris.
The URL updates with a permalink to the code as you edit, for ease of sharing.

[[../images/hdl.png]]

** Simple Scene Classification ([[../papers/simple-scenes-classifier.pdf][PDF]])
A machine learning project.

[[../images/pssr.png]]

A Convolutional Neural Network (CNN) to classify two types of image ("simple" versus "regular" scenes).
The images were taken by infants wearing head-mounted cameras, and researchers wanted to know if younger infants prefer "simple" scenes.
My contribution was to provide an automatic image classification system so that researchers don't have to spend so much time labeling images by hand.

** GPU Support for J ([[../papers/matmul_j_gpu.pdf][PDF]])
Adding a GPU Interfacing to a language.

[[../images/mm_only.png]]

This was a project for a class called "High Performance Computing".
It uses J's foreign function interface (FFI) to call [[https://arrayfire.org/docs/index.htm][ArrayFire]] functions.
With compatible hardware, this speeds up certain operations.
This work was a proof-of-concept intended to find a "cross over" point where the acceleration of the GPU offsets the latency of transferring data from CPU to GPU.

** Obscura ([[https://chrome.google.com/webstore/detail/obscura/nhlkgnilpmpddehjcegjpofpiiaomnen][Chrome Extension]])
This extension is a simple filter that makes web pages darker.
The reason I made this was in response to the complexity of existing solutions.

[[../images/obscura_off.png]]
[[../images/obscura_on_gray.png]]
[[../images/obscura_on_orange.png]]

** Psych-Button (USB keyboard. [[https://github.com/hoosierEE/psych-button/][source code here]])
Programmable keyboard-like user response device with sub-millisecond accuracy.

[[../images/four.png]]

A graduate student needed something with 4 "normal" buttons and a "home" button with sub-millisecond timing resolution.
The "home" button is my favorite part.
It is a screw conductively coupled to a capacitive-sensing pin on a [[http://www.pjrc.com/teensy/teensyLC.html][Teensy LC]], with a [[https://github.com/hoosierEE/aswitch/][library]] to make it behave like a pushbutton switch.

** The Skinny (embedded system)
Measures and records skin conductance levels.

[[../images/group.png]]

Myself and another engineer built 30 of these for an honors-level Psychology course at Indiana University.
The device records skin conductance between 2 electrodes and logs the data to a micro SD card.
Students devised their own experiments around the device, and presented their results at the end of the course.
Experiment topics ranged from "watch a scary movie" to "listen to your favorite music".
