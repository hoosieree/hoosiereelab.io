;;; org-publish.el --- Description -*- lexical-binding: t; -*-
(require 'package)
(setq package-user-dir (expand-file-name "./.packages")
      package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

;; Initialize the package system
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))
(package-install 'htmlize) ;; Install dependencies
(require 'ox-publish) ;; Load the publishing system

(defun my-postprocessor-fn (plist filename pub-dir)
  "custom post-processing html files in python"
  (message (shell-command-to-string
            (concat " python template.py --filename " filename " --pubdir " pub-dir))))

;; settings
(setq org-html-doctype "html5"
      org-html-validation-link nil
      org-html-head-include-scripts nil
      org-html-head-include-default-style nil
      org-html-head "<link rel=\"stylesheet\" type=\"text/css\" href=\"main.css\" />"
      org-confirm-babel-evaluate nil ;; expand macros without asking
      org-reveal-root "file:///Users/ashroyer-admin/repo/reveal.js-master"
      org-reveal-hlevel 1 ;; default is 1 (main headlines horizontal, subsections vertical)
      ;; org-babel-python-command "$HOME/miniconda3/bin/python3" ;; babel: use this python
      org-publish-list-skipped-files nil ;; don't tell me when you're not doing stuff
      ;; This macro inserts an empty string, so you can write an "empty" heading like:
      ;; * {{{empty}}}
      ;; which preserves the heading structure on export.
      org-export-global-macros '(("empty" . "src_emacs-lisp[:results raw]{\"\"}"))

      ;; the publishing bits
      org-publish-project-alist
      '(("org"
         :publishing-function (org-html-publish-to-html my-postprocessor-fn)
         ;; :publishing-function org-html-publish-to-html
         :base-directory "./content"
         :publishing-directory "./public"
         :base-extension "org"
         :body-only t
         :section-numbers nil
         :time-stamp-file nil
         :with-author nil
         :with-toc nil
         :with-date nil
         :recursive t)

        ;; FIXME: for some reason this does not transfer pdf from content/pages/cv.pdf to public/pages/cv.pdf
        ;; Why?
        ("static"
         :publishing-function org-publish-attachment
         :base-directory "./content"
         :publishing-directory "./public"
         :base-extension "css\\|js\\|svg\\|png\\|jpeg\\|jpg\\|gif\\|pdf\\|mp3\\|mp4\\|ico\\|html"
         :exclude ".*hdl/*"
         :recursive t)

        ("hdl"
         :publishing-function org-publish-attachment
         :base-directory "./content/hdl"
         :publishing-directory "./public/hdl"
         :base-extension "css\\|js\\|html"
         :recursive t
         :exclude "codemirror-5.65.0/*"
         :include ["codemirror-5.65.0/lib/codemirror.js"
                   "codemirror-5.65.0/addon/lint/lint.js"
                   "codemirror-5.65.0/lib/codemirror.css"
                   "codemirror-5.65.0/theme/nord.css"
                   "codemirror-5.65.0/theme/eclipse.css"])

        ("index"
         :publishing-function (org-html-publish-to-html my-postprocessor-fn)
         ;; :publishing-function org-html-publish-to-html
         :base-directory "./content"
         :publishing-directory "./public"
         :base-extension "org"
         :body-only t
         :section-numbers nil
         :time-stamp-file nil
         :with-author nil
         :with-toc nil
         :with-date nil
         :exclude ".*"
         :include ["index.org" "404.org"])

        ("all"
         :async t
         :components ("org" "static" "hdl" "index"))))

;; When visiting a .org file, you can publish incrementally with SPC m e P x <choose project> RET
(apply #'org-publish (cdddr command-line-args))
(print "all done!")
