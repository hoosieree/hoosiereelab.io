<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="research and hobbies of a computer engineer">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/main.css">
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <title></title>
    <link rel="canonical" href="alexshroyer.com/2024-06-21-Gradual-Types.html">
  </head>
  <body>
    <header>
      <nav>
        <a href="/index.html">Home</a>
        <a href="/pages/cv.html">CV</a>
        <a href="/pages/projects.html">Projects</a>
        <a href="/rss.xml">RSS</a>
      </nav>
    </header>
    <main>
      <article><h1 class="title">Gradual Types</h1><div class="abstract" id="org04da2c0">
<p>
Cramming static types into an already dense dynamic language.
</p>

</div>


<div id="outline-container-org6b1ba99" class="outline-2">
<h2 id="org6b1ba99">Typing Disciplines</h2>
<div class="outline-text-2" id="text-org6b1ba99">
<p>
Any value in a program has at least one type.
That type may be unimportant from the programmer's perspective, or unspecified by the programming language syntax, but it's always there.
Some languages leverage types to help you write correct or performance-optimized programs, but languages differ in how much type information the programmer must specify, and they differ in terms of <i>when</i> that type information becomes relevant (e.g. at compile time or at execution time).
</p>

<p>
Dynamically typed languages (e.g. Python, Lua, Emacs Lisp) attach type information to values at execution time.
Statically typed languages (e.g. C, Java, Haskell) either permit or require the use of type annotations in the source code, and are usually compiled before running<sup><a id="fnr.1" class="footref" href="#fn.1" role="doc-backlink">1</a></sup> so the type checking system can reject invalid programs that don't play by the rules.
This is not to say that dynamically typed languages never reject invalid types, but rather they reject them <i>later</i>, when the program runs.
</p>

<p>
Within the static/dynamic spectrum, the K family of languages are firmly on the dynamic side.
For the rest of this post, I'll explore syntax for adding type annotations to a K-like language, transforming it from an annotation-free dynamically typed language into something a little closer to the static end of the spectrum.
Adding syntax to a dynamic language in this manner is a form of "<a href="https://wphomes.soic.indiana.edu/jsiek/what-is-gradual-typing/">gradual typing</a>", defined as:
</p>

<blockquote>
<p>
[A type system] that allows parts of a program to be dynamically typed and other parts to be statically typed. The programmer controls which parts are which by either leaving out type annotations or by adding them in.
</p>
</blockquote>
</div>
</div>

<div id="outline-container-orgb1de779" class="outline-2">
<h2 id="orgb1de779">Type Annotations</h2>
<div class="outline-text-2" id="text-orgb1de779">
<p>
How do languages that already have type annotations spell them out in their syntax?
Here are some examples, sorted from "near" to "far" in terms of the distance between the type annotation and the value it describes.
</p>
</div>

<div id="outline-container-org18b166c" class="outline-3">
<h3 id="org18b166c">Near</h3>
<div class="outline-text-3" id="text-org18b166c">
<p>
In C (and C++, and Java, and&#x2026;), a type annotation usually goes before the name:
</p>
<div class="org-src-container">
<pre class="src src-C"><span style="font-weight: bold; text-decoration: underline;">int</span> <span style="font-weight: bold; font-style: italic;">x</span>;
</pre>
</div>

<p>
Array values in C are spelled as: &lt;type of each array element&gt; &lt;name&gt; &lt;square brackets&gt;, for example <code>int x[]</code> denotes an array called <code>x</code> where each element of the array is an <code>int</code> type, and whose length is unspecified.
Conversely, a declaration like <code>int x[3]</code> would specify the same array but with 3 elements.
Function declarations put the return type first:
</p>
<div class="org-src-container">
<pre class="src src-C"><span style="font-weight: bold; text-decoration: underline;">int</span> <span style="font-weight: bold;">foo</span>(<span style="font-weight: bold; text-decoration: underline;">int</span> <span style="font-weight: bold; font-style: italic;">x</span>[]);
</pre>
</div>

<p>
Go uses "near" type annotations like C, but with different ordering:
</p>
<div class="org-src-container">
<pre class="src src-Go">x int
</pre>
</div>

<p>
Go puts the return type after the parameter list:
</p>
<div class="org-src-container">
<pre class="src src-Go">func foo(x []int) int {
</pre>
</div>

<p>
Note that the array type in Go is still to the right of the name, and the order is &lt;name&gt; &lt;brackets&gt; &lt;type of each element&gt;.
</p>

<p>
In Rust, the type goes after the name (with an intervening <code>:</code>):
</p>
<div class="org-src-container">
<pre class="src src-Rust">let x: i32;
</pre>
</div>

<p>
Rust uses a different type for an array of known size versus when the size is not statically known:
</p>
<div class="org-src-container">
<pre class="src src-rust">fn foo(x: [i32; 3]) -&gt; i32;  // known size
fn foo(x: &amp;[i32]) -&gt; i32;    // unknown size, uses a "slice" which is a dynamic view of an array
</pre>
</div>
</div>
</div>

<div id="outline-container-org416dfbc" class="outline-3">
<h3 id="org416dfbc">Medium</h3>
<div class="outline-text-3" id="text-org416dfbc">
<p>
Haskell and other ML-family languages allow you to write the type of a function above its definition:
</p>
<div class="org-src-container">
<pre class="src src-Haskell">f : Int -&gt; Int
f x = 3
</pre>
</div>

<p>
Here the colon separates the name of the value from its type, and <code>X -&gt; Y</code> indicates a function that takes a parameter of type <code>X</code> and returns one of type <code>Y</code>.
This syntax makes it easier to use the type information as documentation, allowing you in some cases to ignore the details of the function definition and focus only on its types.
These kinds of languages typically also have full type <i>inference</i> which means the primary purpose of type annotations is conveying meaning to <i>people</i> reading the code, rather than to the compiler.
</p>
</div>
</div>

<div id="outline-container-org8dd2bb2" class="outline-3">
<h3 id="org8dd2bb2">Far</h3>
<div class="outline-text-3" id="text-org8dd2bb2">
<p>
Still other systems put type information in a separate file, as is the case with JSON Schemas for API endpoints:
</p>

<div class="org-src-container">
<pre class="src src-js">[
  {
    <span style="font-style: italic;">"id"</span>: 0,
    <span style="font-style: italic;">"name"</span>: <span style="font-style: italic;">""</span>,
    <span style="font-style: italic;">"description"</span>: <span style="font-style: italic;">""</span>,
    <span style="font-style: italic;">"created_at"</span>: <span style="font-style: italic;">"datetime"</span>,
    <span style="font-style: italic;">"updated_at"</span>: <span style="font-style: italic;">"datetime"</span>,
    ...
  }
]
</pre>
</div>

<p>
Not only does this arrangement place the types lexically far from the code they're supposed to be annotating, in many implementations it also defers type checking to run-time.
With "far" type annotations, checking the types may even occur on a separate machine altogether.
</p>
</div>
</div>
</div>

<div id="outline-container-org64c9645" class="outline-2">
<h2 id="org64c9645">Adding Type Annotations</h2>
<div class="outline-text-2" id="text-org64c9645">
<p>
What about adding type annotations to a language that does <i>not</i> already have them?
One approach is to change the formal grammar of the language.
This is a breaking and backwards-incompatible change, which may be inconvenient or fracture the language's users into two communities, but on the upside it means you can't accidentally mix old and new versions of the language.
</p>

<p>
Another approach is to add typing metadata outside of the language itself, either in separate files, or as special "magic" comments, or by adding pragmas, directives, or other kinds of (previously) side-effect-free statements in the code.
This approach is backwards-compatible if the old compiler ignores the new metadata.
Here are some examples:
</p>

<p>
Magic comment:
</p>
<div class="org-src-container">
<pre class="src src-k">f:{x+y}  / type(x):`i; type(y):`I
</pre>
</div>

<p>
Side-effect-free statement
</p>
<div class="org-src-container">
<pre class="src src-k">f:{"x`i"; "y`I"; x+y}
</pre>
</div>

<p>
Metadata in a separate schema file (can even be a different filetype, such as JSON):
</p>
<div class="org-src-container">
<pre class="src src-JSON">{
  "files": [
    "source.k": {
      "functions": ["f": {"args": ["x":"`i", "y":"`I"]}]
    }
  ]
}
</pre>
</div>

<p>
For the metadata approach, the source file is unchanged:
</p>
<div class="org-src-container">
<pre class="src src-k">/ filename: source.k
f:{x+y}
</pre>
</div>
</div>
</div>

<div id="outline-container-org94f8ce6" class="outline-2">
<h2 id="org94f8ce6">Type Annotations in K-like Languages</h2>
<div class="outline-text-2" id="text-org94f8ce6">
<p>
Does K need static types or type annotations?
Clearly the answer is no, as proven by plenty of folks who use K without these features.
But having them would make certain types of programming styles more natural, and could broaden the appeal to a segment of the programming world who would otherwise avoid using K for anything.
</p>
</div>

<div id="outline-container-org2c59e23" class="outline-3">
<h3 id="org2c59e23">Q</h3>
<div class="outline-text-3" id="text-org2c59e23">
<p>
A recent <a href="https://code.kx.com/q/releases/ChangesIn4.1/#type-checking">release</a> of the Q language added support for pattern matching and dynamic type checking.
Q version 4.1 already had "pattern matching" which other languages sometimes call "destructuring assignment":
</p>
<div class="org-src-container">
<pre class="src src-q">(a;b): 1 2  /assigns a to 1 and b to 2
</pre>
</div>

<p>
The new release also permits specifying types as symbols.
A pattern with incorrect types generates an error:
</p>
<div class="org-src-container">
<pre class="src src-q">q)(a:`s;b:`h):(`foo;38h)  /types match; a gets `foo, b gets 38h
q)(a:`s;b:`h):(`bar;36.5) /type mismatch: 36.5 is not type `h
'type
</pre>
</div>
</div>
</div>

<div id="outline-container-orge8e7154" class="outline-3">
<h3 id="orge8e7154">Halfkey</h3>
<div class="outline-text-3" id="text-orge8e7154">
<p>
The <a href="https://ktye.github.io/halfkey/index.html">halfkey</a> language has optional type annotations for function parameters:
</p>
<div class="org-src-container">
<pre class="src src-k">f:{[xi;yI]x+y}
</pre>
</div>

<p>
The suffix <code>i</code> indicates an integer atom, whereas the suffix <code>I</code> indicates an integer list.
This uses a restricted set of type annotations that are built in to the language, and as I understand also only works for variables named <code>x</code>, <code>y</code>, or <code>z</code> within functions.
</p>
</div>
</div>

<div id="outline-container-org101b51b" class="outline-3">
<h3 id="org101b51b">Element</h3>
<div class="outline-text-3" id="text-org101b51b">
<p>
Now it's time for some creative brainstorming.
What syntax should <a href="https://github.com/hoosierEE/element">Element</a> use for optional type annotations, and how much of the language should allow type annotations in the first place?
One way to add new semantics without adding new syntax is to find an obscure corner of the existing language's syntax which currently results in an error, and give it a new (non-error) meaning.
For the purpose of this post, I'll focus on using symbols for the type annotation, and combine them with the code in various ways.
</p>
</div>

<div id="outline-container-org58274c6" class="outline-4">
<h4 id="org58274c6">Near</h4>
<div class="outline-text-4" id="text-org58274c6">
<p>
To start, let's examine the different ways to combine names with symbols to create a "near" type annotation either with or without a colon:
</p>

<table>


<colgroup>
<col  class="org-left">

<col  class="org-left">
</colgroup>
<tbody>
<tr>
<td class="org-left">name `symbol</td>
<td class="org-left">`symbol name</td>
</tr>

<tr>
<td class="org-left">name:`symbol</td>
<td class="org-left">`symbol:name</td>
</tr>
</tbody>
</table>

<p>
We can rule out <code>name:`symbol</code> because this is how you would assign a symbol literal to a variable.
Next, <code>name `symbol</code> won't work because it could be function or dictionary application (equivalent to <code>name[`symbol]</code>), leaving the variants where the symbol occurs before the name.
Of these, <code>`symbol name</code> conflicts with the combination of strands and indexing-by-juxtaposition: <code>`a`b 1</code> indexes into the strand <code>`a`b</code> with index <code>1</code>, resulting in the expression <code>`b</code>.
</p>

<p>
The only combination not already defined by the language is <code>`symbol:name</code>.
Currently, symbol literals in both K and Element are immutable so assigning to one does not make sense.
</p>
<div class="org-src-container">
<pre class="src src-k">`sym: 42  /currently an error
</pre>
</div>

<p>
Instead, we can add a new meaning to the assignment operator so <code>&lt;symbol&gt; ":" &lt;name&gt;</code> now means "<code>&lt;name&gt;</code> has the type given by <code>&lt;symbol&gt;</code>".
According to my near/far taxonomy, this annotation syntax is "near" the value, and the placement of the type (left of the name) is similar to C.
</p>

<p>
When defining a variable, instead of a single syntax we now have three:
</p>
<div class="org-src-container">
<pre class="src src-k">f:1     /old style no annotation, type of "f" inferred from the value 1
`i:f:1  /declare f with type `i and value 1
`i:f    /declare f to have type `i without a value
</pre>
</div>

<p>
Moving on, we can annotate a function's parameters:
</p>
<div class="org-src-container">
<pre class="src src-k">f:{[`i:a;`I:b]a+b}
</pre>
</div>

<p>
Or by omitting parameter names, we can still use the default <code>x</code>, <code>y</code>, or <code>z</code> arguments to functions while giving them type annotations:
</p>
<div class="org-src-container">
<pre class="src src-k">f:{[`i;`I]x+y}
</pre>
</div>

<p>
Finally, we can also annotate the function's return type by adding a <code>&lt;type&gt; ":"</code> before the function's definition.
</p>
<div class="org-src-container">
<pre class="src src-k">f:`I:{[`i:a;`I:b] a+b}
</pre>
</div>
</div>
</div>

<div id="outline-container-orgfd8469b" class="outline-4">
<h4 id="orgfd8469b">Medium</h4>
<div class="outline-text-4" id="text-orgfd8469b">
<p>
Instead of writing each type adjacent to its value, we can write types on a separate line, perhaps as a pair of imimutable assignments (one for the type and another for the value):
</p>
<div class="org-src-container">
<pre class="src src-k">f: `i`I`I  /argument types (`i`I) followed by return type `I
f: {[a;b]a+b}
</pre>
</div>

<p>
This method is flexible and compact for functions but has lower "syntactic density" for non-function values because it repeats the <code>&lt;name&gt; ":"</code> part twice:
</p>
<div class="org-src-container">
<pre class="src src-k">a: `i
a: 42
</pre>
</div>

<p>
In addition, it is asymmetric for defining a typed mutable variable:
</p>
<div class="org-src-container">
<pre class="src src-k">a: `i
a:: 42  /a is both typed and mutable
</pre>
</div>

<p>
This asymmetry isn't necessarily a bad thing; if its appearance offends the programmer's sensibilities, we can imagine it as a subtle nudge toward using immutable variables, which is generally better.
</p>

<p>
While these examples showed the type annotation directly above the variable name definition, they could be written farther apart (e.g. in separate files) and perhaps also defined in arbitrary order, but I prefer the look of writing the type annotation directly before the expression.
</p>

<p>
A limitation of this method is it does not permit composing the <i>mutable</i> property wiht a type annotation, because in Element the <code>::</code> operator applied twice would be two normal mutable assignment operations:
</p>

<div class="org-src-container">
<pre class="src src-k">a::`i  /bind name a to symbol `i
a::42  /bind name a to number 42
</pre>
</div>
</div>
</div>

<div id="outline-container-org3ff7493" class="outline-4">
<h4 id="org3ff7493">Functions Only</h4>
<div class="outline-text-4" id="text-org3ff7493">
<p>
We could also rely more heavily on either type inference or dynamic types and restrict our type annotations to only functions, or perhaps only function input parameters:
</p>
<div class="org-src-container">
<pre class="src src-k">w:3
{[a:`i;b:`I]a+b}[w;2 3 4]
</pre>
</div>
</div>
</div>
</div>
</div>

<div id="outline-container-orgec3604a" class="outline-2">
<h2 id="orgec3604a">Summary</h2>
<div class="outline-text-2" id="text-orgec3604a">
<p>
Even in a language like K where nearly every part of the syntax already has a defined meaning, it is possible to add type annotations.
Whether such annotations are worthwhile to programmers remains to be seen.
</p>
</div>
</div>
<div id="footnotes">
<h2 class="footnotes">Footnotes: </h2>
<div id="text-footnotes">

<div class="footdef"><sup><a id="fn.1" class="footnum" href="#fnr.1" role="doc-backlink">1</a></sup> <div class="footpara" role="doc-footnote"><p class="footpara">
While static languages are often compiled, the compiled/interpreted aspect is actually independent from the static/dynamic aspect of a language, and there exist examples of (somewhat rare) compiled/dynamic languages as well as (very rare) interpreted/static languages.
</p></div></div>


</div>
</div>
</article>
    </main>

    <footer>
      <div class="license-container">
        <div class="license-item">Designed in 2024 by <a href="https://alexshroyer.com">Alex Shroyer</a></div>
        <div class="license-item"><a href="https://www.gnu.org/licenses/gpl-3.0.html">GNU GPLv3</a> (code)</div>
        <div class="license-item"><a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC-BY-4.0</a> (prose)</div>
      </div>
      <div class="social">
        <a href="https://stackoverflow.com/users/2037637/alex-shroyer" aria-label="my StackOverflow profile">
          <svg role="img" aria-hidden="true" class="svg-icon iconLogoGlyphMd native" width="32" height="37" viewBox="0 0 32 37">
            <path d="M26 33v-9h4v13H0V24h4v9h22z"></path>
            <path d="m21.5 0-2.7 2 9.9 13.3 2.7-2L21.5 0zM26 18.4 13.3 7.8l2.1-2.5 12.7 10.6-2.1 2.5zM9.1 15.2l15 7 1.4-3-15-7-1.4 3zm14 10.79.68-2.95-16.1-3.35L7 23l16.1 2.99zM23 30H7v-3h16v3z"></path>
          </svg>
        </a>
        <a href="https://github.com/hoosieree" aria-label="my GitHub profile">
          <svg role="img" height="32" aria-hidden="true" viewBox="0 0 16 16" version="1.1" width="32" data-view-component="true" class="octicon octicon-mark-github v-align-middle">
            <path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0016 8c0-4.42-3.58-8-8-8z"></path>
          </svg>
        </a>
        <a href="https://www.linkedin.com/in/alex-shroyer" aria-label="my LinkedIn profile"><svg role="img" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34" class="global-nav__logo">
            <g>
              <path d="M34,2.5v29A2.5,2.5,0,0,1,31.5,34H2.5A2.5,2.5,0,0,1,0,31.5V2.5A2.5,2.5,0,0,1,2.5,0h29A2.5,2.5,0,0,1,34,2.5ZM10,13H5V29h5Zm.45-5.5A2.88,2.88,0,0,0,7.59,4.6H7.5a2.9,2.9,0,0,0,0,5.8h0a2.88,2.88,0,0,0,2.95-2.81ZM29,19.28c0-4.81-3.06-6.68-6.1-6.68a5.7,5.7,0,0,0-5.06,2.58H17.7V13H13V29h5V20.49a3.32,3.32,0,0,1,3-3.58h.19c1.59,0,2.77,1,2.77,3.52V29h5Z" > </path>
            </g>
          </svg>
        </a>
      </div>
    </footer>
    <script>
      window.MathJax={
  tex:{ams:{multlineWidth:'85%'},tags:'ams',tagSide:'right',tagIndent:'.8em'},
  chtml:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  svg:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  output:{font:'mathjax-modern',displayOverflow:'overflow'}}
    </script>
  </body>
</html>
