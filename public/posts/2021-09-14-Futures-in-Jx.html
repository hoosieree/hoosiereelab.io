<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="research and hobbies of a computer engineer">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/main.css">
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <title></title>
    <link rel="canonical" href="alexshroyer.com/2021-09-14-Futures-in-Jx.html">
  </head>
  <body>
    <header>
      <nav>
        <a href="/index.html">Home</a>
        <a href="/pages/cv.html">CV</a>
        <a href="/pages/projects.html">Projects</a>
        <a href="/rss.xml">RSS</a>
      </nav>
    </header>
    <main>
      <article><h1 class="title">Futures in Jx</h1><div class="abstract" id="org5b62dff">
<p>
Jx is a J interpreter with built-in support for task-level parallelism.
For more information about Jx, check out <a href="https://www.monument.ai/m/parallel">this page</a>.
</p>

</div>

<div id="outline-container-org0be2296" class="outline-2">
<h2 id="org0be2296">What are Futures?</h2>
<div class="outline-text-2" id="text-org0be2296">
<p>
In programming, <i>futures</i> are used to describe results of asynchronous computations.
Basically, it's a way to program with variables even if some of those variables don't have values <i>right now</i>.
</p>

<p>
Hopefully an example will make this more clear.
</p>
<div class="org-src-container">
<pre class="src src-python"><span style="font-weight: bold; font-style: italic;">ex1</span> = web_get(<span style="font-style: italic;">"www.example.com/1"</span>)  <span style="font-weight: bold; font-style: italic;"># </span><span style="font-weight: bold; font-style: italic;">takes 2.4s</span>
<span style="font-weight: bold; font-style: italic;">ex2</span> = web_get(<span style="font-style: italic;">"www.example.com/2"</span>)  <span style="font-weight: bold; font-style: italic;"># </span><span style="font-weight: bold; font-style: italic;">takes 2.5s</span>
<span style="font-weight: bold; font-style: italic;">ex3</span> = web_get(<span style="font-style: italic;">"www.example.com/3"</span>)  <span style="font-weight: bold; font-style: italic;"># </span><span style="font-weight: bold; font-style: italic;">takes 1.9s</span>
<span style="font-weight: bold;">print</span>(ex1, ex2, ex3)
</pre>
</div>

<p>
In many programming languages (Python, JavaScript, J) a program like this would run sequentially.
That means, the first line must finish before the second line can start, and so on.
This program will take 6.8 seconds (sum of each <code>web_get</code> call).
</p>

<p>
However, if you could launch each <code>web_get</code> concurrently, the whole program could take as little as 2.5s, which is the time for the slowest of the three calls.
</p>

<p>
For a fully asynchronous program, the theoretical total time is the time taken by the slowest individual part, rather than the sum of the times for all the parts.
</p>

<p>
For practical programs, futures make it somewhat easier to combine sync+async code.
In the above example, the <code>print</code> function needs the results of all 3 <code>web_get</code> functions, so it has to wait until they're all finished.
</p>

<p>
In Python, you could express this goal with the <code>asyncio</code> library:
</p>
<div class="org-src-container">
<pre class="src src-python"><span style="font-weight: bold;">import</span> asyncio
<span style="font-weight: bold; font-style: italic;">ex1</span> = asyncio.create_task(web_get(<span style="font-style: italic;">"www.example.com/1"</span>))
<span style="font-weight: bold; font-style: italic;">ex2</span> = asyncio.create_task(web_get(<span style="font-style: italic;">"www.example.com/2"</span>))
<span style="font-weight: bold; font-style: italic;">ex3</span> = asyncio.create_task(web_get(<span style="font-style: italic;">"www.example.com/3"</span>))
<span style="font-weight: bold;">print</span>(<span style="font-weight: bold;">await</span> ex1, <span style="font-weight: bold;">await</span> ex2, <span style="font-weight: bold;">await</span> ex3)
</pre>
</div>

<p>
This example looks pretty clean, although I've omitted the definition for <code>web_get</code>.
And based on the first example, <code>web_get</code> must have been a normal (synchronous) function.
The <code>asyncio</code>-enabled code therefore requires changing the definition for <code>web_get</code> to make it asyncronous.
While this is a minor change, it is also a limitation, because <code>web_get</code> can be synchronous or asynchronous, but it can't be both within the same program.
</p>
</div>
</div>

<div id="outline-container-orgad2de1d" class="outline-2">
<h2 id="orgad2de1d">background (if you don't know J)</h2>
<div class="outline-text-2" id="text-orgad2de1d">
<p>
In J you can define a function like this:
</p>
<div class="org-src-container">
<pre class="src src-j-mode">   sum =: {{+/ y}}
   sum 2 3 4
9
</pre>
</div>

<p>
The integers function can create a matrix of shape (10, 10):
</p>
<div class="org-src-container">
<pre class="src src-j-mode">   a. 10 10
 0  1  2  3  4  5  6  7  8  9
10 11 12 13 14 15 16 17 18 19
20 21 22 23 24 25 26 27 28 29
30 31 32 33 34 35 36 37 38 39
40 41 42 43 44 45 46 47 48 49
50 51 52 53 54 55 56 57 58 59
60 61 62 63 64 65 66 67 68 69
70 71 72 73 74 75 76 77 78 79
80 81 82 83 84 85 86 87 88 89
90 91 92 93 94 95 96 97 98 99
</pre>
</div>

<p>
Since <code>sum</code> doesn't say what shape its argument should be, it must work on arrays of arbitrary dimension.
The question of "on what dimensions does this function work?" is answered in J with a concept called <i>rank</i>.
All J functions have a rank, and some apply at multiple ranks.
This means functions can be applied across different dimension of their arguments.
For example matrixes have 2 dimensions (rows and columns) so a function can be applied to either of these dimensions:
</p>
<div class="org-src-container">
<pre class="src src-j-mode">   sum i. 10 10
450 460 470 480 490 500 510 520 530 540
</pre>
</div>

<p>
If you don't specify a rank, the function applies at the largest rank possible.
In the case of a matrix, the highest rank is rank 2 (columns), so <code>sum</code> operates on the columns.
</p>

<p>
You can specify other ranks for any function (built-in or user-defined):
</p>
<div class="org-src-container">
<pre class="src src-j-mode">   sum"1 i. 10 10
45 145 245 345 445 545 645 745 845 945
</pre>
</div>

<p>
That extra <code>"1</code> means "apply sum at rank 1 (rows)", so we get an array of row-sums instead of column-sums.
</p>
</div>
</div>

<div id="outline-container-orgc7da402" class="outline-2">
<h2 id="orgc7da402">J with parallel rank</h2>
<div class="outline-text-2" id="text-orgc7da402">
<p>
Finally, we can combine our knowledge about rank with Jx's <i>parallel</i> rank to run each of those separate sum calculations on their own thread:
</p>
<div class="org-src-container">
<pre class="src src-j-mode">   128!:30(1 10)  NB. use 1 thread for primitives, and up to 10 threads for futures
   (sum .. 0"1) i. 10 10
+--+---+---+---+---+---+---+---+---+---+
|45|145|245|345|445|545|645|745|845|945|
+--+---+---+---+---+---+---+---+---+---+
</pre>
</div>
<p>
This time the results are boxed.
To access the results, use unbox (<code>&gt;</code>).
Unboxing will block execution of the main thread until the results become available (although in this case the example is tiny so the results are available immediately anyway).
</p>
<div class="org-src-container">
<pre class="src src-j-mode">   &gt;(sum .. 0"1) i. 10 10
45 145 245 345 445 545 645 745 845 945
</pre>
</div>

<p>
There you have it - task parallelism without much extra work.
</p>
</div>
</div>

<div id="outline-container-org5d42796" class="outline-2">
<h2 id="org5d42796">full source code</h2>
<div class="outline-text-2" id="text-org5d42796">
<p>
To compare, here is the original program:
</p>
<div class="org-src-container">
<pre class="src src-j-mode">sum =: {{+/ y}}
sum"1 i.10 10
</pre>
</div>

<p>
&#x2026;and here is the parallel version:
</p>
<div class="org-src-container">
<pre class="src src-j-mode">128!:30(1 10)
sum =: {{+/ y}}
(sum .. 0"1) i.10 10
</pre>
</div>
</div>
</div>
<div id="outline-container-org9c841c3" class="outline-2">
<h2 id="org9c841c3">summary</h2>
<div class="outline-text-2" id="text-org9c841c3">
<p>
Personally, I find this implementation of parallelism very friendly to use.
I've used Python's <a href="https://docs.python.org/3/library/asyncio-future.html">futures</a>, and I've used <a href="https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Asynchronous/Async_await">promises</a> in JavaScript, but they always felt awkward.
They made thinking about the overall program much harder than thinking about a blocking program.
</p>

<p>
These are my points for and against the Jx implementation:
</p>
<ul class="org-ul">
<li>(pro) no change to original function</li>
<li>(pro) can use sync or async versions of function in same program</li>
<li>(con) no "await" to explicitly show blocking semantics</li>
</ul>

<p>
More importantly, this type of parallelism works well with J's inherent data parallelism, and I look forward to faster-running programs.
</p>
</div>
</div>
</article>
    </main>

    <footer>
      <div class="license-container">
        <div class="license-item">Designed in 2024 by <a href="https://alexshroyer.com">Alex Shroyer</a></div>
        <div class="license-item"><a href="https://www.gnu.org/licenses/gpl-3.0.html">GNU GPLv3</a> (code)</div>
        <div class="license-item"><a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC-BY-4.0</a> (prose)</div>
      </div>
      <div class="social">
        <a href="https://stackoverflow.com/users/2037637/alex-shroyer" aria-label="my StackOverflow profile">
          <svg role="img" aria-hidden="true" class="svg-icon iconLogoGlyphMd native" width="32" height="37" viewBox="0 0 32 37">
            <path d="M26 33v-9h4v13H0V24h4v9h22z"></path>
            <path d="m21.5 0-2.7 2 9.9 13.3 2.7-2L21.5 0zM26 18.4 13.3 7.8l2.1-2.5 12.7 10.6-2.1 2.5zM9.1 15.2l15 7 1.4-3-15-7-1.4 3zm14 10.79.68-2.95-16.1-3.35L7 23l16.1 2.99zM23 30H7v-3h16v3z"></path>
          </svg>
        </a>
        <a href="https://github.com/hoosieree" aria-label="my GitHub profile">
          <svg role="img" height="32" aria-hidden="true" viewBox="0 0 16 16" version="1.1" width="32" data-view-component="true" class="octicon octicon-mark-github v-align-middle">
            <path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0016 8c0-4.42-3.58-8-8-8z"></path>
          </svg>
        </a>
        <a href="https://www.linkedin.com/in/alex-shroyer" aria-label="my LinkedIn profile"><svg role="img" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34" class="global-nav__logo">
            <g>
              <path d="M34,2.5v29A2.5,2.5,0,0,1,31.5,34H2.5A2.5,2.5,0,0,1,0,31.5V2.5A2.5,2.5,0,0,1,2.5,0h29A2.5,2.5,0,0,1,34,2.5ZM10,13H5V29h5Zm.45-5.5A2.88,2.88,0,0,0,7.59,4.6H7.5a2.9,2.9,0,0,0,0,5.8h0a2.88,2.88,0,0,0,2.95-2.81ZM29,19.28c0-4.81-3.06-6.68-6.1-6.68a5.7,5.7,0,0,0-5.06,2.58H17.7V13H13V29h5V20.49a3.32,3.32,0,0,1,3-3.58h.19c1.59,0,2.77,1,2.77,3.52V29h5Z" > </path>
            </g>
          </svg>
        </a>
      </div>
    </footer>
    <script>
      window.MathJax={
  tex:{ams:{multlineWidth:'85%'},tags:'ams',tagSide:'right',tagIndent:'.8em'},
  chtml:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  svg:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  output:{font:'mathjax-modern',displayOverflow:'overflow'}}
    </script>
  </body>
</html>
