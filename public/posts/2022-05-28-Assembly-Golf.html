<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="research and hobbies of a computer engineer">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/main.css">
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <title></title>
    <link rel="canonical" href="alexshroyer.com/2022-05-28-Assembly-Golf.html">
  </head>
  <body>
    <header>
      <nav>
        <a href="/index.html">Home</a>
        <a href="/pages/cv.html">CV</a>
        <a href="/pages/projects.html">Projects</a>
        <a href="/rss.xml">RSS</a>
      </nav>
    </header>
    <main>
      <article><h1 class="title">Assembly Golf</h1><div class="abstract" id="org2664b92">
<p>
Trying to write the smallest assembler possible.
</p>

</div>

<div id="outline-container-org79592d2" class="outline-2">
<h2 id="org79592d2">Hack Assembler</h2>
<div class="outline-text-2" id="text-org79592d2">
<p>
The excellent Nand2Tetris curriculum includes a project in which the student writes an assembler to transform Hack assembly language into Hack machine code.
The Hack assembly language is minimal, with just two instruction types: A and C.
</p>
</div>
</div>

<div id="outline-container-orgcd066e7" class="outline-2">
<h2 id="orgcd066e7"><code>A</code> Instruction</h2>
<div class="outline-text-2" id="text-orgcd066e7">
<p>
The Hack A instruction loads a value into the A register, funnily enough.
This is the only way to load literal values into the system.
But the A register also doubles as the address register for both data (in RAM) and instructions (in ROM), so when you put a value like <code>24</code> in the A register, subsequent instructions can treat this as either RAM[24] or ROM[24] (or both, but that would be very unusual).
</p>

<p>
The syntax looks like this:
</p>

<div class="org-src-container">
<pre class="src src-asm">@24 <span style="font-weight: bold; font-style: italic;">// </span><span style="font-weight: bold; font-style: italic;">load a literal value</span>
@foo <span style="font-weight: bold; font-style: italic;">// </span><span style="font-weight: bold; font-style: italic;">load the value associated with a symbol</span>
</pre>
</div>

<p>
If loading a symbolic value, it must resolve to either a label (which denotes a ROM address) or an address in RAM.
</p>

<p>
A instructions are translated into a 16-bit binary number where the most significant bit is 0.
So if <code>foo</code> happens to be a variable located at RAM[17], then it would be equivalent to write <code>@17</code>.
</p>

<p>
After being translated to "binary", the program above would end up looking like this:
</p>

<pre class="example">
0000000000011000
0000000000010001
</pre>

<p>
NOTE - it's not actual "binary", but a text representation with '0' and '1' characters, to make it easier to visually debug.
This is an educational artifact, so the space and speed of turning it into true binary data is not needed.
</p>
</div>
</div>

<div id="outline-container-orgc569426" class="outline-2">
<h2 id="orgc569426"><code>C</code> Instruction</h2>
<div class="outline-text-2" id="text-orgc569426">
<p>
The C instruction is simultaneously more complex and more constrained than the A instruction.
It's more complex because instead of a single <code>@</code> followed by a value, there are multiple parts:
</p>

<pre class="example">
dest = comp ; jump
</pre>


<p>
And in addition to these multiple parts, the <code>dest=</code> and <code>;jump</code> parts are optional.
</p>

<p>
However, it's also more constrained than an A instruction, because every possible value for each of the parts is known ahead of time, and can be looked up in a table.
</p>

<p>
For example, destinations are one of: <code>A</code>, <code>D</code>, or <code>M</code>, or a combination of up to all 3.
Jumps can be one of <code>JMP JEQ JLE JGE JNE JLT JGT</code>.
</p>

<p>
There are quite a few more <code>comp</code> options, such as <code>D-M</code>, <code>D&amp;M</code>, <code>A+1</code> and others.
In total, the Hack CPU Emulator supports 28 <code>comp</code> variants, and 8 each for <code>dest</code> and <code>jump</code> (including no <code>dest</code> and no <code>jump</code> since they're optional).
</p>

<p>
This means there are a total of 28*8*8 or 1792 different C instructions possible.
By even ARM standards, that is a small instruction set.
</p>

<p>
However, this is the upper bound on the number of instructions.
The A instruction, by contrast, can support any numeric value from 0 to 32767 (0000000000000000 to 0111111111111111 in binary).
</p>
</div>
</div>

<div id="outline-container-orga4f5a37" class="outline-2">
<h2 id="orga4f5a37">Labels</h2>
<div class="outline-text-2" id="text-orga4f5a37">
<p>
It wouldn't be a very good assembly language without some form of <code>goto</code>, and the Hack assembly comes through with symbolic labels.
In source code, they look like this:
</p>
<div class="org-src-container">
<pre class="src src-asm">(LOOP)
  <span style="font-weight: bold;">D=M</span>
  @LOOP
  <span style="font-weight: bold;">0</span><span style="font-weight: bold; font-style: italic;">;</span><span style="font-weight: bold; font-style: italic;">JMP // always jump back to LOOP</span>
</pre>
</div>

<p>
The above example executes <code>D=M</code>, then loads the address of <code>LOOP</code>, which refers to the instruction number of <code>D=M</code>, then unconditionally jumps there.
</p>
</div>
</div>

<div id="outline-container-org0e48336" class="outline-2">
<h2 id="org0e48336">Built-in Constant Symobls</h2>
<div class="outline-text-2" id="text-org0e48336">
<p>
There are several names which are built-in to the language, and which can not be redefined (they are "constant" values).
These include 15 "virtual registers" <code>R0</code> through <code>R15</code>, and some others like <code>SP</code>, <code>ARG</code>, <code>SCREEN</code>, and <code>KBD</code>.
Each of these refer to a specific address in RAM.
</p>
</div>
</div>

<div id="outline-container-orga100c58" class="outline-2">
<h2 id="orga100c58">Golfing an Assembler</h2>
<div class="outline-text-2" id="text-orga100c58">
<p>
The textbook suggests building the assembler in a Java-like OOP language, but that is a terrible idea for code golf, because golfing code means making it as short as possible (like in the real golf game, lower scores are better).
</p>

<p>
However, there are some suggestions which are quite suitable in any style:
</p>
<ul class="org-ul">
<li>initialize a symbol table with the constant symbols first</li>
<li>scan through the assembly file to find labels</li>
<li>scan through again to find variables</li>
</ul>

<p>
The reason to scan twice is that you can't tell without context whether <code>@ASDF</code> is a label or a variable.
So you have to scan through the entire file.
If you see one occurrence of <code>(ASDF)</code>, then <code>ASDF</code> is a label.
Otherwise, it's a variable.
</p>

<p>
Technically, it's possible to build up a symbol table which finds <code>ASDF</code> and isn't sure yet if it's a label or variable, and then resolve at the end once you've either found a label or not.
But instead of scanning the source code twice, it instead must scan the symbol table twice.
I imagine this approach would be slightly more complex, but it's possible it could be slightly faster when translating a very large assembly file with very few symbols.
The largest Hack assembly file I have is the <b>pong</b> program provided by the authors, and my assembler chews through it in a few milliseconds, so I'm not concerned about speed.
</p>
</div>
</div>

<div id="outline-container-org2daf5a7" class="outline-2">
<h2 id="org2daf5a7">Golfing Strategies</h2>
<div class="outline-text-2" id="text-org2daf5a7">
<p>
First, I used Python3.9.12 for this challenge, rather than a more verbose language like C or Java.
I have in the past written this assembler in J, which is usually more terse than Python, but the approach I used didn't benefit very much from J's implicitly vectorized nature.
</p>
</div>

<div id="outline-container-org24f03d3" class="outline-3">
<h3 id="org24f03d3">Use Dictionaries</h3>
<div class="outline-text-3" id="text-org24f03d3">
<p>
Specifically, I use a dictionary for the symbol table.
Dicts in python can be initialized with a "dict comprehension" like this:
</p>

<div class="org-src-container">
<pre class="src src-python"><span style="font-weight: bold; font-style: italic;">D</span> = {k:v <span style="font-weight: bold;">for</span> k,v <span style="font-weight: bold;">in</span> <span style="font-weight: bold;">zip</span>(keys, values)}
</pre>
</div>

<p>
Which can be much shorter than setting key-value pairs explicitly like this, especially if you have lots of key-value pairs:
</p>
<div class="org-src-container">
<pre class="src src-python"><span style="font-weight: bold; font-style: italic;">D</span> = {<span style="font-style: italic;">'a'</span>:1, <span style="font-style: italic;">'b'</span>:2}
</pre>
</div>
</div>
</div>

<div id="outline-container-orgf426295" class="outline-3">
<h3 id="orgf426295">Use Helper Functions</h3>
<div class="outline-text-3" id="text-orgf426295">
<p>
I defined single-letter aliases for some builtins like <code>str.split</code> and also for a zip-to-dict function:
</p>

<div class="org-src-container">
<pre class="src src-python"><span style="font-weight: bold; font-style: italic;">S</span>=<span style="font-weight: bold;">str</span>.split
<span style="font-weight: bold; font-style: italic;">Z</span>=<span style="font-weight: bold;">lambda</span> <span style="font-weight: bold; font-style: italic;">x</span>,<span style="font-weight: bold; font-style: italic;">y</span>=-1:<span style="font-weight: bold;">dict</span>(<span style="font-weight: bold;">zip</span>(S(x,x[0]),<span style="font-weight: bold;">range</span>(y,120)))
</pre>
</div>
</div>
</div>

<div id="outline-container-orgfe6c640" class="outline-3">
<h3 id="orgfe6c640"><code>C</code> Instruction Encoding</h3>
<div class="outline-text-3" id="text-orgfe6c640">
<p>
When scanning the file, the <code>comp</code> parts of the C instruction are retrieved from a look-up table, so for example if you find the code <code>M+1</code> then it should translate into <code>1110111</code> according to the documentation.
Well, building a table like this is not too hard:
</p>
<div class="org-src-container">
<pre class="src src-python"><span style="font-weight: bold; font-style: italic;">lookup</span> = {
    <span style="font-style: italic;">'M+1'</span>: <span style="font-style: italic;">'1110111'</span>,
    ...
}
</pre>
</div>

<p>
However, it's very verbose.
One optimization is to notice that <code>1110111</code> in binary is the same as <code>119</code> in decimal, so you can change this lookup table to use decimal encoded numbers instead, and turn them into binary at the very end:
</p>

<div class="org-src-container">
<pre class="src src-python"><span style="font-weight: bold; font-style: italic;">lookup</span> = {
    <span style="font-style: italic;">'M+1'</span>: 119,
    ...
}
</pre>
</div>

<p>
The same idea can be applied to the <code>dest</code> and <code>jump</code> parts, but it's even simpler than for the <code>comp</code> part, because there are no "missing" entries:
</p>
<div class="org-src-container">
<pre class="src src-python"><span style="font-weight: bold; font-style: italic;">D</span>=Z(<span style="font-style: italic;">' M D MD A AM AD AMD'</span>,0)
<span style="font-weight: bold; font-style: italic;">J</span>=Z(<span style="font-style: italic;">' GT EQ GE LT NE LE MP'</span>,0)
</pre>
</div>

<p>
This produces a <code>dest</code> dictionary whose keys match every possible <code>dest</code> and whose values can be converted to binary to form the <code>dest</code> part of an instruction.
</p>
<div class="org-src-container">
<pre class="src src-python">{<span style="font-style: italic;">''</span>: 0, <span style="font-style: italic;">'M'</span>: 1, <span style="font-style: italic;">'D'</span>: 2, <span style="font-style: italic;">'MD'</span>: 3, <span style="font-style: italic;">'A'</span>: 4, <span style="font-style: italic;">'AM'</span>: 5, <span style="font-style: italic;">'AD'</span>: 6, <span style="font-style: italic;">'AMD'</span>: 7}
</pre>
</div>
</div>
</div>

<div id="outline-container-org53a1a2f" class="outline-3">
<h3 id="org53a1a2f">Miscellaneous</h3>
<div class="outline-text-3" id="text-org53a1a2f">
<ul class="org-ul">
<li>f-strings</li>
<li>1-space indent</li>
<li><code>;</code> statement separator when it helps</li>
<li><code>x:=y</code> assignment expression when it helps</li>
<li><code>4**7</code> instead of <code>16384</code> to save one byte</li>
<li>1-letter variable names</li>
<li><code>{**D1,**D2}</code> to merge dictionaries</li>
<li>no error checking</li>
<li>don't close files or use the <code>with ...</code> context manager, just exit the program to "clean up"</li>
</ul>
</div>
</div>
</div>

<div id="outline-container-orgc06d0e4" class="outline-2">
<h2 id="orgc06d0e4">Results</h2>
<div class="outline-text-2" id="text-orgc06d0e4">
<p>
I don't want to spoil the fun for any prospective learners, so I won't share my complete code here.
But I will share some information about it:
</p>
<ul class="org-ul">
<li>735 bytes</li>
<li>23 lines</li>
<li>the only library used is <code>sys</code> because <code>sys.argv[1]</code> has the name of the file to assemble</li>
<li>output is printed to console rather than written to a file</li>
<li>the longest line (used for <code>comp</code> encoding) is 165 characters</li>
<li><code>str</code> methods used: <code>split</code>, <code>strip</code>, <code>isdigit</code></li>
<li><code>print</code> and <code>range</code> both appear twice, <code>zip</code>, <code>dict</code>, and <code>open</code> each appear once</li>
<li>there are 20 assignments (including three <code>+=</code> and one <code>:=</code>)</li>
<li>1 <code>dict</code> comprehension</li>
<li>1 generator comprehension</li>
<li>2 <code>for</code> loops</li>
<li>six <code>if</code></li>
<li>one <code>elif</code></li>
</ul>
</div>
</div>
</article>
    </main>

    <footer>
      <div class="license-container">
        <div class="license-item">Designed in 2024 by <a href="https://alexshroyer.com">Alex Shroyer</a></div>
        <div class="license-item"><a href="https://www.gnu.org/licenses/gpl-3.0.html">GNU GPLv3</a> (code)</div>
        <div class="license-item"><a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC-BY-4.0</a> (prose)</div>
      </div>
      <div class="social">
        <a href="https://stackoverflow.com/users/2037637/alex-shroyer" aria-label="my StackOverflow profile">
          <svg role="img" aria-hidden="true" class="svg-icon iconLogoGlyphMd native" width="32" height="37" viewBox="0 0 32 37">
            <path d="M26 33v-9h4v13H0V24h4v9h22z"></path>
            <path d="m21.5 0-2.7 2 9.9 13.3 2.7-2L21.5 0zM26 18.4 13.3 7.8l2.1-2.5 12.7 10.6-2.1 2.5zM9.1 15.2l15 7 1.4-3-15-7-1.4 3zm14 10.79.68-2.95-16.1-3.35L7 23l16.1 2.99zM23 30H7v-3h16v3z"></path>
          </svg>
        </a>
        <a href="https://github.com/hoosieree" aria-label="my GitHub profile">
          <svg role="img" height="32" aria-hidden="true" viewBox="0 0 16 16" version="1.1" width="32" data-view-component="true" class="octicon octicon-mark-github v-align-middle">
            <path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0016 8c0-4.42-3.58-8-8-8z"></path>
          </svg>
        </a>
        <a href="https://www.linkedin.com/in/alex-shroyer" aria-label="my LinkedIn profile"><svg role="img" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34" class="global-nav__logo">
            <g>
              <path d="M34,2.5v29A2.5,2.5,0,0,1,31.5,34H2.5A2.5,2.5,0,0,1,0,31.5V2.5A2.5,2.5,0,0,1,2.5,0h29A2.5,2.5,0,0,1,34,2.5ZM10,13H5V29h5Zm.45-5.5A2.88,2.88,0,0,0,7.59,4.6H7.5a2.9,2.9,0,0,0,0,5.8h0a2.88,2.88,0,0,0,2.95-2.81ZM29,19.28c0-4.81-3.06-6.68-6.1-6.68a5.7,5.7,0,0,0-5.06,2.58H17.7V13H13V29h5V20.49a3.32,3.32,0,0,1,3-3.58h.19c1.59,0,2.77,1,2.77,3.52V29h5Z" > </path>
            </g>
          </svg>
        </a>
      </div>
    </footer>
    <script>
      window.MathJax={
  tex:{ams:{multlineWidth:'85%'},tags:'ams',tagSide:'right',tagIndent:'.8em'},
  chtml:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  svg:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  output:{font:'mathjax-modern',displayOverflow:'overflow'}}
    </script>
  </body>
</html>
