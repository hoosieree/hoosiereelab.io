<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="research and hobbies of a computer engineer">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/main.css">
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <title></title>
    <link rel="canonical" href="alexshroyer.com/2023-12-07-IU-Engineering-PhD.html">
  </head>
  <body>
    <header>
      <nav>
        <a href="/index.html">Home</a>
        <a href="/pages/cv.html">CV</a>
        <a href="/pages/projects.html">Projects</a>
        <a href="/rss.xml">RSS</a>
      </nav>
    </header>
    <main>
      <article><h1 class="title">IU Engineering PhD</h1><div class="abstract" id="orgce333b0">
<p>
Experiencing something N=1 times qualifies me to give advice, right?
Maybe not, but here are some IU-specific tips anyway.
</p>

</div>

<div id="outline-container-orgd9c165b" class="outline-2">
<h2 id="orgd9c165b">What are the official milestones?</h2>
<div class="outline-text-2" id="text-orgd9c165b">
<p>
In graduate school, there are official milestones (forms to be signed) and unofficial milestones (your committee thinks you're ready).
Indiana University engineering is no different in this regard.
Early on, because the engineering department was new, some of these forms were difficult to find.
But over time this dramatically improved.
Some of these cross-link to each other, but I still found it useful to bookmark them all for quick reference.
These are the links I shared most frequently with fellow graduate students here at IU:
</p>

<ol class="org-ol">
<li><a href="https://luddy.indiana.edu/academics/grad-programs/graduate-forms/index.html">Luddy Graduate Forms</a></li>
<li><a href="https://luddy.indiana.edu/doc/phd-milestones-guide.pdf">Ph.D. Milestones (PDF)</a></li>
<li><a href="https://graduate.indiana.edu/academics-research/graduation.html">Graduate Timeline</a></li>
<li><a href="https://graduate.indiana.edu/thesis-dissertation/deadlines.html">Dissertation Deadlines</a></li>
<li><a href="https://graduate.indiana.edu/thesis-dissertation/submission/doctoral.html">Doctoral Dissertation Submission</a></li>
</ol>
</div>
</div>

<div id="outline-container-org2f955bd" class="outline-2">
<h2 id="org2f955bd">General advice</h2>
<div class="outline-text-2" id="text-org2f955bd">
<ol class="org-ol">
<li><b>DO</b> attend the public portion of defenses given by your peers.
You can get a feel for what yours will be like by observing others by the same faculty or at least within your school.</li>
<li><b>DON'T</b> watch defense dissertations on YouTube.
Or if you do, be cautious about it.
If someone shares it on YouTube, it's probably an <i>outstanding</i> presentation by a rock star candidate, rather than a typical presentation by average candidate.
And frankly, most of us are average candidates, so comparing yourself to what's shared on social media is a recipe for imposter syndrome.</li>
<li><b>DO</b> practice public speaking as much as possible beforehand.
I want to clarify that practicing public speaking is about increasing your comfort and confidence.
This is separate from increasing your <i>skill</i>.
For that, you need feedback.
You can record yourself and then review it later on your own, or you can get feedback from your audience.
Personally, I <a href="./2023-12-05-Dissertation-Defense.html">get the best feedback</a> from audience members who are not intimately familiar with my work.</li>
<li>Pay attention to the official milestones (see above).</li>
<li>Ask your advisor about the unofficial milestones.
The unofficial milestones are part of the culture of your committee, your department, and your school.
It might be possible to satisfy all the official milestones but miss something important which might endanger your success.
One mistake I made in my research proposal was a much too ambitious timeline.
My advisor warned me that it was probably too aggressive, and he turned out to be right!
You won't know things like this unless you discuss them with your advisor and committee, so keep that dialog going.</li>
<li>Stay healthy.
For me this means regular exercise, drinking enough water, eating healthy most of the time, getting enough sleep, and having some social interaction.
When I was younger, my natural inclinations were to be much less healthy, but having an accountability partner makes it much easier to maintain these as habits.
In my case, my wife is the person who helps me stick to the habits that I <i>say</i> I want to have, even when I might <i>rather</i> do something unhealthy.
Without her, I am sure that I would be much less healthy and feel much worse.</li>
<li>Find a balance that works for you.
There are some people who can dive into research headfirst and be content never having interests or hobbies outside of that topic.
If that works for you, I think that's fine.
But to sustain my own wellbeing, I found it essential to do something completely different from time to time.
Within the high-tech world, my research topic involves machine learning and reverse engineering.
But I also enjoy learning about different programming languages (especially certain array-oriented languages), so when my brain demands taking a break from my main focus, I would do some recreational programming or puzzle solving in these other languages.
Outside of high-tech, I found a lot of enjoyment in hand tool woodworking.
The sounds, smells, and textures of carving, sawing, and planing wood into different shapes is a great way for me to de-stress.
There's also something I find uniquely satisfying about building a usable physical object with my hands.
Having a few of these kinds of non-research activities helps me recharge.</li>
</ol>
</div>
</div>

<div id="outline-container-org020043f" class="outline-2">
<h2 id="org020043f">Recommended reading</h2>
<div class="outline-text-2" id="text-org020043f">
<p>
I shared these articles frequently with my Ph.D. peers.
</p>
<ol class="org-ol">
<li><a href="https://matt.might.net/articles/advice-for-phd-thesis-proposals/">Ph.D. Proposal Advice</a> from Matt Might.
Without reading this article, I probably would have treated my research proposal more like a summary of what I had done so far, and a vague idea about what I wanted to do next.
But with this article as a framework, I was able to craft a research proposal that was concrete and specific.</li>
<li><a href="https://matt.might.net/articles/successful-phd-students/">Tips for Ph.D. Students</a> also from Matt Might.
Similar kinds of advice to the first post by the same author, but aimed at the graduate experience overall.</li>
<li><a href="https://karpathy.github.io/2016/09/07/phd/">A Survival Guide to a PhD</a> by Andrej Karpathy.
Andrej shares some outstandingly approachable content about machine learning on GitHub and YouTube.
Especially pay attention to the section about Writing Papers.
He makes a great point that while reading published papers may seem like a good way to learn how to write publish-worthy papers, if you only read what was published you end up training yourself as a binary classifier that only sees positive examples.
If you also review papers <i>before</i> they've been published, you're much more likely to see negative examples that would not be accepted.
Reviewing papers for journals or conferences is a good way to get this exposure, but don't neglect your own lab mates!
Whenever I review a draft by a peer I usually find at least 1-2 typos and awkwardly phrased sentences per page (even if they use spell and grammar checking).
Likewise, anytime someone reviews my drafts they always find something I missed.
So go beta test those papers!</li>
</ol>
</div>
</div>
</article>
    </main>

    <footer>
      <div class="license-container">
        <div class="license-item">Designed in 2024 by <a href="https://alexshroyer.com">Alex Shroyer</a></div>
        <div class="license-item"><a href="https://www.gnu.org/licenses/gpl-3.0.html">GNU GPLv3</a> (code)</div>
        <div class="license-item"><a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC-BY-4.0</a> (prose)</div>
      </div>
      <div class="social">
        <a href="https://stackoverflow.com/users/2037637/alex-shroyer" aria-label="my StackOverflow profile">
          <svg role="img" aria-hidden="true" class="svg-icon iconLogoGlyphMd native" width="32" height="37" viewBox="0 0 32 37">
            <path d="M26 33v-9h4v13H0V24h4v9h22z"></path>
            <path d="m21.5 0-2.7 2 9.9 13.3 2.7-2L21.5 0zM26 18.4 13.3 7.8l2.1-2.5 12.7 10.6-2.1 2.5zM9.1 15.2l15 7 1.4-3-15-7-1.4 3zm14 10.79.68-2.95-16.1-3.35L7 23l16.1 2.99zM23 30H7v-3h16v3z"></path>
          </svg>
        </a>
        <a href="https://github.com/hoosieree" aria-label="my GitHub profile">
          <svg role="img" height="32" aria-hidden="true" viewBox="0 0 16 16" version="1.1" width="32" data-view-component="true" class="octicon octicon-mark-github v-align-middle">
            <path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0016 8c0-4.42-3.58-8-8-8z"></path>
          </svg>
        </a>
        <a href="https://www.linkedin.com/in/alex-shroyer" aria-label="my LinkedIn profile"><svg role="img" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34" class="global-nav__logo">
            <g>
              <path d="M34,2.5v29A2.5,2.5,0,0,1,31.5,34H2.5A2.5,2.5,0,0,1,0,31.5V2.5A2.5,2.5,0,0,1,2.5,0h29A2.5,2.5,0,0,1,34,2.5ZM10,13H5V29h5Zm.45-5.5A2.88,2.88,0,0,0,7.59,4.6H7.5a2.9,2.9,0,0,0,0,5.8h0a2.88,2.88,0,0,0,2.95-2.81ZM29,19.28c0-4.81-3.06-6.68-6.1-6.68a5.7,5.7,0,0,0-5.06,2.58H17.7V13H13V29h5V20.49a3.32,3.32,0,0,1,3-3.58h.19c1.59,0,2.77,1,2.77,3.52V29h5Z" > </path>
            </g>
          </svg>
        </a>
      </div>
    </footer>
    <script>
      window.MathJax={
  tex:{ams:{multlineWidth:'85%'},tags:'ams',tagSide:'right',tagIndent:'.8em'},
  chtml:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  svg:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  output:{font:'mathjax-modern',displayOverflow:'overflow'}}
    </script>
  </body>
</html>
