<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="research and hobbies of a computer engineer">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/main.css">
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <title></title>
    <link rel="canonical" href="alexshroyer.com/2021-11-09-K-means.html">
  </head>
  <body>
    <header>
      <nav>
        <a href="/index.html">Home</a>
        <a href="/pages/cv.html">CV</a>
        <a href="/pages/projects.html">Projects</a>
        <a href="/rss.xml">RSS</a>
      </nav>
    </header>
    <main>
      <article><h1 class="title">K means</h1><div class="abstract" id="org9ac42e0">
<p>
The k-means clustering algorithm with Python and J implementations.
</p>

</div>

<div id="outline-container-org97f4227" class="outline-2">
<h2 id="org97f4227">K Means</h2>
<div class="outline-text-2" id="text-org97f4227">
<p>
The idea of this algorithm is that you have some data which can be "clustered" into groups based on some distance metric.
</p>

<ol class="org-ol">
<li>choose a number for k (how many clusters)</li>
<li>start with a random guess for the centroid of each cluster</li>
<li>using these centroids, find the points which are closest to each centroid</li>
<li>for each group of points, calculate a new centroid</li>
<li>goto 3 until convergence</li>
</ol>
</div>
</div>

<div id="outline-container-org950b2ca" class="outline-2">
<h2 id="org950b2ca">Example (1d)</h2>
<div class="outline-text-2" id="text-org950b2ca">
<ul class="org-ul">
<li>k: 3</li>
<li>initial centroid locations: 5 15 25</li>
<li>data points: 1 3 9 11 12 37 43 45 60</li>
</ul>

<p>
J solution:
</p>
<div class="org-src-container">
<pre class="src src-J">0j1":{{p(+/%#)/.~{.@/:"1|p-"0 _ y}}^:1[5 15 25[p=:1 3 9 11 12 37 43 45 60
</pre>
</div>

<p>
This Python solution is an approximate translation of the J solution.
Both take the absolute difference between (all) the data points and each individual centroid, then find the index of the nearest centroid using this difference, and find the mean all the points closest to that centroid.
This mean becomes the new centroid for that group of points.
</p>

<div class="org-src-container">
<pre class="src src-python"><span style="font-weight: bold;">import</span> numpy <span style="font-weight: bold;">as</span> np
<span style="font-weight: bold; font-style: italic;">p</span> = np.array([1,3,9,11,12,37,43,45,60])
<span style="font-weight: bold; font-style: italic;">c</span> = np.array([5,15,25])
<span style="font-weight: bold;">def</span> <span style="font-weight: bold;">foo</span>(c,p):
 <span style="font-weight: bold; font-style: italic;">m</span>=np.argmin(np.<span style="font-weight: bold;">abs</span>(p-c[:,<span style="font-weight: bold; text-decoration: underline;">None</span>]),axis=0)
 <span style="font-weight: bold; font-style: italic;">x</span>=[np.mean([p[j]<span style="font-weight: bold;">for</span> j <span style="font-weight: bold;">in</span> <span style="font-weight: bold;">range</span>(<span style="font-weight: bold;">len</span>(p))<span style="font-weight: bold;">if</span> m[j]==i])<span style="font-weight: bold;">for</span> i <span style="font-weight: bold;">in</span> <span style="font-weight: bold;">range</span>(<span style="font-weight: bold;">len</span>(c))]
 <span style="font-weight: bold;">return</span> np.array(x)
<span style="font-weight: bold;">print</span>(np.<span style="font-weight: bold;">round</span>(foo(c,p),1))
</pre>
</div>
</div>
</div>

<div id="outline-container-orged2081f" class="outline-2">
<h2 id="orged2081f">Discussion</h2>
<div class="outline-text-2" id="text-orged2081f">
<p>
Obviously the J version is shorter, but that doesn't mean it's better.
However, to me (a person who knows what the J symbols mean), the J version more clearly expresses the algorithm.
</p>

<p>
Here's what I mean by that.
</p>

<p>
In J, function arguments have default names <code>y</code> (if there's one argument) or <code>x</code> and <code>y</code> (if there are two).
Within the function I wrote, this part executes first:
</p>
<div class="org-src-container">
<pre class="src src-j">|p-"0 _ y
</pre>
</div>

<p>
This is equivalent to the following Python:
</p>
<div class="org-src-container">
<pre class="src src-python">np.<span style="font-weight: bold;">abs</span>(p-c[:,<span style="font-weight: bold; text-decoration: underline;">None</span>])
</pre>
</div>

<p>
Merely replacing the symbol for abs (<code>|</code>) with the word <code>abs</code> doesn't tell the whole story.
What's really interesting is that the <code>-</code> function is <i>modified</i> to apply at a non-default <i>rank</i>.
In this case, its left rank is 0 and its right rank is infinite (spelled <code>_</code>, and meaning "equal to the highest argument rank").
For vectors x and y, <code>x -"(0 _) y</code> implies subtracting the entire vector y from each element of x.
</p>

<p>
This illustrates a key difference between J and Python's Numpy library; in J, all functions have a <i>rank</i> (which the user may override), whereas in Numpy, functions may be <i>broadcast</i> onto the elements of an argument.
This is why I used <code>c[:,None]</code> to add a leading axis to <code>c</code>, transforming it into a 1-column matrix rather than a vector.
</p>

<p>
Essentially, in Numpy, you need to <i>modify the data</i> to fit the broadcasting rules.
But in J, you <i>modify the function</i> to fit the data.
</p>

<p>
Next up in the J explanation is this snippet (and from here I'll use <code>...</code> to indicate "the previously explained expression"):
</p>
<div class="org-src-container">
<pre class="src src-j">{.@/:"1 ...
</pre>
</div>

<p>
This is equivalent to <code>np.argsort(..., axis=0)[0]</code>, but I used <code>np.argmin(..., axis=0)</code> in the Python version since it is more conventional.
</p>

<p>
In both J and Python, this "argmin" operation returns a vector that looks essentially like this:
</p>
<pre class="example">
0 0 0 1 1 2 2 2 2
</pre>

<p>
Last we have this section:
</p>
<div class="org-src-container">
<pre class="src src-j">p(+/%#)/.~ ...
</pre>
</div>

<p>
This uses <i>key</i> (<code>/.</code>) which is like "group by" in SQL, but with a twist: rather than solely forming groups, it <i>applies a function</i> to the groups.
In this case, the function is arithmetic mean (<code>+/%#</code>).
I think of this like "find groups in &#x2026; and get the mean of each group".
</p>

<p>
This is roughly equivalent to the following Python:
</p>
<div class="org-src-container">
<pre class="src src-python">[np.mean([p[j]<span style="font-weight: bold;">for</span> j <span style="font-weight: bold;">in</span> <span style="font-weight: bold;">range</span>(<span style="font-weight: bold;">len</span>(p))<span style="font-weight: bold;">if</span> m[j]==i])<span style="font-weight: bold;">for</span> i <span style="font-weight: bold;">in</span> <span style="font-weight: bold;">range</span>(<span style="font-weight: bold;">len</span>(c))]
</pre>
</div>

<p>
Since learning about <code>/.</code>, I think it expresses the algorithm much more clearly.
Maybe this is an unfair comparison, because the above Python uses two nested list comprehensions, which is probably considered bad style.
</p>

<p>
Here's a translation using regular for loops:
</p>
<div class="org-src-container">
<pre class="src src-python"><span style="font-weight: bold; font-style: italic;">ls</span> = []
<span style="font-weight: bold;">for</span> i <span style="font-weight: bold;">in</span> <span style="font-weight: bold;">range</span>(<span style="font-weight: bold;">len</span>(c)):
    ls.append([])
    <span style="font-weight: bold;">for</span> j <span style="font-weight: bold;">in</span> <span style="font-weight: bold;">range</span>(<span style="font-weight: bold;">len</span>(p)):
        <span style="font-weight: bold;">if</span> m[j] == i:
            ls[i].append(p[j])

    <span style="font-weight: bold; font-style: italic;">ls</span>[i] = np.mean(ls[i])
</pre>
</div>

<p>
This version has a low amount of complexity per line.
Maybe that makes it easier to understand.
However, it takes up a lot more space, and it seems like the majority of the characters are devoted to moving through the structures, and only a tiny fraction deal with the core of the algorithm.
To me, this feels like the operations (and the data for that matter) are visually less important than the control flow.
</p>

<p>
Currently, I prefer <code>p(+/%#)/.~</code>.
</p>
</div>
</div>

<div id="outline-container-orge222b1d" class="outline-2">
<h2 id="orge222b1d">One More Thing</h2>
<div class="outline-text-2" id="text-orge222b1d">
<p>
So far, this has been dealing with a single iteration of the k-means algorithm.
But usually, we want to run multiple iterations, updating the means each time to hopefully converge on a good solution.
</p>

<p>
Here's a Python version which iterates until the result repeats itself:
</p>
<div class="org-src-container">
<pre class="src src-python"><span style="font-weight: bold;">import</span> numpy <span style="font-weight: bold;">as</span> np
<span style="font-weight: bold; font-style: italic;">p</span> = np.array([1,3,9,11,12,37,43,45,60])
<span style="font-weight: bold; font-style: italic;">c</span> = np.array([5,15,25])
<span style="font-weight: bold;">def</span> <span style="font-weight: bold;">foo</span>(c,p):
 <span style="font-weight: bold; font-style: italic;">m</span>=np.argmin(np.<span style="font-weight: bold;">abs</span>(p-c[:,<span style="font-weight: bold; text-decoration: underline;">None</span>]),axis=0)
 <span style="font-weight: bold; font-style: italic;">x</span>=[np.mean([p[j]<span style="font-weight: bold;">for</span> j <span style="font-weight: bold;">in</span> <span style="font-weight: bold;">range</span>(<span style="font-weight: bold;">len</span>(p))<span style="font-weight: bold;">if</span> m[j]==i])<span style="font-weight: bold;">for</span> i <span style="font-weight: bold;">in</span> <span style="font-weight: bold;">range</span>(<span style="font-weight: bold;">len</span>(c))]
 <span style="font-weight: bold;">return</span> np.array(x)
<span style="font-weight: bold;">while</span> <span style="font-weight: bold; text-decoration: underline;">True</span>:
 <span style="font-weight: bold; font-style: italic;">cnew</span> = foo(c,p)
 <span style="font-weight: bold;">if</span> np.allclose(cnew,c):
  <span style="font-weight: bold;">break</span>
 <span style="font-weight: bold; font-style: italic;">c</span> = cnew
<span style="font-weight: bold;">print</span>(np.<span style="font-weight: bold;">round</span>(cnew,1))
</pre>
</div>

<p>
I added a while loop, another numpy function (<code>np.allclose</code>), and another variable.
</p>

<p>
And finally, here's the J version that iterates until convergence:
</p>
<div class="org-src-container">
<pre class="src src-j">0j1":{{p(+/%#)/.~{.@/:"1|p-"0 _ y}}^:_[5 15 25[p=:1 3 9 11 12 37 43 45 60
</pre>
</div>

<p>
That's a 1-character diff, so I'll highlight it here:
</p>
<div class="org-src-container">
<pre class="src src-diff">- ^:1
+ ^:_
</pre>
</div>

<p>
Instead of "do the function once", this means "do the function until it stops changing".
This showcases what J is all about: finding essential abstractions that compose well, and writing them concisely.
</p>
</div>
</div>
</article>
    </main>

    <footer>
      <div class="license-container">
        <div class="license-item">Designed in 2024 by <a href="https://alexshroyer.com">Alex Shroyer</a></div>
        <div class="license-item"><a href="https://www.gnu.org/licenses/gpl-3.0.html">GNU GPLv3</a> (code)</div>
        <div class="license-item"><a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC-BY-4.0</a> (prose)</div>
      </div>
      <div class="social">
        <a href="https://stackoverflow.com/users/2037637/alex-shroyer" aria-label="my StackOverflow profile">
          <svg role="img" aria-hidden="true" class="svg-icon iconLogoGlyphMd native" width="32" height="37" viewBox="0 0 32 37">
            <path d="M26 33v-9h4v13H0V24h4v9h22z"></path>
            <path d="m21.5 0-2.7 2 9.9 13.3 2.7-2L21.5 0zM26 18.4 13.3 7.8l2.1-2.5 12.7 10.6-2.1 2.5zM9.1 15.2l15 7 1.4-3-15-7-1.4 3zm14 10.79.68-2.95-16.1-3.35L7 23l16.1 2.99zM23 30H7v-3h16v3z"></path>
          </svg>
        </a>
        <a href="https://github.com/hoosieree" aria-label="my GitHub profile">
          <svg role="img" height="32" aria-hidden="true" viewBox="0 0 16 16" version="1.1" width="32" data-view-component="true" class="octicon octicon-mark-github v-align-middle">
            <path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0016 8c0-4.42-3.58-8-8-8z"></path>
          </svg>
        </a>
        <a href="https://www.linkedin.com/in/alex-shroyer" aria-label="my LinkedIn profile"><svg role="img" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34" class="global-nav__logo">
            <g>
              <path d="M34,2.5v29A2.5,2.5,0,0,1,31.5,34H2.5A2.5,2.5,0,0,1,0,31.5V2.5A2.5,2.5,0,0,1,2.5,0h29A2.5,2.5,0,0,1,34,2.5ZM10,13H5V29h5Zm.45-5.5A2.88,2.88,0,0,0,7.59,4.6H7.5a2.9,2.9,0,0,0,0,5.8h0a2.88,2.88,0,0,0,2.95-2.81ZM29,19.28c0-4.81-3.06-6.68-6.1-6.68a5.7,5.7,0,0,0-5.06,2.58H17.7V13H13V29h5V20.49a3.32,3.32,0,0,1,3-3.58h.19c1.59,0,2.77,1,2.77,3.52V29h5Z" > </path>
            </g>
          </svg>
        </a>
      </div>
    </footer>
    <script>
      window.MathJax={
  tex:{ams:{multlineWidth:'85%'},tags:'ams',tagSide:'right',tagIndent:'.8em'},
  chtml:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  svg:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  output:{font:'mathjax-modern',displayOverflow:'overflow'}}
    </script>
  </body>
</html>
