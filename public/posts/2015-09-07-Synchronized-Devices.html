<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="research and hobbies of a computer engineer">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/main.css">
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <title></title>
    <link rel="canonical" href="alexshroyer.com/2015-09-07-Synchronized-Devices.html">
  </head>
  <body>
    <header>
      <nav>
        <a href="/index.html">Home</a>
        <a href="/pages/cv.html">CV</a>
        <a href="/pages/projects.html">Projects</a>
        <a href="/rss.xml">RSS</a>
      </nav>
    </header>
    <main>
      <article><h1 class="title">Synchronized Devices</h1><div class="abstract" id="orgb4c67eb">
<p>
Instrumentation for psychological experiments can be simple or complex.
It can be as simple as a pencil-and-paper survey, or as complex as an fMRI outfitted with custom electronics.
The researchers I work with generally want something on the complex side.
The most common challenge I've seen is the problem of time-aligning heterogeneous data.
</p>

</div>

<div id="outline-container-orga52a2fc" class="outline-2">
<h2 id="orga52a2fc"></h2>
<div class="outline-text-2" id="text-orga52a2fc">
<p>
Researchers I work with usually want to measure some combination of:
</p>

<ul class="org-ul">
<li>intentional behaviors (e.g. pressing buttons, answering questions)</li>
<li>physiological changes in response to stimuli (e.g. heart rate, posture, fMRI data, facial expression, gaze)</li>
</ul>
<p>
My job is to help researchers build new instrumentation or get different kinds of instrumentation to work together.
</p>

<p>
Most devices I see in this line of work connect over USB.
FireWire, serial, and custom interfaces are becoming increasingly rare.
Wireless devices are a growing minority.
Networked (Ethernet, Wi-Fi) devices are very rare.
The kind of device varies greatly, from humble laptop keyboards to multi-channel specialized sensors like ECG machines.
</p>
</div>
</div>

<div id="outline-container-org688f86e" class="outline-2">
<h2 id="org688f86e">Example</h2>
<div class="outline-text-2" id="text-org688f86e">
<p>
Most researchers are pragmatic.
They like to start simple and add complexity as needed.
Usually this approach wins because the system they end up with does exactly what they need and nothing else.
But sometimes, especially when precise timing is desired, a more top-down design is better.
</p>

<p>
Let's follow an example scenario to illustrate.
</p>

<p>
The initial experiment design revolves around a subject solving a crossword puzzle in a room.
While they solve the puzzle, someone knocks on the door.
The experiment looks at how they respond to the knock.
The hypothesis is that people are less likely to answer the door if they are told they must complete the crossword in 10 minutes.
</p>

<p>
Additionally, the reasercher wants to record the subject's heart rate during the experiment.
A particular brand of heart rate monitor has been used successfully in other experiments and is generally regarded as accurate.
The monitor connects over USB and interfaces with a closed-source program.
From within this program, we can view waveforms and graphs of the heart rate, and generate text-format recordings of the monitor.
</p>

<blockquote>
<p>
Task: Measure the time between the knock on the door and an increase in heart rate.
</p>
</blockquote>

<p>
This task, like so many in the real world, requires aggregating heterogeneous data from heterogeneous sources.
One data source is our heart rate monitor.
Another is the PC's system clock.
</p>
</div>
</div>

<div id="outline-container-org6aaf86b" class="outline-2">
<h2 id="org6aaf86b">Approaches</h2>
<div class="outline-text-2" id="text-org6aaf86b">
<p>
There are several approaches to choose from depending on the problem at hand.
One is to embed an out-of-band signal into multiple data streams.
For example, to synchronize mulitple cameras one could put an infrared blinker within view of all the cameras.
This infrared marker would be invisible to humans participating in a scene, but visible to the cameras.
After capturing a scene, we can (manually or programmatically) time-align the cameras on the blink rate.
</p>

<p>
In our heart rate monitor + system clock example, we need a different approach.
The software interface is output-only and we can't modify it.
The USB interface is handled by the host operating system.
Obtaining precise (1ms resolution) timing from a user-space application is not practical, because USB interrupts will be serviced on non-deterministic intervals (at least from the perspective of a user-space app).
</p>

<p>
Sending a "start" signal to the USB device also suffers from user-space non-determinicity, because getting a timestamp from the system is one system call, and sending it to a device is another, which requires jumping into and out of the kernel at least twice, incurring arbitrary delays each time.
</p>

<p>
The approach I've used to date has been to encode timestamps into the device's data output stream.
Most microcontrollers have a real-time clock which can be read from and written to output data (such as a serial stream or raw HID byte array).
This provides a precise clock that solves half of the problem.
The other half is matching this clock up with a non-real-time clock such as the PC system time.
For 1ms precision it's not enough to emit `milliseconds since program start` because there is latency between when the user tells the program to start, and when the microcontroller receives the message.
To synchronize the two clocks, we have to compute the latency on each side.
</p>

<p>
The procedure would look something like this:
</p>

<blockquote>
<p>
PC: hi microcontroller, begin an experiment; The time is HHMMSS.SSS (system timestamp).
</p>

<p>
uC: hi PC, I'm beginning an experiment at HHMMSS.SSS and resetting my clock to 0.
</p>

<p>
uC: here's the first data point at `0 + t1`.
</p>

<p>
PC: I got your first data point at `HHMMSS.SSS + t2`
</p>

<p>
uC: here's another data point at `0 + t3`, with a round-trip latency of `t2 - t1`
</p>
</blockquote>

<p>
And so on.
This means that the experimental data contains both the wall clock accuracy and the real time clock precision.
</p>
</div>
</div>

<div id="outline-container-org35ec86d" class="outline-2">
<h2 id="org35ec86d">Future Work</h2>
<div class="outline-text-2" id="text-org35ec86d">
<p>
The approach I'd like to implement soon is an "in-line real-time wrapper".
In the heart rate example, we don't have access to the internals of either the hardware or software, so the "wrapper" is the USB channel itself.
By running the USB device through a deterministic, real-time kernel such as an embedded Linux variant, we can capture its data, timestamp it (within the context of the real-time OS), and export this timestamped data separately from the USB data.
</p>
</div>
</div>
</article>
    </main>

    <footer>
      <div class="license-container">
        <div class="license-item">Designed in 2024 by <a href="https://alexshroyer.com">Alex Shroyer</a></div>
        <div class="license-item"><a href="https://www.gnu.org/licenses/gpl-3.0.html">GNU GPLv3</a> (code)</div>
        <div class="license-item"><a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC-BY-4.0</a> (prose)</div>
      </div>
      <div class="social">
        <a href="https://stackoverflow.com/users/2037637/alex-shroyer" aria-label="my StackOverflow profile">
          <svg role="img" aria-hidden="true" class="svg-icon iconLogoGlyphMd native" width="32" height="37" viewBox="0 0 32 37">
            <path d="M26 33v-9h4v13H0V24h4v9h22z"></path>
            <path d="m21.5 0-2.7 2 9.9 13.3 2.7-2L21.5 0zM26 18.4 13.3 7.8l2.1-2.5 12.7 10.6-2.1 2.5zM9.1 15.2l15 7 1.4-3-15-7-1.4 3zm14 10.79.68-2.95-16.1-3.35L7 23l16.1 2.99zM23 30H7v-3h16v3z"></path>
          </svg>
        </a>
        <a href="https://github.com/hoosieree" aria-label="my GitHub profile">
          <svg role="img" height="32" aria-hidden="true" viewBox="0 0 16 16" version="1.1" width="32" data-view-component="true" class="octicon octicon-mark-github v-align-middle">
            <path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0016 8c0-4.42-3.58-8-8-8z"></path>
          </svg>
        </a>
        <a href="https://www.linkedin.com/in/alex-shroyer" aria-label="my LinkedIn profile"><svg role="img" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34" class="global-nav__logo">
            <g>
              <path d="M34,2.5v29A2.5,2.5,0,0,1,31.5,34H2.5A2.5,2.5,0,0,1,0,31.5V2.5A2.5,2.5,0,0,1,2.5,0h29A2.5,2.5,0,0,1,34,2.5ZM10,13H5V29h5Zm.45-5.5A2.88,2.88,0,0,0,7.59,4.6H7.5a2.9,2.9,0,0,0,0,5.8h0a2.88,2.88,0,0,0,2.95-2.81ZM29,19.28c0-4.81-3.06-6.68-6.1-6.68a5.7,5.7,0,0,0-5.06,2.58H17.7V13H13V29h5V20.49a3.32,3.32,0,0,1,3-3.58h.19c1.59,0,2.77,1,2.77,3.52V29h5Z" > </path>
            </g>
          </svg>
        </a>
      </div>
    </footer>
    <script>
      window.MathJax={
  tex:{ams:{multlineWidth:'85%'},tags:'ams',tagSide:'right',tagIndent:'.8em'},
  chtml:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  svg:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  output:{font:'mathjax-modern',displayOverflow:'overflow'}}
    </script>
  </body>
</html>
