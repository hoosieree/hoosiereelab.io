<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="research and hobbies of a computer engineer">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/main.css">
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <title></title>
    <link rel="canonical" href="alexshroyer.com/2024-12-30-Guitar-Effects-Again.html">
  </head>
  <body>
    <header>
      <nav>
        <a href="/index.html">Home</a>
        <a href="/pages/cv.html">CV</a>
        <a href="/pages/projects.html">Projects</a>
        <a href="/rss.xml">RSS</a>
      </nav>
    </header>
    <main>
      <article><h1 class="title">Guitar Effects Again</h1><div class="abstract" id="org91b6e6b">
<p>
Getting back into guitar effects after a long break.
</p>

</div>

<div id="outline-container-org97efca7" class="outline-2">
<h2 id="org97efca7">History</h2>
<div class="outline-text-2" id="text-org97efca7">
<p>
My fascination with music equipment and electronics was what originally led me down the path toward engineering. There was always something very satisfying about tinkering with a circuit and being able to immediately hear a change. Software can have some of that same feel when you have a short feedback loop (REPLs and live reloads help) but because software is so malleable and because there's no limit to the number of abstracting wrappers you can put on a concept, software ends up lacking some of that immediacy that I get from hardware.
</p>

<p>
Anyway, I was deep into guitar effects pedal circuitry about 15 years ago. My path took me to engineering school and then an engineering career doing mostly software and a bit of hardware here and there. The musical part of my life got smaller and I lost a lot of the inspiration to create music that once seemed endless. But I didn't stop appreciating music, and the musical world kept going even while I took a break.
</p>

<p>
Today in 2024, it feels like a new golden age for electric guitar. Many players still have conservative tastes and prefer some of the early designs that featured in the early days of rock music, but at the same time it's never been easier to obtain more varieties of guitars. There are 7- and 8-string guitars, multiscale, baritones, and headless varieties for sale and not just from premium or custom builders. Some of the finest instruments are still handmade by master craftspeople, but CNC machining has raised the bar of quality at every price by reducing the amount of manual work necessary to create a good guitar.
</p>

<p>
Another development that's hard to overstate is how the internet and especially YouTube have helped grow communities and interest in niches like electric guitars. There are more high-quality players and teachers than ever, and they're more accessible than ever thanks to these technologies. Recently I searched online for software to allow for remote music rehearsal and there are at least a couple products that claim to make it possible with low latency. Maybe it's already possible to have a live jam session with bandmates spread all over the world (or at least all over a country).
</p>
</div>
</div>

<div id="outline-container-org9f5a520" class="outline-2">
<h2 id="org9f5a520">Nostalgia</h2>
<div class="outline-text-2" id="text-org9f5a520">
<p>
So now I want to get back into this hobby that I used to enjoy so much. One of the first things I built was a clean boost and buffer circuit. This one in fact:
</p>


<div id="orgb601591" class="figure">
<p><img src="../images/buster-2011-04-13.png" alt="buster-2011-04-13.png">
</p>
</div>

<p>
Unfortunately, I didn't record all the values for this old version, but I am pretty sure they are:
</p>

<ul class="org-ul">
<li>R1, R5: 1M</li>
<li>R2, R3, R7, R9: 10k</li>
<li>R6: 1k</li>
<li>R4, R8: 100</li>
<li>C1, C4: 100n</li>
<li>C2, C3: 100u</li>
<li>D1: 1n4001</li>
</ul>

<p>
However, after breadboarding it last night, I ended up with a new version. Here's a <a href="https://falstad.com/afilter/circuitjs.html?cct=$+1+0.000005+5+50+5+50%0A%25+0+24389.095776063856%0Aa+544+240+704+240+1+15+-15+1000000%0Aw+544+256+544+320+0%0Aw+704+240+704+320+0%0Ac+544+320+704+320+0+3.3e-9+0%0Ac+320+224+400+224+0+1e-7+0%0Ar+400+224+400+304+0+1000000%0Ag+400+304+400+320+0%0Ar+400+224+544+224+0+220%0A170+320+224+272+224+2+20+4000+5+0.1%0AO+896+240+976+240+0%0Ac+704+240+800+240+0+1e-7+0%0Ar+800+240+800+320+0+100000%0Ar+800+240+896+240+0+220%0Ag+800+320+800+352+0%0A174+544+400+640+368+0+10000+0.005+Resistance%0Aw+704+400+704+320+0%0Ar+480+400+480+528+0+10000%0Aw+480+400+416+400+0%0Ac+416+400+416+480+0+6.8000000000000005e-9+0%0Ar+416+480+416+528+0+3300%0Aw+480+528+416+528+0%0Ag+416+528+416+560+0%0Ar+544+320+480+320+0+1000%0Aw+480+320+480+400+0%0Aw+544+400+480+400+0%0Aw+640+400+704+400+0%0Aw+592+368+592+352+0%0Aw+592+352+544+352+0%0Aw+544+352+544+320+0%0A">simulation</a>, and here's the schematic:
</p>


<div id="org1567df0" class="figure">
<p><img src="../images/buster-2024-12-29.png" alt="buster-2024-12-29.png">
</p>
</div>

<p>
What's different?
</p>

<ul class="org-ul">
<li>reduced the max possible gain (used to be about 10dB, now about 7dB)</li>
<li>frequency response is now a subtle mid/treble boost at higher gain settings
<ul class="org-ul">
<li>6.8n/3.3k provides a boost around 4kHz</li>
<li>3.3n cuts highs around 6kHz</li>
<li>together they form a mid-hump around 5kHz</li>
</ul></li>
<li>1k resistor so the 10k (linear) pot feels more like an audio taper</li>
<li>my current implementation is true bypass instead of the "gain bypass" switch so I can compare against the unbuffered sound</li>
</ul>

<p>
What's the same?
</p>

<ul class="org-ul">
<li>still a transparent (flat frequency response) buffer at minimum gain</li>
<li>diode polarity protection (not shown in new schematic but it's on my breadboard)</li>
</ul>

<p>
What's the motivation behind the changes?
</p>

<ul class="org-ul">
<li>my tastes have changed</li>
<li>my ears are older</li>
</ul>

<p>
Thinking about it more deeply, I feel like the older version is trying to be more versatile than it needs to be. Now that I'm older, I'm less concerned with pleasing a mass audience than making something that's just right for me.
</p>
</div>
</div>

<div id="outline-container-org4a34cc1" class="outline-2">
<h2 id="org4a34cc1">New Circuit</h2>
<div class="outline-text-2" id="text-org4a34cc1">
<p>
This is a soft clipping overdrive in the style of the Ibanez TS-9 or Boss SD-1, but with germanium diodes. I used to have a TS-7 Tube Screamer, but I never really liked how it sounded so I sold it at a yard. But perhaps I was too hasty to write off soft clipping overdrives in general, because I gave the circuit another shot and I actually like this one. I prefer it with germanium clipping diodes because they kick in sooner and make it seem like there's more distortion. The frequency response is slightly flatter than the TS I remember.
</p>
<ul class="org-ul">
<li><a href="https://falstad.com/afilter/circuitjs.html?cct=$+1+0.000005+5+50+5+50%0A%25+0+24389.095776063856%0A170+512+352+464+352+2+20+4000+5+0.1%0AO+1264+416+1312+416+0%0Aa+672+336+816+336+0+15+-15+1000000%0Aa+976+320+1120+320+0+15+-15+1000000%0Ar+928+336+816+336+0+10000%0Ac+512+352+576+352+0+1e-7+0%0Ar+576+352+576+464+0+1000000%0Ag+576+464+576+496+0%0Ar+576+352+672+352+0+1000%0Aw+672+320+672+272+0%0Aw+816+336+816+272+0%0Ac+672+272+816+272+0+3.3e-9+0%0Ac+1120+320+1216+320+0+0.000001+0%0Ag+1216+464+1216+496+0%0Aw+928+336+976+336+0%0A174+928+240+896+176+0+10000+0.9950000000000001+tone%0Ar+976+176+1120+176+0+10000%0Aw+928+176+976+176+0%0Aw+928+240+928+336+0%0Ac+896+176+848+176+0+1e-8+0%0Ar+848+176+848+240+0+1000%0Ag+848+240+848+272+0%0Aw+976+176+976+224+0%0Ac+976+224+1120+224+0+1e-8+0%0Aw+1120+176+1120+224+0%0Aw+1120+224+1120+320+0%0Aw+976+224+976+304+0%0Aw+896+176+896+208+0%0Aw+816+272+816+176+0%0A174+736+176+608+224+0+10000+0.9950000000000001+gain%0Ar+736+176+816+176+0+22000%0Aw+672+224+672+272+0%0Ac+608+176+528+176+0+1.2e-7+0%0Ar+528+176+528+256+0+120%0Ag+528+256+528+272+0%0Ar+480+128+480+256+0+10000%0Aw+480+256+528+256+0%0Ac+480+128+608+128+0+0.000001+0%0Aw+608+128+608+176+0%0Ax+651+133+757+136+0+24+gain+(10k)%0Ax+1251+333+1389+336+0+24+volume+(10k)%0Ax+881+130+989+133+0+24+tone+(10k)%0A174+1216+368+1264+464+0+1000+0.5+Resistance%0Ar+1216+320+1216+368+0+100%0A">frequency response sim (without diodes)</a></li>
<li><a href="https://falstad.com/circuit/circuitjs.html?ctz=CQAgLCAMB0l3BWEBGaBmATGZHK+QJzIIBsCGGICkVVNCApgLTLIBQA8ipgBwhok+yNAHYS-QVChsAhiBIjKaASB7Jxy8UKQskyePBSxqIvDwJgCaSD0glKqSCeRgw9jDzSECBFAciyIARi-LgoODSYNNogun7+Rsg8PBgIaLYi2CnqickYZILBaJb2EPoGbABOQR78KmoaKjTlcGwAxlQ4-ORUIWg9ZcwisTBqNpAEdmAi6RgiyFCwkAsB1Qh9PevibhDN-qu9Gj0KSj17rQDu8oqhNCcgc5QBVw11WjmP0h33n6+fEGh0MxfEwYPoSJBMMVXJg0nYnksVu1wmEoigMDk0bslvAVtAeCIUusXE4nASujA4EiAObo2zgHI4emuPg0Z41PiaIJ9JpsZCZDkPMDRAjifnic5wRaIEAAFwA9gA7BhVblikKsMLiqQtdkEWra4LqiVsK76vhYGjmt5fVSilAhHhgIQhQZMITQQjEKxgVIKYX2ynLaTVJ0urTOoVs+IBWlhqOqSOfNmmtUO8RGh5YW2ZihlCJZiBu1k4vAYzAiH2kHgkdJ4qnSK6a5oagt5xsomjt5u3Du57OZ6y7VPmY128S4Vl8gUzMW1CHhnUGRY8UgQakyACWitVs7pqkZtS7uEu10o7Z+NwC3xs6aoBtdRgwQ0WBBrBAQCGE02dqS-0uDA5yEXYCHlIHVcDYWlQL9e8LSvENwHpJkkK7cDJXZZ00PEGD0ORLD9wXfdsX8BZQWgMBMAKVJKIoUwwCQIMkSuIiUNYkIAgAD3kbN9AWGY9iLIUQA3bcAB0AGcAAp9AAawASjYbjhGIOo0BQBj1K5CBswAN3lAAbABXABbBhJJkyAFKU8duHOMplHAYSFWVCy5MU-kyg8GhsDFXgbUlV8P32fYFn04yzNVHBMS1bze3OVMfmze5CCeNhihEhgUCMeZKOA1KxjgZ8mGGNloEyBjIF-dYyzonU2AAEzPFB9QPMVWsoakVReRlWr+bN2X6iA-ivRKbm1FLWsGw8+FeVLpAAJU6U5KB2bonmy6gyvU6MYAQRq2sLZr206lUlq2cASAgUpLt2KRhXAaBGKkPa2HlHUpE4tJhQldBsBmFIqpIQRIErERGJxFZ+GyhZphe8BhjQNggA">schematic (with diodes)</a></li>
</ul>


<div id="orgaffdc58" class="figure">
<p><img src="../images/gesoft-2024-12-31.png" alt="gesoft-2024-12-31.png">
</p>
</div>
</div>
</div>

<div id="outline-container-orge7d7ae5" class="outline-2">
<h2 id="orge7d7ae5">Enclosed</h2>
<div class="outline-text-2" id="text-orge7d7ae5">
<p>
I put them together in a polished box with the boost before the overdrive. Hardwood dowel knobs with burned-on indicators. I think this box needs some control labels or graphic touches to finish it up.
</p>


<div id="org9c2c6a6" class="figure">
<p><img src="../images/gesoft-built.jpg" alt="gesoft-built.jpg">
</p>
</div>
</div>
</div>
</article>
    </main>

    <footer>
      <div class="license-container">
        <div class="license-item">Designed in 2024 by <a href="https://alexshroyer.com">Alex Shroyer</a></div>
        <div class="license-item"><a href="https://www.gnu.org/licenses/gpl-3.0.html">GNU GPLv3</a> (code)</div>
        <div class="license-item"><a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC-BY-4.0</a> (prose)</div>
      </div>
      <div class="social">
        <a href="https://stackoverflow.com/users/2037637/alex-shroyer" aria-label="my StackOverflow profile">
          <svg role="img" aria-hidden="true" class="svg-icon iconLogoGlyphMd native" width="32" height="37" viewBox="0 0 32 37">
            <path d="M26 33v-9h4v13H0V24h4v9h22z"></path>
            <path d="m21.5 0-2.7 2 9.9 13.3 2.7-2L21.5 0zM26 18.4 13.3 7.8l2.1-2.5 12.7 10.6-2.1 2.5zM9.1 15.2l15 7 1.4-3-15-7-1.4 3zm14 10.79.68-2.95-16.1-3.35L7 23l16.1 2.99zM23 30H7v-3h16v3z"></path>
          </svg>
        </a>
        <a href="https://github.com/hoosieree" aria-label="my GitHub profile">
          <svg role="img" height="32" aria-hidden="true" viewBox="0 0 16 16" version="1.1" width="32" data-view-component="true" class="octicon octicon-mark-github v-align-middle">
            <path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0016 8c0-4.42-3.58-8-8-8z"></path>
          </svg>
        </a>
        <a href="https://www.linkedin.com/in/alex-shroyer" aria-label="my LinkedIn profile"><svg role="img" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34" class="global-nav__logo">
            <g>
              <path d="M34,2.5v29A2.5,2.5,0,0,1,31.5,34H2.5A2.5,2.5,0,0,1,0,31.5V2.5A2.5,2.5,0,0,1,2.5,0h29A2.5,2.5,0,0,1,34,2.5ZM10,13H5V29h5Zm.45-5.5A2.88,2.88,0,0,0,7.59,4.6H7.5a2.9,2.9,0,0,0,0,5.8h0a2.88,2.88,0,0,0,2.95-2.81ZM29,19.28c0-4.81-3.06-6.68-6.1-6.68a5.7,5.7,0,0,0-5.06,2.58H17.7V13H13V29h5V20.49a3.32,3.32,0,0,1,3-3.58h.19c1.59,0,2.77,1,2.77,3.52V29h5Z" > </path>
            </g>
          </svg>
        </a>
      </div>
    </footer>
    <script>
      window.MathJax={
  tex:{ams:{multlineWidth:'85%'},tags:'ams',tagSide:'right',tagIndent:'.8em'},
  chtml:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  svg:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  output:{font:'mathjax-modern',displayOverflow:'overflow'}}
    </script>
  </body>
</html>
