<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="research and hobbies of a computer engineer">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/main.css">
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <title></title>
    <link rel="canonical" href="alexshroyer.com/2023-05-29-VM-Repl.html">
  </head>
  <body>
    <header>
      <nav>
        <a href="/index.html">Home</a>
        <a href="/pages/cv.html">CV</a>
        <a href="/pages/projects.html">Projects</a>
        <a href="/rss.xml">RSS</a>
      </nav>
    </header>
    <main>
      <article><h1 class="title">VM Repl</h1><div class="abstract" id="org6e7d6a7">
<p>
What special features does a language interpreter need that are different from an ahead-of-time compiled language?
</p>

</div>

<div id="outline-container-orgc163f5a" class="outline-2">
<h2 id="orgc163f5a"></h2>
<div class="outline-text-2" id="text-orgc163f5a">
<p>
Programming languages fall into 2 main categories: compiled and interpreted.
Usually this also implies different levels of interactivity.
Compiled usually means not interactive, and interpreted usually means interactive, and often that implies that the interpreted language has a read-eval-print-loop (repl) interface.
Sometimes this repl is the de-facto language implementation, in the same way that some compiled languages have only one compiler implementation.
</p>

<p>
There are exceptions to this, but for the most part it's a useful generalization.
But the differences get blurry in some places.
For example, interpreted languages often have constructs that have multiple parts, and which don't make sense unless all parts exist.
</p>

<p>
Here's a concrete example in Python:
</p>

<div class="org-src-container">
<pre class="src src-python"><span style="font-weight: bold;">if</span> condition:
  <span style="font-weight: bold;">print</span>(<span style="font-weight: bold; text-decoration: underline;">True</span>)
<span style="font-weight: bold;">else</span>:
  <span style="font-weight: bold;">print</span>(<span style="font-weight: bold; text-decoration: underline;">False</span>)
</pre>
</div>

<p>
Python interpreters allow users to type this if/else statement character by character.
This requires the interpreter to <i>defer</i> execution of some lines of code in certain situations.
Specifically, when you type the above statement, the interpreter goes into a special mode and does not execute the first print statement right away.
The cpython repl handles this in an interesting way.
While you type a multi-line statement, it defers interpretation until the user types Enter twice:
</p>

<div class="org-src-container">
<pre class="src src-python">&gt;&gt;&gt; <span style="font-weight: bold;">if</span> 1/0:                                 <span style="font-weight: bold; font-style: italic;"># </span><span style="font-weight: bold; font-style: italic;">no error yet</span>
...   <span style="font-weight: bold;">print</span>(<span style="font-style: italic;">'control does not reach here'</span>)  <span style="font-weight: bold; font-style: italic;"># </span><span style="font-weight: bold; font-style: italic;">still fine</span>
...                                         <span style="font-weight: bold; font-style: italic;"># </span><span style="font-weight: bold; font-style: italic;">type Enter once to get here</span>
&gt;&gt;&gt;                                         <span style="font-weight: bold; font-style: italic;"># </span><span style="font-weight: bold; font-style: italic;">type Enter twice, error shows up here</span>
</pre>
</div>

<p>
By contrast, if the user types <code>1/0</code> without an <code>if</code> in front, the error appears right away:
</p>

<div class="org-src-container">
<pre class="src src-python">&gt;&gt;&gt; 1/0                                     <span style="font-weight: bold; font-style: italic;"># </span><span style="font-weight: bold; font-style: italic;">type Enter; instant error message</span>
</pre>
</div>
</div>
</div>


<div id="outline-container-orgb188a25" class="outline-2">
<h2 id="orgb188a25">Deferring execution in the repl</h2>
<div class="outline-text-2" id="text-orgb188a25">
<p>
In the original Nand2Tetris VM language, there is global, "top-level" code, and there are function definitions.
In theory, these are separate execution contexts.
In practice, the only thing that happens in the global scope is an implicit call to a <code>Sys.main</code> function.
From there, all execution occurs in the context of functions.
</p>

<p>
However, when adapting this language for use in a repl, I decided to allow the user to execute top-level statements in between function definitions.
This makes the repl more useful, because you don't need an implicit <code>Sys.main</code> and can start executing code right away.
But a <code>goto</code> statement does not make sense in a repl context, so <a href="./2023-05-21-Stack-VM-Language.html">I removed it</a>.
I also added an <code>end</code> keyword to mark the end of a function definition<sup><a id="fnr.1" class="footref" href="#fn.1" role="doc-backlink">1</a></sup>.
</p>

<p>
From a usability perspective, the interpreter should let the user define a function without immediately executing it.
For example, as long as you don't <i>call</i> A before <i>defining</i> B, this should work:
</p>

<div class="org-src-container">
<pre class="src src-asm"><span style="font-weight: bold;">function</span> <span style="font-weight: bold;">A</span> 0
  <span style="font-weight: bold;">call</span> B 0          <span style="font-weight: bold; font-style: italic;">// </span><span style="font-weight: bold; font-style: italic;">B hasn't been defined yet</span>
  <span style="font-weight: bold;">return</span>
<span style="font-weight: bold;">end</span>

<span style="font-weight: bold;">push</span> <span style="font-weight: bold;">constant</span> 999   <span style="font-weight: bold; font-style: italic;">// </span><span style="font-weight: bold; font-style: italic;">some top-level code</span>

<span style="font-weight: bold;">function</span> <span style="font-weight: bold;">B</span> 0
  <span style="font-weight: bold;">push</span> constant 32
  <span style="font-weight: bold;">return</span>
<span style="font-weight: bold;">end</span>

<span style="font-weight: bold;">call</span> <span style="font-weight: bold;">A</span> 0            <span style="font-weight: bold; font-style: italic;">// </span><span style="font-weight: bold; font-style: italic;">B is now defined, so we can call A</span>
</pre>
</div>

<p>
For an ahead-of-time compiled language, steps to execute are:
</p>
<ol class="org-ol">
<li>tokenize and parse the source code</li>
<li>translate function definitions to assembly code</li>
<li>later, execute the assembly code, which will begin with a jump to <code>Sys.init</code></li>
</ol>

<p>
This interpreted version takes a different route:
</p>
<ol class="org-ol">
<li>tokenize and parse the source code</li>
<li>when interpreting a function definition, save the code somewhere, but don't execute it yet</li>
<li>later, when interpreting a <code>call</code> instruction, jump to the appropriate function and begin execution</li>
</ol>
</div>
</div>
<div id="footnotes">
<h2 class="footnotes">Footnotes: </h2>
<div id="text-footnotes">

<div class="footdef"><sup><a id="fn.1" class="footnum" href="#fnr.1" role="doc-backlink">1</a></sup> <div class="footpara" role="doc-footnote"><p class="footpara">
The <code>end</code> keyword marks the end of a function definition and resumes a global execution context between functions.
As I was writing this post, another way of ending functions occurred to me: I could copy how Python works.
The language could treat an extra blank line in the interpreter as a "function end" token.
This would mean that code written for the compiler and code written for the interpreter could look the <i>same</i> but have different <i>meanings</i>.
But my preference is that code that is different should also look different, so for now the <code>end</code> keyword stays.
</p></div></div>


</div>
</div>
</article>
    </main>

    <footer>
      <div class="license-container">
        <div class="license-item">Designed in 2024 by <a href="https://alexshroyer.com">Alex Shroyer</a></div>
        <div class="license-item"><a href="https://www.gnu.org/licenses/gpl-3.0.html">GNU GPLv3</a> (code)</div>
        <div class="license-item"><a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC-BY-4.0</a> (prose)</div>
      </div>
      <div class="social">
        <a href="https://stackoverflow.com/users/2037637/alex-shroyer" aria-label="my StackOverflow profile">
          <svg role="img" aria-hidden="true" class="svg-icon iconLogoGlyphMd native" width="32" height="37" viewBox="0 0 32 37">
            <path d="M26 33v-9h4v13H0V24h4v9h22z"></path>
            <path d="m21.5 0-2.7 2 9.9 13.3 2.7-2L21.5 0zM26 18.4 13.3 7.8l2.1-2.5 12.7 10.6-2.1 2.5zM9.1 15.2l15 7 1.4-3-15-7-1.4 3zm14 10.79.68-2.95-16.1-3.35L7 23l16.1 2.99zM23 30H7v-3h16v3z"></path>
          </svg>
        </a>
        <a href="https://github.com/hoosieree" aria-label="my GitHub profile">
          <svg role="img" height="32" aria-hidden="true" viewBox="0 0 16 16" version="1.1" width="32" data-view-component="true" class="octicon octicon-mark-github v-align-middle">
            <path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0016 8c0-4.42-3.58-8-8-8z"></path>
          </svg>
        </a>
        <a href="https://www.linkedin.com/in/alex-shroyer" aria-label="my LinkedIn profile"><svg role="img" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34" class="global-nav__logo">
            <g>
              <path d="M34,2.5v29A2.5,2.5,0,0,1,31.5,34H2.5A2.5,2.5,0,0,1,0,31.5V2.5A2.5,2.5,0,0,1,2.5,0h29A2.5,2.5,0,0,1,34,2.5ZM10,13H5V29h5Zm.45-5.5A2.88,2.88,0,0,0,7.59,4.6H7.5a2.9,2.9,0,0,0,0,5.8h0a2.88,2.88,0,0,0,2.95-2.81ZM29,19.28c0-4.81-3.06-6.68-6.1-6.68a5.7,5.7,0,0,0-5.06,2.58H17.7V13H13V29h5V20.49a3.32,3.32,0,0,1,3-3.58h.19c1.59,0,2.77,1,2.77,3.52V29h5Z" > </path>
            </g>
          </svg>
        </a>
      </div>
    </footer>
    <script>
      window.MathJax={
  tex:{ams:{multlineWidth:'85%'},tags:'ams',tagSide:'right',tagIndent:'.8em'},
  chtml:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  svg:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  output:{font:'mathjax-modern',displayOverflow:'overflow'}}
    </script>
  </body>
</html>
