<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="research and hobbies of a computer engineer">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/main.css">
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <title></title>
    <link rel="canonical" href="alexshroyer.com/2023-05-21-Stack-VM-Language.html">
  </head>
  <body>
    <header>
      <nav>
        <a href="/index.html">Home</a>
        <a href="/pages/cv.html">CV</a>
        <a href="/pages/projects.html">Projects</a>
        <a href="/rss.xml">RSS</a>
      </nav>
    </header>
    <main>
      <article><h1 class="title">Stack VM Language</h1><div class="abstract" id="orgd9bd5aa">
<p>
A deep dive into the Nand2Tetris VM language, and what it means to write an interpreter for a compiled language.
</p>

</div>

<div id="outline-container-org16232c3" class="outline-2">
<h2 id="org16232c3">Why virtual machines?</h2>
<div class="outline-text-2" id="text-org16232c3">
<p>
The point of a virtual machine is to make writing multi-layered software easier.
The idea is we have multiple computer architectures, and multiple computer languages that we want to eventually compile to those target architectures.
But this means a lot of duplicated work - each language needs:
</p>
<ul class="org-ul">
<li>tokenizing</li>
<li>parsing</li>
<li>code generation</li>
</ul>
<p>
So if you have <code>N</code> languages and <code>M</code> architectures, then supporting the full matrix takes <code>N*M</code> effort:
</p>


<div id="org7931907" class="figure">
<p><img src="../images/n_times_m.svg" alt="n_times_m.svg" class="org-svg">
</p>
<p><span class="figure-number">Figure 1: </span>N times M compilers</p>
</div>


<p>
However, if you split the "front-end" tasks of tokenizing and parsing from "back-end" tasks like code generation, you get a different pattern:
</p>


<div id="orgda307f5" class="figure">
<p><img src="../images/n_plus_m.svg" alt="n_plus_m.svg" class="org-svg">
</p>
<p><span class="figure-number">Figure 2: </span>N plus M compilers</p>
</div>


<p>
By adding a Virtual Machine as an intermediate stage, you can make it less costly to add new languages and/or architectures, because the complexity is <code>N+M</code> rather than <code>N*M</code>.
</p>

<p>
Now, I should take a moment here to point out that this is only worth doing if 3 things are simultaneously true:
</p>
<ol class="org-ol">
<li>Extracting the VM into a separate part is not a big deal.</li>
<li>You plan to have more than 2 languages.</li>
<li>You plan to have more than 2 architecture targets.</li>
</ol>

<p>
For most people building a programming language, the number of architectures worth supporting is likely 1 to 3.
Likewise, part of the reason to make a programming language is to do things differently from others, so your new language might not have much in common with existing languages.
This means more difficulty finding commonalities a VM can exploit.
</p>

<p>
However, there is an important exception to this advice.
The LLVM project demonstrates that a modular compiler architecture is useful for multiple different languages.
Front end interfaces exist for LLVM to support languages such as C, C++, Rust, Pony, and Scheme.
If you're interested in making a real programming language, it might be a great idea to take advantage of the great work of LLVM's optimization and code generation back-ends, and focus your efforts on the syntax and semantics of your language by writing a front-end for LLVM.
</p>

<p>
That said, if you want to learn more about writing a stripped-down VM, read on.
</p>
</div>
</div>
<div id="outline-container-org1a56391" class="outline-2">
<h2 id="org1a56391">Nand2Tetris VM translator</h2>
<div class="outline-text-2" id="text-org1a56391">
<p>
In the Nand2Tetris curriculum, chapters 7 and 8 guide you through the task of building a VM Translator program.
This program translates Hack Virtual Machine sources in <code>.vm</code> file format into Hack Assembly <code>.asm</code> files.
</p>

<p>
Here's a sample of Hack VM language:
</p>

<div class="org-src-container">
<pre class="src src-asm"><span style="font-weight: bold;">push</span> <span style="font-weight: bold;">constant</span> 7
<span style="font-weight: bold;">push</span> <span style="font-weight: bold;">constant</span> 8
<span style="font-weight: bold;">sub</span>
</pre>
</div>

<p>
This program pushes 7, then 8, onto a stack, pops both to perform the subtraction (7-8) and pushes the result (-1) onto the stack.
In the end, the stack will contain only the value -1.
</p>

<p>
The language also supports user-defined functions and function calls:
</p>

<div class="org-src-container">
<pre class="src src-asm"><span style="font-weight: bold;">function</span> <span style="font-weight: bold;">foo</span> 1   <span style="font-weight: bold; font-style: italic;">// </span><span style="font-weight: bold; font-style: italic;">declare a function named foo that has 1 local variable</span>
  <span style="font-weight: bold;">push</span> constant 42
  <span style="font-weight: bold;">pop</span> local 0      <span style="font-weight: bold; font-style: italic;">// </span><span style="font-weight: bold; font-style: italic;">local[0] = 42</span>
  <span style="font-weight: bold;">push</span> argument 0  <span style="font-weight: bold; font-style: italic;">// </span><span style="font-weight: bold; font-style: italic;">copy arg[0] to stack</span>
  <span style="font-weight: bold;">push</span> argument 1  <span style="font-weight: bold; font-style: italic;">// </span><span style="font-weight: bold; font-style: italic;">copy arg[1] to stack</span>
  <span style="font-weight: bold;">sub</span>              <span style="font-weight: bold; font-style: italic;">// </span><span style="font-weight: bold; font-style: italic;">top of stack: arg[0]-arg[1]</span>
  <span style="font-weight: bold;">push</span> local 0     <span style="font-weight: bold; font-style: italic;">// </span><span style="font-weight: bold; font-style: italic;">42</span>
  <span style="font-weight: bold;">call</span> bar 2       <span style="font-weight: bold; font-style: italic;">// </span><span style="font-weight: bold; font-style: italic;">call bar with 2 arguments (top 2 stack values)</span>
  <span style="font-weight: bold;">neg</span>              <span style="font-weight: bold; font-style: italic;">// </span><span style="font-weight: bold; font-style: italic;">negate it</span>
  <span style="font-weight: bold;">return</span>           <span style="font-weight: bold; font-style: italic;">// </span><span style="font-weight: bold; font-style: italic;">return the negated vesion of the above expression</span>
</pre>
</div>

<p>
Following the Nand2Tetris curriculum for chapter 7, implement support first for the arithmetic operations (<code>add</code>, <code>sub</code>, etc.) and then for control flow commands like <code>if-goto</code> and <code>label</code>.
Then for chapter 8, implement <code>function</code>, <code>call</code>, and <code>return</code>.
</p>

<p>
The function-related commands of chapter 8 are non-trivial and require pushing values to the stack at runtime.
This is also in my opinion the most interesting part of the whole course, because it requires you to think carefully about what kinds of transformations your program can do to the source code, and what tasks belong to the runtime environment.
The key part of chapter 8 is the function calling convention.
This convention dictates what happens at runtime when the code issues a <code>call</code> instruction, and what happens during a <code>return</code> instruction.
</p>

<p>
In short, the caller first pushes any arguments the callee needs to the stack, then it pushes its own return address, then it pushes the values of <code>LCL</code>, <code>ARG</code>, <code>THIS</code>, and <code>THAT</code> to the stack.
It then sets the value of <code>ARG</code> to point at the first argument it pushed earlier.
Finally, it sets <code>LCL</code> to the current <code>SP</code> value and jumps to the function it's calling.
This jump is technically a <code>goto</code> that <i>is</i> allowed to leave the function's scope.
</p>

<p>
For example, the function calling convention requires pushing the current values for <code>SP</code>, <code>ARG</code>, <code>LCL</code>, <code>THIS</code>, and <code>THAT</code> to the stack before a function call.
These are the names of host RAM addresses 0-4, respectively.
The currently-running function uses these values to determine where it should look in RAM for some values it needs.
Other functions will probably have different values, so something needs to keep track.
That "something" happens at runtime.
</p>
</div>
</div>

<div id="outline-container-org43a39bb" class="outline-2">
<h2 id="org43a39bb">"Translator" assumptions</h2>
<div class="outline-text-2" id="text-org43a39bb">
<p>
By the end of chapter 8, you'll have a translator that can take multiple <code>.vm</code> files and combine them to make a single <code>.asm</code> file.
Individual files can contain multiple functions, but "static" variables are private to their containing file.
</p>

<p>
The biggest assumption of the translator is that there is a <code>Sys.init</code> function which sets up some pointers, then calls <code>main</code>, and finally when <code>main</code> returns, goes into an infinite loop.
</p>

<p>
In chapter 7, these aspects are not as important, and you can test just the arithmetic and control flow commands by allowing the VM emulator to do the tasks which would otherwise be done by <code>Sys.init</code>.
</p>

<p>
But the additions of chapter 8 introduce some subtle edge cases in the language.
For example, the language specifies that <code>goto</code> and <code>if-goto</code> may not leave their containing function.
However, the chapter 7 tests use gotos outside of any function; in other words, the language has <i>global</i> gotos.
</p>

<p>
But these are more like unit tests than system or integration tests, and I think the intention is that "real" programs will always start by implicitly calling <code>Sys.init</code>, which never returns.
So even though the language permits global gotos, in practice they are never used.
</p>

<p>
Another edge case of the language is the fact that there is no syntax to mark the end of a function.
To demonstrate, first consider a function with an obvious ending:
</p>

<div class="org-src-container">
<pre class="src src-asm"><span style="font-weight: bold;">function</span> <span style="font-weight: bold;">f1</span> 0
  <span style="font-weight: bold;">return</span>  <span style="font-weight: bold; font-style: italic;">// </span><span style="font-weight: bold; font-style: italic;">f1 obviously ends here</span>
</pre>
</div>

<p>
But some functions have ambiguous endings:
</p>

<div class="org-src-container">
<pre class="src src-asm"><span style="font-weight: bold;">function</span> <span style="font-weight: bold;">f2</span> 0
  <span style="font-weight: bold;">goto</span> NEXT
  <span style="font-weight: bold;">label</span> START
  <span style="font-weight: bold;">return</span>     <span style="font-weight: bold; font-style: italic;">// </span><span style="font-weight: bold; font-style: italic;">f2 returns here, so the function's control ends here</span>
  <span style="font-weight: bold;">label</span> NEXT
  <span style="font-weight: bold;">goto</span> START <span style="font-weight: bold; font-style: italic;">// </span><span style="font-weight: bold; font-style: italic;">but the text of the function actually ends here</span>
</pre>
</div>

<p>
In practice, the VM Translator can't tell where a function ends unless it either:
a) reaches the end of the file, or
b) reaches a new function definition.
</p>

<p>
A subtle consequence of this is that it's not possible to mix global code with functions, and therefore a VM Translator program must be entirely function definitions.
</p>
</div>
</div>

<div id="outline-container-org3dea0f8" class="outline-2">
<h2 id="org3dea0f8">Compiled versus interpreted</h2>
<div class="outline-text-2" id="text-org3dea0f8">
<ul class="org-ul">
<li><b>compilation</b> means transforming source code from one language to another; often, the destination language executable</li>
<li><b>interpretation</b> means executing source code of a language</li>
</ul>

<p>
These are over-simplified, but workable definitions.
In particular, the Nand2Tetris VM Translator is a compiler, because it transforms VM sources into Hack assembly code.
But I think it would be fun to have an interpreter for this language, to enable interactive explorations and to get a more visceral feel for how it works.
So I'm building one.
</p>

<p>
In this interpreter, I want to retain the feel of the original VM language.
It still uses an array of signed 16 bit values as its host RAM, and the first 5 addresses represent <code>SP</code>, <code>LCL</code>, <code>ARG</code>, <code>THIS</code>, and <code>THAT</code> pointers.
But I'm building it in a webpage, so I need to make some minor changes.
The first is that filesystem access is a bit&#x2026; weird in a webpage context, so I need to figure out how to handle multiple <code>.vm</code> sources.
Maybe they will be files you have to upload to the webpage, or maybe there will be a place to copy-paste code.
That's a problem for my future self, though.
</p>

<p>
What I have at the moment is closer to a compiler than an interpreter.
It passes all the chapter 7 tests and most of the chapter 8 tests.
The current failing test that I'm debugging involves function calls, so I suspect either my caller or callee is not doing the right thing.
</p>
</div>
</div>

<div id="outline-container-org48f2328" class="outline-2">
<h2 id="org48f2328">Language changes</h2>
<div class="outline-text-2" id="text-org48f2328">
</div>
<div id="outline-container-org8a34d7a" class="outline-3">
<h3 id="org8a34d7a">Explicit function <code>end</code> token</h3>
<div class="outline-text-3" id="text-org8a34d7a">
<p>
The (compiled) language in practice does not permit mixing functions and global code in the same <code>.vm</code> file.
But in an interpreted context, most of the code will be global.
</p>

<p>
This means the interpreter needs to know where functions end.
One way to do this would be a keyboard shortcut or button in the interpreter, to switch from "regular" mode to "function defining" mode.
But I dislike this approach - it hides too much information.
</p>

<p>
I prefer to change the language.
A syntactic token such as <code>end</code> could spell this out in the syntax:
</p>

<div class="org-src-container">
<pre class="src src-asm"><span style="font-weight: bold;">function</span> <span style="font-weight: bold;">f3</span> 0
  <span style="font-weight: bold;">return</span>
<span style="font-weight: bold;">end</span>

<span style="font-weight: bold;">push</span> <span style="font-weight: bold;">constant</span> 21  <span style="font-weight: bold; font-style: italic;">// </span><span style="font-weight: bold; font-style: italic;">executable code...</span>
<span style="font-weight: bold;">call</span> <span style="font-weight: bold;">f3</span> 0         <span style="font-weight: bold; font-style: italic;">// </span><span style="font-weight: bold; font-style: italic;">between function definitions!</span>

<span style="font-weight: bold;">function</span> <span style="font-weight: bold;">f4</span> 0
  <span style="font-weight: bold;">return</span>
<span style="font-weight: bold;">end</span>
</pre>
</div>

<p>
This new language is incompatible with the original langauge, but at least it's easy to <i>see</i> the incompatibility.
When writing software, I prefer "obviously no bugs" to "no obvious bugs"<sup><a id="fnr.1" class="footref" href="#fn.1" role="doc-backlink">1</a></sup>, so I like this kind of visibility.
</p>
</div>
</div>

<div id="outline-container-org4912249" class="outline-3">
<h3 id="org4912249">No more global <code>goto</code></h3>
<div class="outline-text-3" id="text-org4912249">
<p>
An ahead-of-time compiled program knows all the possible destinations for all the <code>goto</code> commands in the program, so jumping forward or backward is no problem.
But an interpreted program executes one line at a time, so jumping forward would require time travel, and even jumping backward would imply taking control away from the user, making the program non-interactive.
Both of these situations seem bad, so I hereby abolish the global <code>goto</code>.
</p>
</div>
</div>

<div id="outline-container-org6e6e87b" class="outline-3">
<h3 id="org6e6e87b">Modules instead of files(?)</h3>
<div class="outline-text-3" id="text-org6e6e87b">
<p>
This is a half-baked idea (something for my future self to figure out) but if files are currently used for namespaces, then in a webpage context, we could use something else to provide namespace-like behavior.
</p>

<p>
In the compiled VM Translator world, references to <code>static</code> locations resolve to unique names in the generated assembly.
For example, the command <code>push static 3</code> in a VM file <code>Abc.vm</code> becomes a Hack assembly symbol <code>Abc.3</code>.
</p>

<p>
Similarly, you can use the same name for different functions, as long as those functions are in different files.
So if we're changing the language syntax, we may as well add namespaces:
</p>

<div class="org-src-container">
<pre class="src src-asm"><span style="font-weight: bold;">namespace</span> <span style="font-weight: bold;">Abc</span>
 <span style="font-weight: bold;">function</span> f 0
  <span style="font-weight: bold;">push</span> argument 0
  <span style="font-weight: bold;">return</span>
 <span style="font-weight: bold;">end</span>
<span style="font-weight: bold;">end</span>

<span style="font-weight: bold;">namespace</span> <span style="font-weight: bold;">Xyz</span>
 <span style="font-weight: bold;">function</span> f 0 <span style="font-weight: bold; font-style: italic;">//</span><span style="font-weight: bold; font-style: italic;">Xyz.f is a different function, same name as Abc.f</span>
  <span style="font-weight: bold;">pop</span> local 2
  <span style="font-weight: bold;">return</span>
 <span style="font-weight: bold;">end</span>
<span style="font-weight: bold;">end</span>
</pre>
</div>

<p>
In the above example I reused the function-ending <code>end</code> token for ending namespaces too.
</p>
</div>
</div>
</div>
<div id="footnotes">
<h2 class="footnotes">Footnotes: </h2>
<div id="text-footnotes">

<div class="footdef"><sup><a id="fn.1" class="footnum" href="#fnr.1" role="doc-backlink">1</a></sup> <div class="footpara" role="doc-footnote"><p class="footpara">
C.A.R. Hoare, <a href="https://dl.acm.org/doi/10.1145/358549.358561">The Emperor's Old Clothes</a>, 1980.
</p></div></div>


</div>
</div>
</article>
    </main>

    <footer>
      <div class="license-container">
        <div class="license-item">Designed in 2024 by <a href="https://alexshroyer.com">Alex Shroyer</a></div>
        <div class="license-item"><a href="https://www.gnu.org/licenses/gpl-3.0.html">GNU GPLv3</a> (code)</div>
        <div class="license-item"><a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC-BY-4.0</a> (prose)</div>
      </div>
      <div class="social">
        <a href="https://stackoverflow.com/users/2037637/alex-shroyer" aria-label="my StackOverflow profile">
          <svg role="img" aria-hidden="true" class="svg-icon iconLogoGlyphMd native" width="32" height="37" viewBox="0 0 32 37">
            <path d="M26 33v-9h4v13H0V24h4v9h22z"></path>
            <path d="m21.5 0-2.7 2 9.9 13.3 2.7-2L21.5 0zM26 18.4 13.3 7.8l2.1-2.5 12.7 10.6-2.1 2.5zM9.1 15.2l15 7 1.4-3-15-7-1.4 3zm14 10.79.68-2.95-16.1-3.35L7 23l16.1 2.99zM23 30H7v-3h16v3z"></path>
          </svg>
        </a>
        <a href="https://github.com/hoosieree" aria-label="my GitHub profile">
          <svg role="img" height="32" aria-hidden="true" viewBox="0 0 16 16" version="1.1" width="32" data-view-component="true" class="octicon octicon-mark-github v-align-middle">
            <path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0016 8c0-4.42-3.58-8-8-8z"></path>
          </svg>
        </a>
        <a href="https://www.linkedin.com/in/alex-shroyer" aria-label="my LinkedIn profile"><svg role="img" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34" class="global-nav__logo">
            <g>
              <path d="M34,2.5v29A2.5,2.5,0,0,1,31.5,34H2.5A2.5,2.5,0,0,1,0,31.5V2.5A2.5,2.5,0,0,1,2.5,0h29A2.5,2.5,0,0,1,34,2.5ZM10,13H5V29h5Zm.45-5.5A2.88,2.88,0,0,0,7.59,4.6H7.5a2.9,2.9,0,0,0,0,5.8h0a2.88,2.88,0,0,0,2.95-2.81ZM29,19.28c0-4.81-3.06-6.68-6.1-6.68a5.7,5.7,0,0,0-5.06,2.58H17.7V13H13V29h5V20.49a3.32,3.32,0,0,1,3-3.58h.19c1.59,0,2.77,1,2.77,3.52V29h5Z" > </path>
            </g>
          </svg>
        </a>
      </div>
    </footer>
    <script>
      window.MathJax={
  tex:{ams:{multlineWidth:'85%'},tags:'ams',tagSide:'right',tagIndent:'.8em'},
  chtml:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  svg:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  output:{font:'mathjax-modern',displayOverflow:'overflow'}}
    </script>
  </body>
</html>
