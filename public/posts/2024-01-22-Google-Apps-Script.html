<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="research and hobbies of a computer engineer">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/main.css">
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <title></title>
    <link rel="canonical" href="alexshroyer.com/2024-01-22-Google-Apps-Script.html">
  </head>
  <body>
    <header>
      <nav>
        <a href="/index.html">Home</a>
        <a href="/pages/cv.html">CV</a>
        <a href="/pages/projects.html">Projects</a>
        <a href="/rss.xml">RSS</a>
      </nav>
    </header>
    <main>
      <article><h1 class="title">Google Apps Script</h1><div class="abstract" id="org82429f3">
<p>
Customizing the behavior of a Google Form using Google Apps Script.
</p>

</div>

<div id="outline-container-org2921898" class="outline-2">
<h2 id="org2921898">The Setup</h2>
<div class="outline-text-2" id="text-org2921898">
<p>
The owner of a small business comes to you with a problem.
Her business offers specialized training courses for professionals, and clients earn a certification by correctly answering a quiz at the end.
Unfortunately, manually grading these quizzes takes a lot of time.
</p>

<p>
Your task is to craft an automated solution that leverages her existing infrastructure.
Currently, her business uses Google Forms for the quizzes.
</p>

<p>
Google Forms can be configured as a quiz where each question earns a certain number of points.
When a user submits the quiz they can see their score, but Google Forms does not provide any kind of grading feature.
The Form creator can view all the quiz results, but then it is up to her to do something with those results.
</p>

<p>
Your job will be to automatically send quiz-takers a PDF certificate if they score high enough on the quiz.
</p>
</div>
</div>

<div id="outline-container-orga193733" class="outline-2">
<h2 id="orga193733">The Specification</h2>
<div class="outline-text-2" id="text-orga193733">
<p>
The business offers multiple different training courses, with different names and durations.
</p>
<ol class="org-ol">
<li>A passing score is 75% or higher.</li>
<li>If the user passes, they receive an email certificate.</li>
<li>The certificate is a PDF with parameters:
<ul class="org-ul">
<li>Full name</li>
<li>Date</li>
<li>Name of training</li>
<li>Number of credit hours in the training</li>
</ul></li>
</ol>
</div>
</div>

<div id="outline-container-orge5bd143" class="outline-2">
<h2 id="orge5bd143">Apps Script Overview</h2>
<div class="outline-text-2" id="text-orge5bd143">
<p>
The purpose of Apps Script seems to be to enable extensions to Google's cloud products like Google Sheets, Google Forms, Gmail, etc.
This idea is very similar to VBA for Microsoft Office products, except instead of using Visual Basic as the scripting language, you use JavaScript.
But the language is just one part of a larger system.
Most noticeably, this system includes Apps Script classes that provide interfaces to products like Google Sheets and Gmail.
These classes are available to the language at runtime, and the ones you'll use in this job are:
</p>
<ul class="org-ul">
<li>DriveApp</li>
<li>EmailApp</li>
<li>FormApp</li>
<li>HtmlService</li>
<li>SpreadsheetApp</li>
<li>Utilities</li>
</ul>

<p>
In addition to using JavaScript, you can also add HTML sources to your script.
For this application, you will use HTML as the template for the certificate.
</p>
</div>
</div>

<div id="outline-container-org92a9250" class="outline-2">
<h2 id="org92a9250">Deployment Options</h2>
<div class="outline-text-2" id="text-org92a9250">
<p>
Apps Scripts have multiple deployment options, but the forms already exist, so here you choose to use a "container-bound" script.
For each Form, you follow this procedure:
</p>
<ol class="org-ol">
<li>Open the form.</li>
<li>Within the Form, launch the Script Editor.</li>
<li>Write a script.</li>
<li>Add a Trigger so the script runs "on Form Submit".</li>
<li>Test, debug, repeat.</li>
</ol>
</div>
</div>

<div id="outline-container-orga5bc255" class="outline-2">
<h2 id="orga5bc255">Implementation Details</h2>
<div class="outline-text-2" id="text-orga5bc255">
<p>
The way this implementation works is as a function that takes a Form submission as "input" and produces a PDF as an "output" and also updates an existing spreadsheet.
So you start with the Form.
</p>

<div class="org-src-container">
<pre class="src src-js"><span style="font-weight: bold;">const</span> <span style="font-weight: bold; font-style: italic;">form</span> = FormApp.getActiveForm(); <span style="font-weight: bold; font-style: italic;">// </span><span style="font-weight: bold; font-style: italic;">the "parent" to which this script is "container-bound"</span>
<span style="font-weight: bold;">const</span> <span style="font-weight: bold; font-style: italic;">response</span> = form.getResponses().at(-1); <span style="font-weight: bold; font-style: italic;">// </span><span style="font-weight: bold; font-style: italic;">most recent form submission</span>
</pre>
</div>

<p>
Even though the Apps Script runs on the "on form submit" trigger, it can actually access <i>all</i> the previous submissions through the FormApp class.
For now though, all you need is the most recent submission, hence the <code>.at(-1)</code>.
Next, if you want to calculate the user's score, you need two things: the number of points possible, and the number of points earned.
To get the total points possible, look to the <code>form</code> rather than the <code>response</code>:
</p>

<div class="org-src-container">
<pre class="src src-js"><span style="font-weight: bold;">const</span> <span style="font-weight: bold; font-style: italic;">items</span> = form.getItems();
<span style="font-weight: bold;">let</span> <span style="font-weight: bold; font-style: italic;">possible</span> = 0;
<span style="font-weight: bold;">for</span> (<span style="font-weight: bold;">let</span> <span style="font-weight: bold; font-style: italic;">item</span> <span style="font-weight: bold;">of</span> items) {
  <span style="font-weight: bold;">switch</span> (item.getType()) {
  <span style="font-weight: bold;">case</span> FormApp.ItemType.CHECKBOX: possible += item.asCheckboxItem().getPoints(); <span style="font-weight: bold;">break</span>;
  <span style="font-weight: bold;">case</span> FormApp.ItemType.MULTIPLE_CHOICE: possible += item.asMultipleChoiceItem().getPoints(); <span style="font-weight: bold;">break</span>;
  <span style="font-weight: bold; font-style: italic;">// </span><span style="font-weight: bold; font-style: italic;">TODO: if the quiz uses additional item types, insert more case statements to handle them here...</span>
  <span style="font-weight: bold;">default</span>: <span style="font-weight: bold;">continue</span>;
  }
}
</pre>
</div>

<p>
After writing this, you wonder to yourself why <code>getPoints</code> is not a method of <code>item</code>, but decide not to dwell on the matter.
Next you need to tally the user's score for this attempt.
You can't spell "functional programming" without "fun", so&#x2026;
</p>

<div class="org-src-container">
<pre class="src src-js"><span style="font-weight: bold;">const</span> <span style="font-weight: bold; font-style: italic;">total</span> = items.reduce((x,item)=&gt;x+response.getGradableResponseForItem(item).getScore(), 0);
</pre>
</div>

<p>
This is the minimum you need to send an email if the user scores high enough:
</p>

<div class="org-src-container">
<pre class="src src-js"><span style="font-weight: bold;">const</span> <span style="font-weight: bold; font-style: italic;">passed</span> = total &gt;= 0.75 * possible;
<span style="font-weight: bold;">if</span> (passed) {
  MailApp.sendEmail(<span style="font-style: italic;">'email@example.com'</span>, <span style="font-style: italic;">'email subject'</span>, <span style="font-style: italic;">'email body text'</span>, {
    name: <span style="font-style: italic;">'sender name'</span>,
    attachments: []
  });
}
</pre>
</div>

<p>
Next you preview your own quiz, and answer all the questions correctly in order to verify that the real email address you used in place of <code>email@example.com</code> received the email from you.
You did remember to swap that out for a real email address, right?
But not so fast, first you must give permission for the app script to send an email on your behalf.
</p>


<div id="orgec499ad" class="figure">
<p><img src="../images/apps-script-verify-1.jpg" alt="apps-script-verify-1.jpg">
</p>
</div>

<p>
You also need to verify that you're willing to run the script made by "the developer".
Since in this case, the developer is yourself, you can confidently click the "Advanced" link:
</p>


<div id="org9e774f4" class="figure">
<p><img src="../images/apps-script-verify-2.jpg" alt="apps-script-verify-2.jpg">
</p>
</div>

<p>
Finally, you can consent to all the different permissions you added (email, Sheets, Drive, etc):
</p>


<div id="orgd5554ef" class="figure">
<p><img src="../images/apps-script-allow.jpg" alt="apps-script-allow.jpg">
</p>
</div>

<p>
Run through your checklist before you start sending links to customers:
</p>

<ol class="org-ol">
<li>Ensure you use the right Google account for this script.</li>
<li>Test your quiz to verify it only sends the certification with a passing score.</li>
<li>Check that the email goes to the right recipient.</li>
</ol>
</div>
</div>

<div id="outline-container-org64f5393" class="outline-2">
<h2 id="org64f5393">More Features</h2>
<div class="outline-text-2" id="text-org64f5393">
<p>
At this point your main <code>.gs</code> file looks like this:
</p>

<div class="org-src-container">
<pre class="src src-js"><span style="font-weight: bold;">function</span> <span style="font-weight: bold;">myFunction</span>() {
  <span style="font-weight: bold;">const</span> <span style="font-weight: bold; font-style: italic;">form</span> = FormApp.getActiveForm();
  <span style="font-weight: bold;">const</span> <span style="font-weight: bold; font-style: italic;">response</span> = form.getResponses().at(-1);
  <span style="font-weight: bold;">const</span> <span style="font-weight: bold; font-style: italic;">items</span> = form.getItems();
  <span style="font-weight: bold;">const</span> <span style="font-weight: bold; font-style: italic;">email</span> = response.getRespondentEmail();
  <span style="font-weight: bold;">let</span> <span style="font-weight: bold; font-style: italic;">possible</span> = 0;
  <span style="font-weight: bold;">for</span> (<span style="font-weight: bold;">let</span> <span style="font-weight: bold; font-style: italic;">item</span> <span style="font-weight: bold;">of</span> items) {
    <span style="font-weight: bold;">switch</span> (item.getType()) {
    <span style="font-weight: bold;">case</span> FormApp.ItemType.CHECKBOX: possible += item.asCheckboxItem().getPoints(); <span style="font-weight: bold;">break</span>;
    <span style="font-weight: bold;">case</span> FormApp.ItemType.MULTIPLE_CHOICE: possible += item.asMultipleChoiceItem().getPoints(); <span style="font-weight: bold;">break</span>;
    <span style="font-weight: bold;">default</span>: <span style="font-weight: bold;">continue</span>;
    }
  }
  <span style="font-weight: bold;">const</span> <span style="font-weight: bold; font-style: italic;">total</span> = items.reduce((x,item)=&gt;x+response.getGradableResponseForItem(item).getScore(), 0);
  <span style="font-weight: bold;">const</span> <span style="font-weight: bold; font-style: italic;">passed</span> = total &gt;= 0.75 * possible;
  <span style="font-weight: bold;">if</span> (passed) {
    MailApp.sendEmail(email, <span style="font-style: italic;">'Subject Line'</span>, <span style="font-style: italic;">'Congratulations! Please find the attached certificate.'</span>, {
      name: <span style="font-style: italic;">'Sender Name'</span>,
      attachments: []
    });
  }
}
</pre>
</div>

<p>
You demo this prototype to the owner, and she confirms this is how it should work.
Now it's time to create the PDF certificate.
While it's possible to generate a string of html and then render it as a PDF, it's not the most secure method.
You could interpolate the values like this:
</p>

<div class="org-src-container">
<pre class="src src-js"><span style="font-weight: bold;">const</span> <span style="font-weight: bold; font-style: italic;">html</span> = <span style="font-style: italic;">`email: &lt;h1&gt;${responseItem.getResponse()}&lt;/h1&gt;`</span>;
</pre>
</div>

<p>
But then what if <code>responseItem</code> was a text field where they could write anything?
To ensure the integrity of the certification, you want to prevent the quiz-taker from bypassing the whole grading process and receiving an unearned certificate.
There are many ways to address this vulnerability that all fall under the umbrella of sanitizing untrusted input.
</p>

<p>
The way you think about this is that any place where a user is able to freely write whatever they want is a potential vulnerability.
Here you need to balance convenience and usability on the user's side with the integrity of the result on the application's side.
In the case of a textbox, it's safe to assume that you really want the user to have freedom to write whatever they want, and so you do not restrict anything on their side.
Client-side validation should be thought of as <i>feedback</i> to the user to inform a legitimate user how to best use the application.
It's important to not treat client-side validation as a security feature, because it is trivially bypassed by an attacker.
On the other hand, server-side validation is something you can use for security.
Google Apps Scripts have a feature they call HTML Templates, which allow you to insert data from the user into HTML with varying degrees of safety.
They call this feature <a href="https://developers.google.com/apps-script/guides/html/templates?hl=en#scriptlets">Scriptlets</a>.
</p>
<ul class="org-ul">
<li>Standard scriptlets allow mixing code and HTML.</li>
<li>Printing scriptlets interpolate values but apply contextual escaping to prevent a malicious user from injecting HTML.</li>
<li>Force-printing scriptlets interpolate without escaping (danger zone!).</li>
</ul>

<p>
The way you use Templated HTML is to add a new file to the Apps Script project (in this example it is named <b>Index.html</b>).
The corresponding Index.html file can be sprinkled with Scriptlets such as:
</p>

<div class="org-src-container">
<pre class="src src-html">&lt;<span style="font-weight: bold;">!doctype</span> html&gt;
&lt;<span style="font-weight: bold;">html</span>&gt;
  &lt;<span style="font-weight: bold;">head</span>&gt;
  &lt;/<span style="font-weight: bold;">head</span>&gt;
  &lt;<span style="font-weight: bold;">body</span>&gt;
    &lt;<span style="font-weight: bold;">p</span>&gt;&lt;?= data ?&gt;&lt;/<span style="font-weight: bold;">p</span>&gt;
  &lt;/<span style="font-weight: bold;">body</span>&gt;
&lt;/<span style="font-weight: bold;">html</span>&gt;
</pre>
</div>

<p>
And then populated by the code in the Script.gs file:
</p>

<div class="org-src-container">
<pre class="src src-js"><span style="font-weight: bold;">const</span> <span style="font-weight: bold; font-style: italic;">html</span> = HtmlService.createTemplateFromFile(<span style="font-style: italic;">'Index'</span>);
html.data = <span style="font-style: italic;">'hi world'</span>;
</pre>
</div>

<p>
Putting it all together, we have a Code.gs file:
</p>
<div class="org-src-container">
<pre class="src src-js"><span style="font-weight: bold; font-style: italic;">// </span><span style="font-weight: bold; font-style: italic;">run on form submit trigger</span>
<span style="font-weight: bold;">function</span> <span style="font-weight: bold;">myFunction</span>() {
  <span style="font-weight: bold; font-style: italic;">// </span><span style="font-weight: bold; font-style: italic;">TODO: Change the following two strings for use with different quiz/sheet:</span>
  <span style="font-weight: bold;">const</span> <span style="font-weight: bold; font-style: italic;">mySpreadsheetID</span> = <span style="font-style: italic;">'idNumber'</span>; <span style="font-weight: bold; font-style: italic;">// </span><span style="font-weight: bold; font-style: italic;">spreadsheet ID (from Sheets url)</span>
  <span style="font-weight: bold;">const</span> <span style="font-weight: bold; font-style: italic;">mySheet</span> = <span style="font-style: italic;">'Quiz Name'</span>; <span style="font-weight: bold; font-style: italic;">// </span><span style="font-weight: bold; font-style: italic;">page name (from the above spreadsheet)</span>

  <span style="font-weight: bold;">const</span> <span style="font-weight: bold; font-style: italic;">form</span> = FormApp.getActiveForm();
  <span style="font-weight: bold;">const</span> <span style="font-weight: bold; font-style: italic;">title</span> = form.getTitle();
  <span style="font-weight: bold;">const</span> <span style="font-weight: bold; font-style: italic;">response</span> = form.getResponses().at(-1);
  <span style="font-weight: bold;">const</span> <span style="font-weight: bold; font-style: italic;">items</span> = form.getItems();
  <span style="font-weight: bold;">const</span> <span style="font-weight: bold; font-style: italic;">email</span> = response.getRespondentEmail();
  <span style="font-weight: bold;">let</span> <span style="font-weight: bold; font-style: italic;">possible</span> = 0;
  <span style="font-weight: bold;">for</span> (<span style="font-weight: bold;">let</span> <span style="font-weight: bold; font-style: italic;">item</span> <span style="font-weight: bold;">of</span> items) {
    <span style="font-weight: bold;">switch</span> (item.getType()) {
      <span style="font-weight: bold;">case</span> FormApp.ItemType.CHECKBOX: possible += item.asCheckboxItem().getPoints(); <span style="font-weight: bold;">break</span>;
      <span style="font-weight: bold;">case</span> FormApp.ItemType.MULTIPLE_CHOICE: possible += item.asMultipleChoiceItem().getPoints(); <span style="font-weight: bold;">break</span>;
      <span style="font-weight: bold;">case</span> FormApp.ItemType.TEXT: fullName = response.getResponseForItem(item).getResponse(); <span style="font-weight: bold;">break</span>;
      <span style="font-weight: bold;">default</span>: <span style="font-weight: bold;">continue</span>;
    }
  }
  <span style="font-weight: bold;">const</span> <span style="font-weight: bold; font-style: italic;">total</span> = items.reduce((x,item)=&gt;x+response.getGradableResponseForItem(item).getScore(), 0);
  <span style="font-weight: bold;">const</span> <span style="font-weight: bold; font-style: italic;">date</span> = <span style="font-weight: bold;">new</span> <span style="font-weight: bold; text-decoration: underline;">Date</span>();
  <span style="font-weight: bold;">const</span> <span style="font-weight: bold; font-style: italic;">credits</span> = form.getDescription().split(<span style="font-style: italic;">' '</span>).map(parseFloat).filter(Number).pop();
  <span style="font-weight: bold;">const</span> <span style="font-weight: bold; font-style: italic;">html</span> = getHtml(date, title, fullName, credits);
  <span style="font-weight: bold;">const</span> <span style="font-weight: bold; font-style: italic;">pdf</span> = htmlToPDF(html, fullName);
  <span style="font-weight: bold;">const</span> <span style="font-weight: bold; font-style: italic;">sheet</span> = SpreadsheetApp.openById(mySpreadsheetID).getSheetByName(mySheet);
  <span style="font-weight: bold;">const</span> <span style="font-weight: bold; font-style: italic;">passed</span> = total &gt;= 0.75 * possible;
  sheet.appendRow([date.toString(), email, fullName, possible, total, passed]);
  <span style="font-weight: bold;">if</span> (passed) {
    MailApp.sendEmail(email, <span style="font-style: italic;">'Subject Line'</span>, <span style="font-style: italic;">'Congratulations! Please find the attached certificate.'</span>, {
      name: <span style="font-style: italic;">'Sender Name'</span>,
      attachments: [pdf]
    });
  }
}


<span style="font-weight: bold; font-style: italic;">//</span><span style="font-weight: bold; font-style: italic;">@param html:HtmlOutput - the html code to be converted</span>
<span style="font-weight: bold; font-style: italic;">//</span><span style="font-weight: bold; font-style: italic;">@param title:string - the name for the resulting pdf</span>
<span style="font-weight: bold; font-style: italic;">//</span><span style="font-weight: bold; font-style: italic;">@return pdf:Blob</span>
<span style="font-weight: bold;">function</span> <span style="font-weight: bold;">htmlToPDF</span>(<span style="font-weight: bold; font-style: italic;">html</span>,<span style="font-weight: bold; font-style: italic;">title</span>) {
  html.setWidth(800).setHeight(600).setTitle(title);
  <span style="font-weight: bold;">return</span> html.getBlob().getAs(<span style="font-style: italic;">"application/pdf"</span>);
}

<span style="font-weight: bold;">function</span> <span style="font-weight: bold;">dfmt</span>(<span style="font-weight: bold; font-style: italic;">date</span>) {
  <span style="font-weight: bold;">const</span> <span style="font-weight: bold; font-style: italic;">m</span> = <span style="font-style: italic;">"January February March April May June July August September October November December"</span>.split(<span style="font-style: italic;">" "</span>)[date.getMonth()];
  <span style="font-weight: bold;">const</span> <span style="font-weight: bold; font-style: italic;">d</span> = date.getDate();
  <span style="font-weight: bold;">const</span> <span style="font-weight: bold; font-style: italic;">dth</span> = d+([1,2,3,21,22,23,31].includes(d)?[0,<span style="font-style: italic;">'st'</span>,<span style="font-style: italic;">'nd'</span>,<span style="font-style: italic;">'rd'</span>][d%10]:<span style="font-style: italic;">'th'</span>);
  <span style="font-weight: bold;">return</span> <span style="font-style: italic;">`${m} ${dth}, ${date.getFullYear()}`</span>;
}

<span style="font-weight: bold; font-style: italic;">//</span><span style="font-weight: bold; font-style: italic;">@param date:string - formatted datetime</span>
<span style="font-weight: bold; font-style: italic;">//</span><span style="font-weight: bold; font-style: italic;">@param title:string - name of result document</span>
<span style="font-weight: bold; font-style: italic;">//</span><span style="font-weight: bold; font-style: italic;">@param fullName:string - as it should appear on the document</span>
<span style="font-weight: bold; font-style: italic;">//</span><span style="font-weight: bold; font-style: italic;">@param credits:int - quiz score</span>
<span style="font-weight: bold; font-style: italic;">//</span><span style="font-weight: bold; font-style: italic;">@return HtmlOutput</span>
<span style="font-weight: bold;">function</span> <span style="font-weight: bold;">getHtml</span>(<span style="font-weight: bold; font-style: italic;">date</span>, <span style="font-weight: bold; font-style: italic;">title</span>, <span style="font-weight: bold; font-style: italic;">fullName</span>, <span style="font-weight: bold; font-style: italic;">credits</span>) {
  <span style="font-weight: bold;">const</span> <span style="font-weight: bold; font-style: italic;">b64</span> =(x)=&gt;Utilities.base64Encode(DriveApp.getFileById(x).getBlob().getBytes());
  <span style="font-weight: bold;">const</span> <span style="font-weight: bold; font-style: italic;">h</span> = HtmlService.createTemplateFromFile(<span style="font-style: italic;">'Index'</span>);
  h.logo0 = <span style="font-style: italic;">`data:image/png;base64, ${b64('id_1')}`</span>; <span style="font-weight: bold; font-style: italic;">// </span><span style="font-weight: bold; font-style: italic;">replace id_N with Drive url for image</span>
  h.logo1 = <span style="font-style: italic;">`data:image/jpeg;base64, ${b64('id_2')}`</span>;
  h.logo2 = <span style="font-style: italic;">`data:image/jpeg;base64, ${b64('id_3')}`</span>;
  h.sig = <span style="font-style: italic;">`data:image/png;base64, ${b64('id_4')}`</span>;
  h.date = dfmt(date);
  h.title = title;
  h.fullName = fullName;
  h.credits = credits; <span style="font-weight: bold; font-style: italic;">// </span><span style="font-weight: bold; font-style: italic;">WARN - input is parseInt("8 Cr") so be careful if this changes</span>
  <span style="font-weight: bold;">return</span> h.evaluate();
}
</pre>
</div>

<p>
And the corresponding Index.html:
</p>
<div class="org-src-container">
<pre class="src src-html">&lt;<span style="font-weight: bold;">!DOCTYPE</span> html&gt;
&lt;<span style="font-weight: bold;">html</span>&gt;
  &lt;<span style="font-weight: bold;">head</span>&gt;
    &lt;<span style="font-weight: bold;">base</span> <span style="font-weight: bold; font-style: italic;">target</span>=<span style="font-style: italic;">"_top"</span>&gt;
    &lt;<span style="font-weight: bold;">style</span>&gt;
      page {
          text-align:center;
          background-color: #fffdeb;
      }

      @media print{
        page {
            padding:0!important;
            margin:0!important;
        }
      }
      @page {size:landscape; margin:0cm;}

      .completion {font-family:caps;}
      .logo img {width:15em;}
      .signature {height:2em;}
      .signature-label {font-style:italic;}
      .foot {
          width:100%;
          display:flex;
          flex-direction:row;
          justify-content:space-around;
          align-items:center;
      }
      .l1, .l2 {display:block; max-width:6em;}
    &lt;/<span style="font-weight: bold;">style</span>&gt;
  &lt;/<span style="font-weight: bold;">head</span>&gt;
  &lt;<span style="font-weight: bold;">body</span>&gt;
    &lt;<span style="font-weight: bold;">page</span>&gt;
      &lt;<span style="font-weight: bold;">div</span> <span style="font-weight: bold; font-style: italic;">class</span>=<span style="font-style: italic;">"logo"</span>&gt;
        &lt;<span style="font-weight: bold;">img</span> <span style="font-weight: bold; font-style: italic;">src</span>=<span style="font-style: italic;">"&lt;?!= logo0 ?&gt;"</span>&gt;
      &lt;/<span style="font-weight: bold;">div</span>&gt;
      &lt;<span style="font-weight: bold;">div</span> <span style="font-weight: bold; font-style: italic;">class</span>=<span style="font-style: italic;">"container"</span>&gt;
        &lt;<span style="font-weight: bold;">h1</span> <span style="font-weight: bold; font-style: italic;">class</span>=<span style="font-style: italic;">"completion"</span>&gt;<span style="font-weight: bold; text-decoration: underline;">Certificate of Completion</span>&lt;/<span style="font-weight: bold;">h1</span>&gt;
        &lt;<span style="font-weight: bold;">p</span>&gt;This is to certify that&lt;/<span style="font-weight: bold;">p</span>&gt;
        &lt;<span style="font-weight: bold;">h2</span>&gt;&lt;?= fullName ?&gt;&lt;/<span style="font-weight: bold;">h2</span>&gt;
        &lt;<span style="font-weight: bold;">p</span>&gt;completed&lt;/<span style="font-weight: bold;">p</span>&gt;
        &lt;<span style="font-weight: bold;">h2</span>&gt;&lt;?= title ?&gt;&lt;/<span style="font-weight: bold;">h2</span>&gt;
        &lt;<span style="font-weight: bold;">p</span>&gt;on &lt;?= date ?&gt;&lt;/<span style="font-weight: bold;">p</span>&gt;
        &lt;<span style="font-weight: bold;">hr</span> <span style="font-weight: bold; font-style: italic;">class</span>=<span style="font-style: italic;">"glyph"</span>&gt;
        &lt;<span style="font-weight: bold;">div</span> <span style="font-weight: bold; font-style: italic;">class</span>=<span style="font-style: italic;">"credits"</span>&gt;
          &lt;<span style="font-weight: bold;">p</span>&gt;&lt;?= credits ?&gt; Credit Hours Issued by&lt;/<span style="font-weight: bold;">p</span>&gt;
          &lt;<span style="font-weight: bold;">p</span>&gt;Name of Institution&lt;/<span style="font-weight: bold;">p</span>&gt;
          &lt;<span style="font-weight: bold;">br</span>&gt;
          &lt;<span style="font-weight: bold;">p</span>&gt;&lt;<span style="font-weight: bold;">img</span> <span style="font-weight: bold; font-style: italic;">class</span>=<span style="font-style: italic;">"signature"</span> <span style="font-weight: bold; font-style: italic;">src</span>=<span style="font-style: italic;">"&lt;?!= sig ?&gt;"</span>&gt;&lt;/<span style="font-weight: bold;">p</span>&gt;
          &lt;<span style="font-weight: bold;">hr</span> <span style="font-weight: bold; font-style: italic;">class</span>=<span style="font-style: italic;">"style"</span>&gt;
          &lt;<span style="font-weight: bold;">p</span> <span style="font-weight: bold; font-style: italic;">class</span>=<span style="font-style: italic;">"signature-label"</span>&gt;SIGNATURE&lt;/<span style="font-weight: bold;">p</span>&gt;
          &lt;<span style="font-weight: bold;">br</span>&gt;
        &lt;/<span style="font-weight: bold;">div</span>&gt;
        &lt;<span style="font-weight: bold;">div</span> <span style="font-weight: bold; font-style: italic;">class</span>=<span style="font-style: italic;">"foot"</span>&gt;
          &lt;<span style="font-weight: bold;">img</span> <span style="font-weight: bold; font-style: italic;">class</span>=<span style="font-style: italic;">"l1"</span> <span style="font-weight: bold; font-style: italic;">src</span>=<span style="font-style: italic;">"&lt;?!= logo1 ?&gt;"</span>&gt;
          &lt;<span style="font-weight: bold;">div</span> <span style="font-weight: bold; font-style: italic;">class</span>=<span style="font-style: italic;">"post"</span>&gt;
            &lt;<span style="font-weight: bold;">p</span>&gt;Company Name&lt;/<span style="font-weight: bold;">p</span>&gt;
            &lt;<span style="font-weight: bold;">br</span>&gt;
            &lt;<span style="font-weight: bold;">p</span>&gt;555-1234&lt;/<span style="font-weight: bold;">p</span>&gt;
            &lt;<span style="font-weight: bold;">br</span>&gt;
            &lt;<span style="font-weight: bold;">p</span>&gt;contact@example.com&lt;/<span style="font-weight: bold;">p</span>&gt;
            &lt;<span style="font-weight: bold;">p</span>&gt;Address 123&lt;/<span style="font-weight: bold;">p</span>&gt;
            &lt;<span style="font-weight: bold;">p</span>&gt;City, State 00000&lt;/<span style="font-weight: bold;">p</span>&gt;
          &lt;/<span style="font-weight: bold;">div</span>&gt;
          &lt;<span style="font-weight: bold;">img</span> <span style="font-weight: bold; font-style: italic;">class</span>=<span style="font-style: italic;">"l2"</span> <span style="font-weight: bold; font-style: italic;">src</span>=<span style="font-style: italic;">"&lt;?!= logo2 ?&gt;"</span>&gt;
        &lt;/<span style="font-weight: bold;">div</span>&gt;
      &lt;/<span style="font-weight: bold;">div</span>&gt;
    &lt;/<span style="font-weight: bold;">page</span>&gt;
  &lt;/<span style="font-weight: bold;">body</span>&gt;
&lt;/<span style="font-weight: bold;">html</span>&gt;
</pre>
</div>
</div>
</div>

<div id="outline-container-org0239366" class="outline-2">
<h2 id="org0239366">Conclusions</h2>
<div class="outline-text-2" id="text-org0239366">
<p>
Now you have an extension to Google Forms that uses Apps Script, emails template-based PDFs, and updates a Google Sheet.
What are some of the pros and cons of this approach?
</p>

<p>
Pro:
</p>
<ul class="org-ul">
<li>Integrates with client's existing workflow</li>
<li>Low ongoing maintenance</li>
<li>Uses familiar technologies</li>
<li>Good defaults for secure HTML</li>
</ul>

<p>
Con:
</p>
<ul class="org-ul">
<li>Vendor lock-in with Google products</li>
<li>Differences from browser-based JS not always obvious</li>
<li>Permissions are coarse-grained</li>
<li>Still need to be cautious with user input and threat modeling</li>
</ul>

<p>
You decide that for this scale of project, the benefits outweigh the drawbacks.
This might become a harder decision as the size of the project increases, or if it requires integrations with third-party applications and APIs.
But for non-software companies already using Google products who just need small amounts of extra functionality, Apps Script lands squarely in a sweet spot of functionality and ease of use.
</p>
</div>
</div>
</article>
    </main>

    <footer>
      <div class="license-container">
        <div class="license-item">Designed in 2024 by <a href="https://alexshroyer.com">Alex Shroyer</a></div>
        <div class="license-item"><a href="https://www.gnu.org/licenses/gpl-3.0.html">GNU GPLv3</a> (code)</div>
        <div class="license-item"><a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC-BY-4.0</a> (prose)</div>
      </div>
      <div class="social">
        <a href="https://stackoverflow.com/users/2037637/alex-shroyer" aria-label="my StackOverflow profile">
          <svg role="img" aria-hidden="true" class="svg-icon iconLogoGlyphMd native" width="32" height="37" viewBox="0 0 32 37">
            <path d="M26 33v-9h4v13H0V24h4v9h22z"></path>
            <path d="m21.5 0-2.7 2 9.9 13.3 2.7-2L21.5 0zM26 18.4 13.3 7.8l2.1-2.5 12.7 10.6-2.1 2.5zM9.1 15.2l15 7 1.4-3-15-7-1.4 3zm14 10.79.68-2.95-16.1-3.35L7 23l16.1 2.99zM23 30H7v-3h16v3z"></path>
          </svg>
        </a>
        <a href="https://github.com/hoosieree" aria-label="my GitHub profile">
          <svg role="img" height="32" aria-hidden="true" viewBox="0 0 16 16" version="1.1" width="32" data-view-component="true" class="octicon octicon-mark-github v-align-middle">
            <path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0016 8c0-4.42-3.58-8-8-8z"></path>
          </svg>
        </a>
        <a href="https://www.linkedin.com/in/alex-shroyer" aria-label="my LinkedIn profile"><svg role="img" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34" class="global-nav__logo">
            <g>
              <path d="M34,2.5v29A2.5,2.5,0,0,1,31.5,34H2.5A2.5,2.5,0,0,1,0,31.5V2.5A2.5,2.5,0,0,1,2.5,0h29A2.5,2.5,0,0,1,34,2.5ZM10,13H5V29h5Zm.45-5.5A2.88,2.88,0,0,0,7.59,4.6H7.5a2.9,2.9,0,0,0,0,5.8h0a2.88,2.88,0,0,0,2.95-2.81ZM29,19.28c0-4.81-3.06-6.68-6.1-6.68a5.7,5.7,0,0,0-5.06,2.58H17.7V13H13V29h5V20.49a3.32,3.32,0,0,1,3-3.58h.19c1.59,0,2.77,1,2.77,3.52V29h5Z" > </path>
            </g>
          </svg>
        </a>
      </div>
    </footer>
    <script>
      window.MathJax={
  tex:{ams:{multlineWidth:'85%'},tags:'ams',tagSide:'right',tagIndent:'.8em'},
  chtml:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  svg:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  output:{font:'mathjax-modern',displayOverflow:'overflow'}}
    </script>
  </body>
</html>
