<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="research and hobbies of a computer engineer">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/main.css">
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <title></title>
    <link rel="canonical" href="alexshroyer.com/2024-01-04-Search.html">
  </head>
  <body>
    <header>
      <nav>
        <a href="/index.html">Home</a>
        <a href="/pages/cv.html">CV</a>
        <a href="/pages/projects.html">Projects</a>
        <a href="/rss.xml">RSS</a>
      </nav>
    </header>
    <main>
      <article><h1 class="title">Search</h1><div class="abstract" id="orgb6e4e4b">
<p>
Russel and Norvig's approach to search problems.
</p>

</div>

<div id="outline-container-orgabed3f2" class="outline-2">
<h2 id="orgabed3f2">Search Problems</h2>
<div class="outline-text-2" id="text-orgabed3f2">
<p>
It's possible to view a lot of different kinds of problems as <i>search</i> problems.
Some obvious kinds of search are things like enemy movement in video games: the ghosts move towards pac-man, etc.
But even turn-based games like chess or tic-tac-toe involve choosing one of many possible moves repeatedly, with a goal of scoring higher than an opponent.
In real life you might try to pick the line at the grocery store checkout that minimizes your shopping time.
This may not be the shortest line, because not all lines move at the same speed.
</p>

<p>
In their book <span class="underline">Artificial intelligence a modern approach</span>, Russel and Norvig define a systematic approach to search problems.
</p>
</div>
</div>

<div id="outline-container-org8908cf2" class="outline-2">
<h2 id="org8908cf2">Abstract Approach</h2>
<div class="outline-text-2" id="text-org8908cf2">
<p>
Let's model the search problem abstractly, then later implement it concretely to explore different variations and their trade-offs.
</p>

<p>
First they define concepts like <b>search space</b>, <b>goal states</b>, a <b>successor function</b> to produce a new set of states from the current set, and a <b>cost function</b> to help rank the choices.
So for any given search function, your first task is to determine:
</p>

<ol class="org-ol">
<li>a set of states \(S\)</li>
<li>an initial state \(s_{0}\)</li>
<li>a successor function \(\text{succ}(S) \rightarrow 2^{S}\) that encodes transitions from current state \(S\) to its <a href="https://en.wikipedia.org/wiki/Power_set">power set</a> \(2^{S}\)</li>
<li>a set of goal states \(S_{G}\)</li>
<li>a cost function \(\text{cost}(s_{i}) \rightarrow \text{num}\)</li>
</ol>

<p>
Once you have all these, you start to <i>explore</i> the possible sequences of states with the aim of finding a <i>path</i> from the initial state to one of the goal states.
Depending on other factors, you might keep searching until you find the <i>best</i> path, or simply return the first one you find.
Remember that in general it's also possible that no solution exists so searches have the possibility of failure.
</p>
</div>
</div>

<div id="outline-container-org6e26b28" class="outline-2">
<h2 id="org6e26b28">Example</h2>
<div class="outline-text-2" id="text-org6e26b28">
<p>
Let's look at an example problem: navigating a 2d maze.
</p>

<pre class="example">
. . . s .
. . . . .
. . . . .
. g . . .
</pre>

<p>
Here <code>s</code> is start position and <code>g</code> is the end position, and at each iteration we can explore at most one position in any of the directions up, down, left, or right.
At each iteration the first question we should ask is: did we reach the goal?
Here the answer is no, so we continue to the next phase: exploration.
The next set of states we could possibly explore are these (<code>L</code> for left, <code>D</code> for down, <code>R</code> for right):
</p>

<pre class="example">
. . L s R
. . . D .
. . . . .
. . g . .
</pre>

<p>
Of these, both <code>L</code> and <code>D</code> are closer to <code>g</code> than the current position, so we should move to one of those.
But for clarity, here are all the possible new states \(2^{S}\)
</p>

<p>
State \(s_{1}\), after moving left:
</p>
<pre class="example">
. . s . .
. . . . .
. . . . .
. . g . .
</pre>

<p>
State \(s_{2}\), after moving down:
</p>
<pre class="example">
. . . . .
. . . s .
. . . . .
. . g . .
</pre>

<p>
State \(s_{3}\), after moving right:
</p>
<pre class="example">
. . . . s
. . . . .
. . . . .
. . g . .
</pre>

<p>
Moving right results in a new state which is "obviously" farther from the goal, so let's not do that.
But how can we make a computer program do the "obvious" right thing?
This is where the cost function comes in.
In a maze like this we can use the distance from the current position to the goal position as our cost function.
Compute the cost of each path:
</p>

<ul class="org-ul">
<li>\(\text{cost}(s_{1}) = 4\)</li>
<li>\(\text{cost}(s_{2}) = 4\)</li>
<li>\(\text{cost}(s_{3}) = 6\)</li>
</ul>

<p>
Since the cost of \(s_{3}\) is higher, we rank it lower in our set of choices.
The next step is to take one of the better choices and continue searching until we reach the goal.
This example so far has been easy because we can keep moving closer to the goal at each iteration, for example we can mark the previous position with an <code>x</code> and illustrate one possible path to the goal:
</p>

<pre class="example">
. . . x .
. . . x .
. . x x .
. . g . .
</pre>

<p>
But what if there was a wall of <code>#</code> blocking one of our paths?
</p>

<pre class="example">
. . . s .
# # # # .
. . . . .
. . g . .
</pre>

<p>
Now the only viable path to the goal requires moving right to pass the wall.
The possible paths are now moving left, resulting in failure to reach the goal:
</p>

<pre class="example">
. . x x .
# # # # .
. . . . .
. . g . .
</pre>

<p>
And moving right, which eventually results in success:
</p>
<pre class="example">
. . . x x
# # # # x
. . x x x
. . g . .
</pre>

<p>
Alternatively, we could model the wall as an "infinitely long" path, which might let us avoid iterating in that direction to begin with.
Changing the initial state slightly makes this more obvious
</p>

<pre class="example">
. . . s .
. # # # .
. . . . .
. . g . .
</pre>

<p>
Now there are at least two paths to the goal, but it turns out that initially moving right (away from the goal) is part of the shortest path.
Finding the <i>shortest path</i> implies exploring all paths to the goal.
</p>

<p>
To systematically explore the state space, we need to keep track of not only the current shortest distance to the goal, but the <i>cumulative</i> cost of the entire path from where we started.
</p>
</div>
</div>
</article>
    </main>

    <footer>
      <div class="license-container">
        <div class="license-item">Designed in 2024 by <a href="https://alexshroyer.com">Alex Shroyer</a></div>
        <div class="license-item"><a href="https://www.gnu.org/licenses/gpl-3.0.html">GNU GPLv3</a> (code)</div>
        <div class="license-item"><a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC-BY-4.0</a> (prose)</div>
      </div>
      <div class="social">
        <a href="https://stackoverflow.com/users/2037637/alex-shroyer" aria-label="my StackOverflow profile">
          <svg role="img" aria-hidden="true" class="svg-icon iconLogoGlyphMd native" width="32" height="37" viewBox="0 0 32 37">
            <path d="M26 33v-9h4v13H0V24h4v9h22z"></path>
            <path d="m21.5 0-2.7 2 9.9 13.3 2.7-2L21.5 0zM26 18.4 13.3 7.8l2.1-2.5 12.7 10.6-2.1 2.5zM9.1 15.2l15 7 1.4-3-15-7-1.4 3zm14 10.79.68-2.95-16.1-3.35L7 23l16.1 2.99zM23 30H7v-3h16v3z"></path>
          </svg>
        </a>
        <a href="https://github.com/hoosieree" aria-label="my GitHub profile">
          <svg role="img" height="32" aria-hidden="true" viewBox="0 0 16 16" version="1.1" width="32" data-view-component="true" class="octicon octicon-mark-github v-align-middle">
            <path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0016 8c0-4.42-3.58-8-8-8z"></path>
          </svg>
        </a>
        <a href="https://www.linkedin.com/in/alex-shroyer" aria-label="my LinkedIn profile"><svg role="img" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34" class="global-nav__logo">
            <g>
              <path d="M34,2.5v29A2.5,2.5,0,0,1,31.5,34H2.5A2.5,2.5,0,0,1,0,31.5V2.5A2.5,2.5,0,0,1,2.5,0h29A2.5,2.5,0,0,1,34,2.5ZM10,13H5V29h5Zm.45-5.5A2.88,2.88,0,0,0,7.59,4.6H7.5a2.9,2.9,0,0,0,0,5.8h0a2.88,2.88,0,0,0,2.95-2.81ZM29,19.28c0-4.81-3.06-6.68-6.1-6.68a5.7,5.7,0,0,0-5.06,2.58H17.7V13H13V29h5V20.49a3.32,3.32,0,0,1,3-3.58h.19c1.59,0,2.77,1,2.77,3.52V29h5Z" > </path>
            </g>
          </svg>
        </a>
      </div>
    </footer>
    <script>
      window.MathJax={
  tex:{ams:{multlineWidth:'85%'},tags:'ams',tagSide:'right',tagIndent:'.8em'},
  chtml:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  svg:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  output:{font:'mathjax-modern',displayOverflow:'overflow'}}
    </script>
  </body>
</html>
