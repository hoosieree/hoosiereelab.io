<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="research and hobbies of a computer engineer">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/main.css">
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <title></title>
    <link rel="canonical" href="alexshroyer.com/2024-05-10-Prototype-Language-Implementation.html">
  </head>
  <body>
    <header>
      <nav>
        <a href="/index.html">Home</a>
        <a href="/pages/cv.html">CV</a>
        <a href="/pages/projects.html">Projects</a>
        <a href="/rss.xml">RSS</a>
      </nav>
    </header>
    <main>
      <article><h1 class="title">Prototype Language Implementation</h1><div class="abstract" id="org1cff146">
<p>
Some strategies for developing a programming language <a href="https://github.com/hoosierEE/element">interpreter</a> (prototype).
</p>

</div>

<div id="outline-container-org253ee19" class="outline-2">
<h2 id="org253ee19"><i>Rapid</i> Prototyping</h2>
<div class="outline-text-2" id="text-org253ee19">
<p>
When exploring a new domain it's a good idea to keep your goals in mind and avoid getting sidetracked.
Currently I am implementing an interpreter for a programming language, which is something I haven't done before.
It's something I'm doing just for fun, so the chance of getting sidetracked is high.
But I also want to hit my goals, so how do I prevent my own impulses from getting the better of me?
</p>
</div>

<div id="outline-container-orgcc22eef" class="outline-3">
<h3 id="orgcc22eef">Define the Goals</h3>
<div class="outline-text-3" id="text-orgcc22eef">
<p>
I originally set out with two primary aims:
</p>
<ol class="org-ol">
<li>learning for its own sake</li>
<li>address some deficiencies in the programming languages I use</li>
</ol>

<p>
The first one will take care of itself as long as I stay on task.
The second one is something I needed to define, so here's the elevator pitch:
</p>

<p>
<i>A vector-oriented language based on K with transparent multi-core and GPU acceleration.</i>
</p>

<p>
The longer explanation is that I want to work with tensors and machine learning but Python isn't great at it.
Libraries like Jax, PyTorch, and Tensorflow are incredibly useful but not as enjoyable to use as languages which are natively array-oriented (like APL/J/K/Q/BQN/Uiua).
I still want to implement machine learning models, just with a more pleasant language.
</p>

<p>
This leads to some follow-up questions and answers:
</p>
<ol class="org-ol">
<li>Pick an implementation language that supports the GPU I have. ⇒ I have an NVIDIA GPU, therefore: CUDA/C++</li>
<li>Pick an implementation language that makes multi-core easy. ⇒ no idea</li>
<li>Based on K? Find a reference implementation (ideally open-source) to compare against. ⇒ <a href="https://codeberg.org/ngn/k">ngn/k</a></li>
</ol>
</div>
</div>

<div id="outline-container-org1294317" class="outline-3">
<h3 id="org1294317">Easy Mode</h3>
<div class="outline-text-3" id="text-org1294317">
<p>
My first attempt involved writing some CUDA C++ code to attempt to do parsing.
Being new to CUDA (and coming back to C++ after a long hiatus) this was slow going.
Then I took a step back and realized I was trying to do too many unfamiliar things at once.
</p>

<p>
So rather than dive straight in to the CUDA implementation, I decided to switch gears and do a naive implementation in a more familiar language: Python.
</p>

<p>
While part of the point of this project is to move away from Python, familiar technology can mitigate some risks.
By virtue of being familiar you move a lot of the risks from "unknown" to "known".
</p>
</div>
</div>
</div>

<div id="outline-container-org0f0630b" class="outline-2">
<h2 id="org0f0630b">Trade-Offs</h2>
<div class="outline-text-2" id="text-org0f0630b">
<p>
To keep from getting distracted by too many fun side quests, I made some conscious design choices with the prototype:
</p>
</div>

<div id="outline-container-orgf7bf37c" class="outline-3">
<h3 id="orgf7bf37c">Choose a Subset</h3>
<div class="outline-text-3" id="text-orgf7bf37c">
<p>
Rather than implement all of ngn/k, I wanted to implement some smaller fraction of it.
The choice now is what is the smallest fraction that is still <i>useful</i> for a prototype?
</p>

<p>
For me, the most interesting parts of K are its notation for lambdas and vectors, its support for heterogeneous lists and dictionaries, and its terse ascii-based primitive functions and iterators.
</p>

<p>
The less interesting parts are:
</p>
<ul class="org-ul">
<li>interaction with the host system</li>
<li>imports</li>
</ul>

<p>
Some features I wish it had:
</p>
<ul class="org-ul">
<li>lexical scope</li>
<li>closures</li>
</ul>
</div>
</div>

<div id="outline-container-org21986fa" class="outline-3">
<h3 id="org21986fa">Define Progress</h3>
<div class="outline-text-3" id="text-org21986fa">
<p>
I made a checklist and am gradually working through it:
<code>[3/6]</code>
</p>
<ul class="org-ul">
<li class="on"><code>[X]</code> lexical analysis (scanning or tokenizing)
<ul class="org-ul">
<li class="on"><code>[X]</code> names</li>
<li class="on"><code>[X]</code> built-in symbols</li>
<li class="on"><code>[X]</code> strands</li>
</ul></li>
<li class="on"><code>[X]</code> parsing
<ul class="org-ul">
<li class="on"><code>[X]</code> recognizing unary vs binary symbols</li>
<li class="on"><code>[X]</code> juxtaposition is application</li>
<li class="on"><code>[X]</code> projections like <code>(2+)</code></li>
<li class="on"><code>[X]</code> compositions like <code>(*-)</code></li>
<li class="on"><code>[X]</code> lambda definitions</li>
<li class="on"><code>[X]</code> iterators (or "adverbs")</li>
</ul></li>
<li class="trans"><code>[-]</code> semantic analysis
<ul class="org-ul">
<li class="off"><code>[&#xa0;]</code> arity checking</li>
<li class="off"><code>[&#xa0;]</code> enforce immutable/mutable semantics</li>
<li class="off"><code>[&#xa0;]</code> iterator overloads</li>
<li class="on"><code>[X]</code> lambda arguments (implicit) <code>{x}</code></li>
<li class="off"><code>[&#xa0;]</code> lambda arguments (explicit) <code>{[a]a}</code></li>
<li class="off"><code>[&#xa0;]</code> lambda partial application <code>{z}[1;2;] ⇒ {x}</code></li>
<li class="on"><code>[X]</code> projection ⇒ lambda</li>
<li class="on"><code>[X]</code> composition ⇒ lambda</li>
<li class="off"><code>[&#xa0;]</code> type inference</li>
<li class="off"><code>[&#xa0;]</code> type-based operator overloads</li>
</ul></li>
<li class="trans"><code>[-]</code> evaluation
<ul class="org-ul">
<li class="on"><code>[X]</code> name binding (in scope)</li>
<li class="on"><code>[X]</code> name lookup (in scope)</li>
<li class="on"><code>[X]</code> lambda definition</li>
<li class="on"><code>[X]</code> application</li>
<li class="off"><code>[&#xa0;]</code> operator overloads</li>
<li class="off"><code>[&#xa0;]</code> conditionals</li>
<li class="off"><code>[&#xa0;]</code> iterators</li>
</ul></li>
<li class="off"><code>[&#xa0;]</code> runtime environment
<ul class="org-ul">
<li class="off"><code>[&#xa0;]</code> pre-defined globals (none)</li>
<li class="off"><code>[&#xa0;]</code> argument scope <code>3~a+{y}[a:2;1]</code></li>
<li class="off"><code>[&#xa0;]</code> global scope access (interactive vs repl?)</li>
</ul></li>
<li class="on"><code>[X]</code> some unit tests (never truly "done")
<ul class="org-ul">
<li class="on"><code>[X]</code> scan</li>
<li class="on"><code>[X]</code> parse</li>
<li class="on"><code>[X]</code> semantic analysis</li>
<li class="on"><code>[X]</code> eval</li>
</ul></li>
</ul>

<p>
Not surprisingly, it's hard to implement part of a scanner so that part is complete.
It's possible to implement a partial parser but I finished it anyway because I'm not sure which parts of the language I can omit while still getting a good feeling for what the final version will need.
</p>

<p>
On the other hand, it's <i>very</i> easy to omit features from the evaluation/runtime part of the project.
For example, I know I want to support all the arithmetic operators, all the structural operators, and all the iterators.
</p>

<p>
But for the prototype I only need to implement one arithmetic operator to get a good sense of how they will all work.
The obvious first choice might have been addition, but I chose subtraction because it's not commutative.
It's also possible to implement addition in terms of only subtraction: <code>a+b</code> is <code>0-((0-a)-b)</code>, but it's <i>not</i> possible to implement subtraction only in terms of addition (you also need negative numbers), so this universality is also aesthetically appealing in a way I can't quite define.
</p>

<p>
Similarly, I only need to implement maybe one structural operator to get a good feeling for how all of them should work.
Here I chose the join/list operator (<code>,</code>) but I'm not sure if it's the most universal structural primitive, so I will likely implement another like <code>_</code> or <code>#</code>.
</p>

<p>
Based on my checklist I know approximately which parts need the most work, and as I make progress I am getting a better sense of which tasks are most essential.
Currently I have been working on operator overloads.
I have a gut feeling that implementing conditionals will be easy but iterators will be hard.
</p>
</div>
</div>
</div>

<div id="outline-container-orgb669bfc" class="outline-2">
<h2 id="orgb669bfc">Distractions</h2>
<div class="outline-text-2" id="text-orgb669bfc">
<p>
The side quests are endless.
</p>

<ul class="org-ul">
<li>Should I get rid of mutability altogether at least for the prototype?</li>
<li>What about static typing plus type inference?</li>
<li>Should I implement a true Tensor type?</li>
<li>How should the interpreter manage memory?
<ul class="org-ul">
<li>Will I need a garbage collector?</li>
<li>Ownership + reference counting like in <a href="https://aardappel.github.io/lobster/memory_management.html">Lobster</a>?</li>
<li>Mutable value semantics like in <a href="https://www.hylo-lang.org/">Hylo</a>?</li>
<li>Should the interpreter have its own memory or be given memory from an external runtime?</li>
</ul></li>
<li>How about interacting with the host as in <a href="https://www.roc-lang.org/platforms">Roc</a>?</li>
</ul>
</div>
</div>
</article>
    </main>

    <footer>
      <div class="license-container">
        <div class="license-item">Designed in 2024 by <a href="https://alexshroyer.com">Alex Shroyer</a></div>
        <div class="license-item"><a href="https://www.gnu.org/licenses/gpl-3.0.html">GNU GPLv3</a> (code)</div>
        <div class="license-item"><a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC-BY-4.0</a> (prose)</div>
      </div>
      <div class="social">
        <a href="https://stackoverflow.com/users/2037637/alex-shroyer" aria-label="my StackOverflow profile">
          <svg role="img" aria-hidden="true" class="svg-icon iconLogoGlyphMd native" width="32" height="37" viewBox="0 0 32 37">
            <path d="M26 33v-9h4v13H0V24h4v9h22z"></path>
            <path d="m21.5 0-2.7 2 9.9 13.3 2.7-2L21.5 0zM26 18.4 13.3 7.8l2.1-2.5 12.7 10.6-2.1 2.5zM9.1 15.2l15 7 1.4-3-15-7-1.4 3zm14 10.79.68-2.95-16.1-3.35L7 23l16.1 2.99zM23 30H7v-3h16v3z"></path>
          </svg>
        </a>
        <a href="https://github.com/hoosieree" aria-label="my GitHub profile">
          <svg role="img" height="32" aria-hidden="true" viewBox="0 0 16 16" version="1.1" width="32" data-view-component="true" class="octicon octicon-mark-github v-align-middle">
            <path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0016 8c0-4.42-3.58-8-8-8z"></path>
          </svg>
        </a>
        <a href="https://www.linkedin.com/in/alex-shroyer" aria-label="my LinkedIn profile"><svg role="img" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34" class="global-nav__logo">
            <g>
              <path d="M34,2.5v29A2.5,2.5,0,0,1,31.5,34H2.5A2.5,2.5,0,0,1,0,31.5V2.5A2.5,2.5,0,0,1,2.5,0h29A2.5,2.5,0,0,1,34,2.5ZM10,13H5V29h5Zm.45-5.5A2.88,2.88,0,0,0,7.59,4.6H7.5a2.9,2.9,0,0,0,0,5.8h0a2.88,2.88,0,0,0,2.95-2.81ZM29,19.28c0-4.81-3.06-6.68-6.1-6.68a5.7,5.7,0,0,0-5.06,2.58H17.7V13H13V29h5V20.49a3.32,3.32,0,0,1,3-3.58h.19c1.59,0,2.77,1,2.77,3.52V29h5Z" > </path>
            </g>
          </svg>
        </a>
      </div>
    </footer>
    <script>
      window.MathJax={
  tex:{ams:{multlineWidth:'85%'},tags:'ams',tagSide:'right',tagIndent:'.8em'},
  chtml:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  svg:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  output:{font:'mathjax-modern',displayOverflow:'overflow'}}
    </script>
  </body>
</html>
