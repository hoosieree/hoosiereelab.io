<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="research and hobbies of a computer engineer">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/main.css">
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <title></title>
    <link rel="canonical" href="alexshroyer.com/2022-11-12-First-Emacs-Syntax-Highlighter.html">
  </head>
  <body>
    <header>
      <nav>
        <a href="/index.html">Home</a>
        <a href="/pages/cv.html">CV</a>
        <a href="/pages/projects.html">Projects</a>
        <a href="/rss.xml">RSS</a>
      </nav>
    </header>
    <main>
      <article><h1 class="title">First Emacs Syntax Highlighter</h1><div class="abstract" id="orgd3b827f">
<p>
Writing a syntax highlighter for emacs starting from zero knowledge.
</p>

</div>

<div id="outline-container-orgcf8cffe" class="outline-2">
<h2 id="orgcf8cffe">Syntax Highlighting</h2>
<div class="outline-text-2" id="text-orgcf8cffe">
<p>
I prefer to have syntax highlighting enabled when writing and editing code and prose.
Typical syntax highlighting uses different colors for different syntactic parts in a language.
It's easiest to see with an example contrasting highlighted code with plain code:
</p>

<p>
Highlighted:
</p>
<div class="org-src-container">
<pre class="src src-python"><span style="font-weight: bold;">print</span>(23, <span style="font-style: italic;">"hello"</span>) <span style="font-weight: bold; font-style: italic;"># </span><span style="font-weight: bold; font-style: italic;">comment</span>
</pre>
</div>

<p>
Not highlighted:
</p>
<pre class="example">
print(23, "hello") # comment
</pre>

<p>
This can be helpful when you're first learning a language because it helps you spot keywords and punctuation used in unfamiliar ways.
</p>
</div>
</div>

<div id="outline-container-org8412d74" class="outline-2">
<h2 id="org8412d74">The Task</h2>
<div class="outline-text-2" id="text-org8412d74">
<p>
In a way, programming is <i>formalized problem solving</i>.
</p>

<p>
To solve a big or complex problem, common sense says to first break it down into smaller parts, then solve those parts.
By repeatedly solving the small/simple parts you will eventually solve the big/complex part.
</p>

<p>
As programmers, we use programming languages to solve informally specified goals, and we get so used to this decomposing-the-problem routine that we become susceptible to falling into <a href="https://en.wikipedia.org/wiki/Down_the_rabbit_hole">rabbit holes</a> or <a href="https://seths.blog/2005/03/dont_shave_that/">yak shaving</a>, which actually take us farther from our goal.
</p>

<p>
In this case my primary task is to rewrite my Nand2Tetris assembler (currently written in Python3) in K.
My motivation for the rewrite is to learn more about the K language.
A secondary motivation is the joy of <a href="https://code.golf/odious-numbers-long#python">Code Golf</a>.
I think it's fun to try and optimize a program for the smallest source code size possible.
</p>
</div>
</div>

<div id="outline-container-org40f0ec1" class="outline-2">
<h2 id="org40f0ec1">k-mode</h2>
<div class="outline-text-2" id="text-org40f0ec1">
<p>
So here I am about to rewrite an assembler in K.
I start by creating an empty buffer called <code>assembler.k</code> and begin adding code.
But there is no syntax highlighting!
Emacs does not know about K language by default.
I find <a href="https://github.com/gitonthescene/ngnk-mode/">k-mode</a> on GitHub which seems promising - it is for the free version of K6 that I am using (<a href="https://codeberg.org/ngn/k">ngnk</a>).
</p>

<p>
This mode seems to do a lot, but one thing it does not do is &#x2026;syntax highlighting.
</p>
</div>
</div>

<div id="outline-container-org956e3bd" class="outline-2">
<h2 id="org956e3bd">First Attempt</h2>
<div class="outline-text-2" id="text-org956e3bd">
<p>
I try writing a k-mode just for syntax highlighting, but as a newb at Emacs lisp, I make mistakes.
</p>

<p>
First, I try copy-pasting an example from <a href="https://github.com/museoa/bqn-mode/blob/trunk/bqn-syntax.el">BQN</a> (another array language).
After changing some of the values to match K's syntax it seems to be working, which is encouraging.
But then I notice that if I try to (for example) change one of the colors in the syntax file, I don't see a corresponding change in the K file.
</p>

<p>
After <a href="https://emacs.stackexchange.com/q/74544/10036">asking for help</a>, I eventually realize that the use of <code>defvar</code> when creating the syntax table means "define at most once".
This means that reloading the mode with <code>eval-buffer</code> has no effect after the first time.
</p>

<p>
One solution seems to be to use <code>defconst</code>, which by contrast <i>is</i> re-evaluated by <code>eval-buffer</code>.
Perhaps this is not the most appropriate solution, but it is the first one I found (described in detail <a href="http://endlessparentheses.com/what-s-a-defconst-and-why-you-should-use-it.html">here</a>).
As I learn more elisp, perhaps I will eventually discover a better approach but for now this gets the job done.
This already seems like a rabbit hole, but Emacs is a <a href="https://www.youtube.com/watch?v=8Ab3ArE8W3s">living</a> program so I want to take advantage of its capabilities.
</p>

<blockquote>
<p>
Sidebar - reloading in <a href="https://github.com/doomemacs/doomemacs">doom-emacs</a>
</p>

<p>
At this point I have two files open and I switch between them to see the effects of my changes.
</p>
<ul class="org-ul">
<li>k-mode.el</li>
<li>assembler.k</li>
</ul>

<p>
After editing <code>k-mode.el</code>, I want to re-evaluate the entire mode.
In doom-emacs the key combo <code>SPC m e b</code> does the job.
</p>

<p>
Then I switch to the <code>assembler.k</code> buffer, but to actually load the new mode I need to refresh it.
In doom, that's <code>SPC b r</code>.
</p>
</blockquote>

<p>
Now I have this in my <code>k-mode.el</code> file:
</p>

<div class="org-src-container">
<pre class="src src-emacs-lisp">(<span style="font-weight: bold;">defconst</span> <span style="font-weight: bold; font-style: italic;">k--token-syntax-types</span>
  '((
     (<span style="font-style: italic;">"[0-9]+"</span> . font-lock-constant-face)
     (<span style="font-style: italic;">" /.*$"</span> . font-lock-comment-face)
     )<span style="font-weight: bold;">nil nil nil))</span>

(<span style="font-weight: bold;">defconst</span> <span style="font-weight: bold; font-style: italic;">k--syntax-table</span>
  (<span style="font-weight: bold;">let</span> ((table (make-syntax-table)))
    (modify-syntax-entry ?\/  <span style="font-style: italic;">"&lt;"</span> table)
    (modify-syntax-entry ?\n  <span style="font-style: italic;">"&gt;"</span> table)
    table)
  <span style="font-style: italic;">"Syntax table for k-mode."</span>)

<span style="font-weight: bold; font-style: italic;">;;;</span><span style="font-weight: bold; font-style: italic;">###</span><span style="font-weight: bold; font-style: italic;">autoload</span>
(<span style="font-weight: bold;">defgroup</span> <span style="font-weight: bold; text-decoration: underline;">k</span> nil
  <span style="font-style: italic;">"Major mode for editing K code."</span>
  <span style="font-weight: bold;">:prefix</span> 'k
  <span style="font-weight: bold;">:group</span> 'languages)

<span style="font-weight: bold; font-style: italic;">;;;</span><span style="font-weight: bold; font-style: italic;">###</span><span style="font-weight: bold; font-style: italic;">autoload</span>
(<span style="font-weight: bold;">define-derived-mode</span> <span style="font-weight: bold;">k-mode</span> prog-mode <span style="font-style: italic;">"k"</span>
  <span style="font-style: italic;">"Major mode for editing K source code."</span>
  <span style="font-weight: bold;">:syntax-table</span> k--syntax-table
  <span style="font-weight: bold;">:group</span> 'k
  (<span style="font-weight: bold;">setq-local</span> font-lock-defaults k--token-syntax-types)
  (<span style="font-weight: bold;">setq-local</span> comment-start <span style="font-style: italic;">"/"</span>))

<span style="font-weight: bold; font-style: italic;">;;;</span><span style="font-weight: bold; font-style: italic;">###</span><span style="font-weight: bold; font-style: italic;">autoload</span>
(add-to-list 'auto-mode-alist '(<span style="font-style: italic;">"\\.k\\'"</span> . k-mode))
<span style="font-weight: bold; font-style: italic;">;;;</span><span style="font-weight: bold; font-style: italic;">###</span><span style="font-weight: bold; font-style: italic;">autoload</span>
(add-to-list 'interpreter-mode-alist '(<span style="font-style: italic;">"k"</span> . k-mode))
(<span style="font-weight: bold;">provide</span> '<span style="font-weight: bold; text-decoration: underline;">k-mode</span>)
</pre>
</div>

<p>
And given this <code>assembler.k</code> file:
</p>

<pre class="example">
/ basics
42 4.5 -4 10 /vector of numbers
1.2e3 /scientific notation
"x" /char
"abc" /string
(2;3;4) /list
a:42 /assign
{x+y} /lambda
f:{x+y}; f[2;3] /assign lambda and call it

/ tricky spacing
+ /x        /+ followed by commented out x
+/x         /sum x
</pre>

<p>
Generates this syntax highlighting:
</p>

<div class="org-src-container">
<pre class="src src-k">/ basics
42 4.5 -4 10 /vector of numbers
1.2e3 /scientific notation
"x" /char
"abc" /string
(2;3;4) /list
a:42 /assign
{x+y} /lambda
f:{x+y}; f[2;3] /assign lambda and call it

/ tricky spacing
+ /x        /+ followed by commented out x
+/x         /sum x
</pre>
</div>
</div>
</div>
<div id="outline-container-org60a5921" class="outline-2">
<h2 id="org60a5921">Next Steps</h2>
<div class="outline-text-2" id="text-org60a5921">
<p>
Most of the highlighting in the example so far came from the deriving mode (<code>prog-mode</code>).
I want to leverage as much of <code>prog-mode</code> as possible, so I will keep the existing colors for strings and numbers but extend the numbers to recognize <code>2e3</code> and such.
Then I will add support for null literals <code>0n</code> and <code>0N</code>.
</p>

<p>
Hopefully getting familiar with highlighting constant literal values will be good practice for highlighting functions, operators, and some of the tricky aspects of ngn/k like how <code>f /y</code> is <code>f</code> followed by a commented-out <code>y</code>, but <code>f/y</code> is <code>y</code> reduced by the binary function <code>f</code>.
</p>
</div>
</div>
</article>
    </main>

    <footer>
      <div class="license-container">
        <div class="license-item">Designed in 2024 by <a href="https://alexshroyer.com">Alex Shroyer</a></div>
        <div class="license-item"><a href="https://www.gnu.org/licenses/gpl-3.0.html">GNU GPLv3</a> (code)</div>
        <div class="license-item"><a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC-BY-4.0</a> (prose)</div>
      </div>
      <div class="social">
        <a href="https://stackoverflow.com/users/2037637/alex-shroyer" aria-label="my StackOverflow profile">
          <svg role="img" aria-hidden="true" class="svg-icon iconLogoGlyphMd native" width="32" height="37" viewBox="0 0 32 37">
            <path d="M26 33v-9h4v13H0V24h4v9h22z"></path>
            <path d="m21.5 0-2.7 2 9.9 13.3 2.7-2L21.5 0zM26 18.4 13.3 7.8l2.1-2.5 12.7 10.6-2.1 2.5zM9.1 15.2l15 7 1.4-3-15-7-1.4 3zm14 10.79.68-2.95-16.1-3.35L7 23l16.1 2.99zM23 30H7v-3h16v3z"></path>
          </svg>
        </a>
        <a href="https://github.com/hoosieree" aria-label="my GitHub profile">
          <svg role="img" height="32" aria-hidden="true" viewBox="0 0 16 16" version="1.1" width="32" data-view-component="true" class="octicon octicon-mark-github v-align-middle">
            <path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0016 8c0-4.42-3.58-8-8-8z"></path>
          </svg>
        </a>
        <a href="https://www.linkedin.com/in/alex-shroyer" aria-label="my LinkedIn profile"><svg role="img" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34" class="global-nav__logo">
            <g>
              <path d="M34,2.5v29A2.5,2.5,0,0,1,31.5,34H2.5A2.5,2.5,0,0,1,0,31.5V2.5A2.5,2.5,0,0,1,2.5,0h29A2.5,2.5,0,0,1,34,2.5ZM10,13H5V29h5Zm.45-5.5A2.88,2.88,0,0,0,7.59,4.6H7.5a2.9,2.9,0,0,0,0,5.8h0a2.88,2.88,0,0,0,2.95-2.81ZM29,19.28c0-4.81-3.06-6.68-6.1-6.68a5.7,5.7,0,0,0-5.06,2.58H17.7V13H13V29h5V20.49a3.32,3.32,0,0,1,3-3.58h.19c1.59,0,2.77,1,2.77,3.52V29h5Z" > </path>
            </g>
          </svg>
        </a>
      </div>
    </footer>
    <script>
      window.MathJax={
  tex:{ams:{multlineWidth:'85%'},tags:'ams',tagSide:'right',tagIndent:'.8em'},
  chtml:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  svg:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  output:{font:'mathjax-modern',displayOverflow:'overflow'}}
    </script>
  </body>
</html>
