<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="research and hobbies of a computer engineer">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/main.css">
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <title></title>
    <link rel="canonical" href="alexshroyer.com/2023-04-10-Pyhidra.html">
  </head>
  <body>
    <header>
      <nav>
        <a href="/index.html">Home</a>
        <a href="/pages/cv.html">CV</a>
        <a href="/pages/projects.html">Projects</a>
        <a href="/rss.xml">RSS</a>
      </nav>
    </header>
    <main>
      <article><h1 class="title">Pyhidra</h1><div class="abstract" id="org281a113">
<p>
Some notes about using <a href="https://github.com/dod-cyber-crime-center/pyhidra">pyhidra</a> for reverse engineering.
Some of this post is background on what reverse engineering is, but the focus is on pyhidra, an interface between Python and Ghidra.
</p>

</div>

<div id="outline-container-orgb11b0b1" class="outline-2">
<h2 id="orgb11b0b1">Reverse Engineering</h2>
<div class="outline-text-2" id="text-orgb11b0b1">
<p>
Reverse engineering (or RE) is the practice of obtaining information about an engineered system by examining it in the absence of any insider information.
Essentially, whenever you systematically try to understand how some bit of tech works - you are reverse engineering that tech.
</p>

<p>
Sometimes RE is the only way to obtain information about some sort of technology.
For example, some ancient tool discovered by archaeologists may not have any surviving documentation or writing to explain how it was used or how it works.
So, an archaeologist might build a replica and just try to use it, and use their judgement to attempt to derive how the tool worked in its time.
</p>

<p>
Similarly, software reverse engineering may require deriving some mental model for software whose authors have disappeared and for which there is insufficient documentation.
</p>

<p>
Another side of RE is about gathering intelligence on some tech without its creators knowing.
Sometimes companies try to reverse engineer their competitors' products in order to produce copies.
Security researchers may try to reverse engineer malware in order to understand what it is meant to accomplish.
</p>
</div>
</div>

<div id="outline-container-org958b610" class="outline-2">
<h2 id="org958b610">Ghidra</h2>
<div class="outline-text-2" id="text-org958b610">
<p>
In 2019, the NSA made their software reverse engineering suite <a href="https://www.ghidra-sre.org/">Ghidra</a> available to the public.
Prior to its release, other software reverse engineering tools existed, such as <a href="https://hex-rays.com/ida-pro/">IDA</a> and <a href="https://rada.re/n/">Radare</a>.
An inaccurate but useful stereotype is that professional reverse engineers working for larger companies use IDA, and hobbyists use Radare2.
However, Ghidra seems to be competitive with IDA, making it an interesting alternative for people with smaller RE budgets.
</p>
</div>
</div>

<div id="outline-container-org0aa370a" class="outline-2">
<h2 id="org0aa370a">Scripting</h2>
<div class="outline-text-2" id="text-org0aa370a">
<p>
Ghidra is a Java application, and can be scripted with Java or Jython (a Java-based Python interpreter).
However, Jython is not exactly the same as Cpython, so this is not ideal.
</p>

<p>
The first solution to this mismatch that I found is <a href="https://github.com/justfoxing/ghidra_bridge">Ghidra Bridge</a>, an open-source interface between Ghidra's Jython interpreter and your system's Cpython interpreter.
This allows regular Python scripts to access Ghidra's internal data structures as if you were using Jython from inside Ghidra.
</p>

<p>
Pyhidra is a newer solution to the same problem that seems to be also released by the US government, so perhaps it is more "official" than Ghidra Bridge.
I'm not exactly sure how Pyhidra works internally, but it feels like a "patched" Ghidra that works with normal Python.
</p>
</div>
</div>

<div id="outline-container-org50b15bc" class="outline-2">
<h2 id="org50b15bc">Python-in-Ghidra</h2>
<div class="outline-text-2" id="text-org50b15bc">
<p>
You can find example code using Ghidra's internal Jython environment, but these typically assume:
</p>

<ol class="org-ol">
<li>The Python interpreter is running inside of an already-open Ghidra project.</li>
<li>Certain global variables are already initialized for you.</li>
</ol>

<p>
For example, <a href="https://reverseengineering.stackexchange.com/a/23477">this answer</a> references the variables <code>currentProgram</code> and <code>monitor</code>, which do not exist in a standard Python program.
However, Ghidra defines them after you open a project.
</p>

<p>
From inside Ghidra, you can open the Pyhidra REPL and type in the following to print the instructions for all the basic blocks in a program:
</p>
<div class="org-src-container">
<pre class="src src-python"><span style="font-weight: bold;">from</span> ghidra.program.model.block <span style="font-weight: bold;">import</span> BasicBlockModel
<span style="font-weight: bold;">from</span> ghidra.util.task <span style="font-weight: bold;">import</span> TaskMonitor
<span style="font-weight: bold;">for</span> block <span style="font-weight: bold;">in</span> BasicBlockModel(currentProgram).getCodeBlocks(TaskMonitor.DUMMY):
 <span style="font-weight: bold;">for</span> inst <span style="font-weight: bold;">in</span> currentProgram.getListing().getInstructions(block, <span style="font-weight: bold; text-decoration: underline;">True</span>):
  <span style="font-weight: bold;">print</span>(inst.getAddressString(<span style="font-weight: bold; text-decoration: underline;">False</span>, <span style="font-weight: bold; text-decoration: underline;">True</span>), inst)
</pre>
</div>
</div>
</div>

<div id="outline-container-orgbe1e5b4" class="outline-2">
<h2 id="orgbe1e5b4">Pyhidra Outside Ghidra</h2>
<div class="outline-text-2" id="text-orgbe1e5b4">
<p>
To get the function call graph for every file in a large project, it would be more convenient to write a standalone script and not have to manually type commands into the Pyhidra REPL.
</p>

<p>
However, we need to obtain some special variables in a different way.
The <code>import ghidra</code> statement will fail without the proper prerequisite.
You have to first start Ghidra running in headless mode like this:
</p>
<div class="org-src-container">
<pre class="src src-python"><span style="font-weight: bold;">import</span> pyhidra
pyhidra.start()
<span style="font-weight: bold;">import</span> ghidra <span style="font-weight: bold; font-style: italic;">#</span><span style="font-weight: bold; font-style: italic;">this works now</span>
</pre>
</div>

<p>
But it's more convenient (and faster) to import the ghidra modules from inside a pyhidra context manager:
</p>
<div class="org-src-container">
<pre class="src src-python"><span style="font-weight: bold;">import</span> pyhidra
<span style="font-weight: bold;">def</span> <span style="font-weight: bold;">call_graphs</span>(program):
 <span style="font-weight: bold; font-style: italic;">calls</span> = {}
 <span style="font-weight: bold; font-style: italic;">called_by</span> = {}
 <span style="font-weight: bold;">with</span> pyhidra.open_program(program) <span style="font-weight: bold;">as</span> flat_api:
  <span style="font-weight: bold;">from</span> ghidra.util.task <span style="font-weight: bold;">import</span> TaskMonitor
  <span style="font-weight: bold; font-style: italic;">monitor</span> = TaskMonitor.DUMMY
  <span style="font-weight: bold; font-style: italic;">currentProgram</span> = flat_api.getCurrentProgram()
  <span style="font-weight: bold; font-style: italic;">funcs</span> = currentProgram.getFunctionManager().getFunctions(<span style="font-weight: bold; text-decoration: underline;">True</span>)
  <span style="font-weight: bold;">for</span> f <span style="font-weight: bold;">in</span> funcs:
   <span style="font-weight: bold; font-style: italic;">calls</span>[f] = f.getCalledFunctions(monitor)
   <span style="font-weight: bold; font-style: italic;">called_by</span>[f] = f.getCallingFunctions(monitor)

 <span style="font-weight: bold;">return</span> {<span style="font-style: italic;">'calls'</span>:calls,<span style="font-style: italic;">'called_by'</span>:called_by}
</pre>
</div>

<p>
The call to <code>pyhidra.start()</code> takes a few seconds on my machine, but only needs to run once.
Also, the <code>currentProgram</code> variable is self-explanatory, but Ghidra sets its value for you when you create a project.
For a standalone script, we set its value yourself:
</p>
<div class="org-src-container">
<pre class="src src-python"><span style="font-weight: bold;">import</span> pyhidra
<span style="font-weight: bold;">with</span> pyhidra.open_program(<span style="font-style: italic;">'a.out'</span>) <span style="font-weight: bold;">as</span> flat_api:
  <span style="font-weight: bold; font-style: italic;">currentProgram</span> = flat_api.getCurrentProgram()  <span style="font-weight: bold; font-style: italic;">#</span><span style="font-weight: bold; font-style: italic;">same currentProgram as in typical Ghidra examples</span>
  <span style="font-weight: bold; font-style: italic;">#</span><span style="font-weight: bold; font-style: italic;">currentProgram = flat_api.currentProgram  #same as above</span>
  <span style="font-weight: bold;">for</span> f <span style="font-weight: bold;">in</span> currentProgram.getFunctionManager().getFunctions(<span style="font-weight: bold; text-decoration: underline;">True</span>):
   <span style="font-weight: bold;">print</span>(f.getName())
</pre>
</div>
</div>
</div>

<div id="outline-container-orgc32dc37" class="outline-2">
<h2 id="orgc32dc37">Complete example</h2>
<div class="outline-text-2" id="text-orgc32dc37">
<p>
To demonstrate the workflow, start with a C program:
</p>
<div class="org-src-container">
<pre class="src src-c"><span style="font-weight: bold;">#include</span> <span style="font-style: italic;">&lt;stdio.h&gt;</span>
<span style="font-weight: bold;">#include</span> <span style="font-style: italic;">&lt;stdlib.h&gt;</span>
<span style="font-weight: bold;">#include</span> <span style="font-style: italic;">&lt;stddef.h&gt;</span>

<span style="font-weight: bold; text-decoration: underline;">int</span> <span style="font-weight: bold;">math</span>(<span style="font-weight: bold; text-decoration: underline;">int32_t</span> <span style="font-weight: bold; font-style: italic;">x</span>, <span style="font-weight: bold; text-decoration: underline;">float</span> <span style="font-weight: bold; font-style: italic;">y</span>) {<span style="font-weight: bold;">return</span> y &gt; 2 ? -x : x+4;}

<span style="font-weight: bold; text-decoration: underline;">int</span> <span style="font-weight: bold;">math2</span>(<span style="font-weight: bold; text-decoration: underline;">int</span> <span style="font-weight: bold; font-style: italic;">x</span>) {
  <span style="font-weight: bold; text-decoration: underline;">int</span> <span style="font-weight: bold; font-style: italic;">tmp</span> = math(x, 3.14159);
  <span style="font-weight: bold;">return</span> tmp-x;
}

<span style="font-weight: bold; text-decoration: underline;">int</span> <span style="font-weight: bold;">main</span>(<span style="font-weight: bold; text-decoration: underline;">int</span> <span style="font-weight: bold; font-style: italic;">argc</span>, <span style="font-weight: bold; text-decoration: underline;">char</span>**<span style="font-weight: bold; font-style: italic;">argv</span>){
  <span style="font-weight: bold;">if</span>(argc&lt;3){printf(<span style="font-style: italic;">"supply 2 number arguments please.\n"</span>);<span style="font-weight: bold;">return</span> 1;}
  <span style="font-weight: bold; text-decoration: underline;">int</span> <span style="font-weight: bold; font-style: italic;">x</span> = atoi(argv[1]);
  <span style="font-weight: bold; text-decoration: underline;">float</span> <span style="font-weight: bold; font-style: italic;">y</span> = atoi(argv[2]);
  printf(<span style="font-style: italic;">"Your number is %d\n"</span>, math(x, y));
  printf(<span style="font-style: italic;">"Some other number is %d\n"</span>, math2(x));
  <span style="font-weight: bold;">return</span> 0;
}
</pre>
</div>

<p>
I want to analyze <a href="https://www.fefe.de/dietlibc/">dietlibc</a>, so after installing diet I compile like this:
</p>
<div class="org-src-container">
<pre class="src src-shell">diet gcc example.c
</pre>
</div>

<p>
This results in an executable called <code>a.out</code> by default so that's the file I will analyze with pyhidra.
For example, one feature of interest is the function call graph.
Here's a way to extract a graph of functions in both directions (caller/callee):
</p>

<div class="org-src-container">
<pre class="src src-python"><span style="font-weight: bold;">import</span> pyhidra
<span style="font-weight: bold;">def</span> <span style="font-weight: bold;">call_graphs</span>(program):
 <span style="font-weight: bold; font-style: italic;">calls</span> = {}
 <span style="font-weight: bold; font-style: italic;">called_by</span> = {}
 <span style="font-weight: bold;">with</span> pyhidra.open_program(program) <span style="font-weight: bold;">as</span> flat_api:
  <span style="font-weight: bold;">from</span> ghidra.util.task <span style="font-weight: bold;">import</span> TaskMonitor
  <span style="font-weight: bold; font-style: italic;">monitor</span> = TaskMonitor.DUMMY
  <span style="font-weight: bold;">for</span> f <span style="font-weight: bold;">in</span> flat_api.currentProgram.getFunctionManager().getFunctions(<span style="font-weight: bold; text-decoration: underline;">True</span>):
   <span style="font-weight: bold; font-style: italic;">calls</span>[f] = f.getCalledFunctions(monitor)
   <span style="font-weight: bold; font-style: italic;">called_by</span>[f] = f.getCallingFunctions(monitor)

 <span style="font-weight: bold;">return</span> {<span style="font-style: italic;">'calls'</span>:calls,<span style="font-style: italic;">'called_by'</span>:called_by}

call_graphs(<span style="font-style: italic;">'a.out'</span>)
</pre>
</div>

<p>
And here's a way to iterate through the basic blocks:
</p>

<div class="org-src-container">
<pre class="src src-python"><span style="font-weight: bold;">import</span> pyhidra
<span style="font-weight: bold;">def</span> <span style="font-weight: bold;">block_graphs</span>(program):
 <span style="font-weight: bold;">with</span> pyhidra.open_program(program) <span style="font-weight: bold;">as</span> flat_api:
  <span style="font-weight: bold;">from</span> ghidra.program.model.block <span style="font-weight: bold;">import</span> BasicBlockModel
  <span style="font-weight: bold;">from</span> ghidra.util.task <span style="font-weight: bold;">import</span> TaskMonitor
  <span style="font-weight: bold;">for</span> b <span style="font-weight: bold;">in</span> BasicBlockModel(flat_api.currentProgram).getCodeBlocks(TaskMonitor.DUMMY):
   <span style="font-weight: bold;">print</span>(f<span style="font-style: italic;">'Label: </span>{b.name}<span style="font-style: italic;">'</span>)
   <span style="font-weight: bold;">print</span>(f<span style="font-style: italic;">'  min address: </span>{b.minAddress}<span style="font-style: italic;">'</span>)
   <span style="font-weight: bold;">print</span>(f<span style="font-style: italic;">'  max address: </span>{b.maxAddress}<span style="font-style: italic;">'</span>)

block_graphs(<span style="font-style: italic;">'a.out'</span>)
</pre>
</div>

<p>
The ability to construct function call graphs and basic block graphs opens new possibilities for static analysis.
</p>
</div>
</div>
</article>
    </main>

    <footer>
      <div class="license-container">
        <div class="license-item">Designed in 2024 by <a href="https://alexshroyer.com">Alex Shroyer</a></div>
        <div class="license-item"><a href="https://www.gnu.org/licenses/gpl-3.0.html">GNU GPLv3</a> (code)</div>
        <div class="license-item"><a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC-BY-4.0</a> (prose)</div>
      </div>
      <div class="social">
        <a href="https://stackoverflow.com/users/2037637/alex-shroyer" aria-label="my StackOverflow profile">
          <svg role="img" aria-hidden="true" class="svg-icon iconLogoGlyphMd native" width="32" height="37" viewBox="0 0 32 37">
            <path d="M26 33v-9h4v13H0V24h4v9h22z"></path>
            <path d="m21.5 0-2.7 2 9.9 13.3 2.7-2L21.5 0zM26 18.4 13.3 7.8l2.1-2.5 12.7 10.6-2.1 2.5zM9.1 15.2l15 7 1.4-3-15-7-1.4 3zm14 10.79.68-2.95-16.1-3.35L7 23l16.1 2.99zM23 30H7v-3h16v3z"></path>
          </svg>
        </a>
        <a href="https://github.com/hoosieree" aria-label="my GitHub profile">
          <svg role="img" height="32" aria-hidden="true" viewBox="0 0 16 16" version="1.1" width="32" data-view-component="true" class="octicon octicon-mark-github v-align-middle">
            <path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0016 8c0-4.42-3.58-8-8-8z"></path>
          </svg>
        </a>
        <a href="https://www.linkedin.com/in/alex-shroyer" aria-label="my LinkedIn profile"><svg role="img" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34" class="global-nav__logo">
            <g>
              <path d="M34,2.5v29A2.5,2.5,0,0,1,31.5,34H2.5A2.5,2.5,0,0,1,0,31.5V2.5A2.5,2.5,0,0,1,2.5,0h29A2.5,2.5,0,0,1,34,2.5ZM10,13H5V29h5Zm.45-5.5A2.88,2.88,0,0,0,7.59,4.6H7.5a2.9,2.9,0,0,0,0,5.8h0a2.88,2.88,0,0,0,2.95-2.81ZM29,19.28c0-4.81-3.06-6.68-6.1-6.68a5.7,5.7,0,0,0-5.06,2.58H17.7V13H13V29h5V20.49a3.32,3.32,0,0,1,3-3.58h.19c1.59,0,2.77,1,2.77,3.52V29h5Z" > </path>
            </g>
          </svg>
        </a>
      </div>
    </footer>
    <script>
      window.MathJax={
  tex:{ams:{multlineWidth:'85%'},tags:'ams',tagSide:'right',tagIndent:'.8em'},
  chtml:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  svg:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  output:{font:'mathjax-modern',displayOverflow:'overflow'}}
    </script>
  </body>
</html>
