<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="research and hobbies of a computer engineer">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/main.css">
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <title></title>
    <link rel="canonical" href="alexshroyer.com/2022-01-16-Endianness.html">
  </head>
  <body>
    <header>
      <nav>
        <a href="/index.html">Home</a>
        <a href="/pages/cv.html">CV</a>
        <a href="/pages/projects.html">Projects</a>
        <a href="/rss.xml">RSS</a>
      </nav>
    </header>
    <main>
      <article><h1 class="title">Endianness</h1><div class="abstract" id="org1363f13">
<p>
What is "endianness", and what are some examples?
</p>

</div>

<div id="outline-container-orgc32c4c7" class="outline-2">
<h2 id="orgc32c4c7">Etymology</h2>
<div class="outline-text-2" id="text-orgc32c4c7">
<p>
The term comes from the novel Gulliver's Travels, to distinguish between two types of people, based on whether they eat an egg starting at the "big end" or the "little end".
While the word was originally clearly meant to point out the ridiculous ways in which we humans create tribal divisions among ourselves, it has come to describe a useful mathematical concept of ordering a sequence of values.
</p>
</div>
</div>

<div id="outline-container-orge17a3e9" class="outline-2">
<h2 id="orge17a3e9">In Computing</h2>
<div class="outline-text-2" id="text-orge17a3e9">
<p>
In computer systems, byte order is one thing which can have an "endianness" property.
Bytes (groups of 8 bits) can represent 256 different values (00000000, 00000001, 00000010, etc.) such as the range 0 to 255 or the range -128 to +127.
But most computers natively support values of greater than a single byte, so it's necessary to put a group of bytes in some consistent order.
</p>

<p>
The order that maps directly to Arabic numerals is "big endian" (BE).
A <b>BE</b> number has its <i>most significant</i> bytes written on the left, and its <i>least significant</i> bytes on the right.
</p>

<p>
For example, the integer value 2050 can be written in hexadecimal format (0x802) or as a sequence of 2 hex values (0x08 0x02), or as two 8-bit sequences:
</p>

<pre class="example" id="org6456e48">
      0x08             0x02
┌───────────────┬───────────────┐
│0 0 0 0 1 0 0 0│0 0 0 0 0 0 1 0│
└───────────────┴───────────────┘
</pre>

<p>
But we could alternatively write this in a "little endian" (LE) format:
</p>
<pre class="example" id="orgab2693e">
      0x02             0x08
┌───────────────┬───────────────┐
│0 0 0 0 0 0 1 0│0 0 0 0 1 0 0 0│
└───────────────┴───────────────┘
</pre>

<p>
As long as the system is consistent, storing in either format is valid.
</p>

<p>
There may be good reasons to prefer one or the other, but if you must interface two systems, it's best if they both "speak" the same Endianness - otherwise you have to keep track of which one you're using, and convert back and forth between the two formats.
</p>
</div>

<div id="outline-container-orged81705" class="outline-3">
<h3 id="orged81705">Domain Names</h3>
<div class="outline-text-3" id="text-orged81705">
<p>
In a domain name, the top-level domain (TLD) could reasonably be considered the "most significant" part of the name.
The hostname comes before the TLD, and is probably the next most significant part.
Subdomains come before the hostname, separated by dot characters, and are less significant than the hostname.
However, pages within a domain or subdomain come <i>after</i> the TLD, and are separated by (usually) slashes.
</p>

<p>
For example, if we write a generic URL and try to number its logical components from most significant (0) to least (4), we might come up with this:
</p>
<pre class="example" id="orgc69a8b6">
proto://subdomain.domain.tld/page/subpage
            2       1     0   3    4
            v-------&lt;-----&lt;
            |
            &gt;-----------------&gt;----&gt;
</pre>
<p>
If this counts as an example of "endianness", then it's a strange one, due to the spiral pattern.
</p>
</div>
</div>
<div id="outline-container-orgf6d837e" class="outline-3">
<h3 id="orgf6d837e">File Paths</h3>
<div class="outline-text-3" id="text-orgf6d837e">
<p>
On Linux, MacOS, and Windows, the locations of specific files in a filesystem are given as "paths", and generally in BE format:
</p>

<ul class="org-ul">
<li><code>/usr/bin/python3</code> where <code>/usr</code> is higher in the hierarchy</li>
<li><code>C:\Documents\letter.docx</code> where <code>C:</code> is higher in the hierarcy</li>
</ul>

<p>
In hierarchical filesystems like these, the subfolders are written to the right of their containing folder, and individual files (not folders) are listed last.
</p>
</div>
</div>
</div>
<div id="outline-container-org4261eb7" class="outline-2">
<h2 id="org4261eb7">Outside Computing</h2>
<div class="outline-text-2" id="text-org4261eb7">
<p>
This concept may seem at first to only exist in this tiny computing-specific niche.
However, it actually appears in many different domains.
</p>
</div>

<div id="outline-container-org35af280" class="outline-3">
<h3 id="org35af280">Dates</h3>
<div class="outline-text-3" id="text-org35af280">
<p>
For example, dates are written in different formats depending sometimes on country and/or culture:
</p>
<table>


<colgroup>
<col  class="org-left">

<col  class="org-left">

<col  class="org-left">
</colgroup>
<tbody>
<tr>
<td class="org-left">yyyy-mm-dd</td>
<td class="org-left">China, Japan</td>
<td class="org-left">BE</td>
</tr>

<tr>
<td class="org-left">mm-dd-yyyy</td>
<td class="org-left">United States</td>
<td class="org-left">middle-endian (ME)</td>
</tr>

<tr>
<td class="org-left">dd-mm-yyyy</td>
<td class="org-left">Parts of Europe</td>
<td class="org-left">LE</td>
</tr>
</tbody>
</table>
</div>
</div>
<div id="outline-container-org493c9bd" class="outline-3">
<h3 id="org493c9bd">People's Names</h3>
<div class="outline-text-3" id="text-org493c9bd">
<p>
Most people I know (reflecting my cultural biases) have 2 or 3 names.
Their names are written left to right, with the leftmost one called the "first" name, and the rightmost called the "last" name.
Stretching the definition of "significant" to include "formality", the last name would be the most formal/significant, and so in the US, names are written in little endian format.
</p>

<p>
In some cultures, it is common to have more than one "last" name, and yet rarely use all of these names at once.
For example, someone may have one last name from each parent, and use both of these in "official" correspondence like birth certificates, but only use one in most other situations.
</p>

<p>
Other cultures use a more big endian ordering for their names, which can cause confusion with international teams, or whenever you try to make software that tries to make sense of human names.
</p>
</div>
</div>
<div id="outline-container-orgec7f2f8" class="outline-3">
<h3 id="orgec7f2f8">Places</h3>
<div class="outline-text-3" id="text-orgec7f2f8">
<p>
Again depending on culture, the names of places may be written in a particular BE or LE style.
The style I see used most often is LE.
</p>

<p>
For example:
</p>
<ul class="org-ul">
<li>Bloomington, IN (city, state)</li>
<li>Bloomington, IN, USA (city, state, country)</li>
<li>Oslo, Norway (city, country)</li>
</ul>

<p>
City names are often reused in different regions, so only mentioning "Bloomington" or even "Bloomington, USA" is ambiguous.
However, some especially large or famous cities (New York City, London, Tokyo) are globally unambiguous, because people will tacitly assume you mean the most famous one.
</p>
</div>
</div>
</div>
<div id="outline-container-org32bc3b1" class="outline-2">
<h2 id="org32bc3b1">Which is Better?</h2>
<div class="outline-text-2" id="text-org32bc3b1">
<p>
In theory, neither; in practice, it depends.
</p>

<p>
My opinion is that Little Endian is preferred in situations where the global context can be easily inferred.
For example, where I work there is a "campus mail" system.
Within this system, the planet, country, state, city, and approximate geographic location are all implied, so there is no reason to list any of them.
However, sending mail outside of this system requires more specificity.
</p>

<p>
One argument for little endian is that if you need to change between campus mail and worldwide mail, the only necessary change is to add a suffix to specify the state/country.
This reduces the burden of writing addresses because they become easier to change.
</p>

<p>
A very niche argument for little endian is that prefixes are easier to search for than suffixes.
So in systems like Integrated Development Environments (IDEs) naming variables with a common prefix will result in more false positives and require more typing, compared to if you name variables with a common suffix.
</p>

<p>
So names like these:
</p>
<ul class="org-ul">
<li>widgetController</li>
<li>widgetConstructor</li>
<li>widgetRemover</li>
</ul>

<p>
Are harder to search for compared to names like these:
</p>
<ul class="org-ul">
<li>controllerForWidget</li>
<li>constructorForWidget</li>
<li>removerForWidget</li>
</ul>

<p>
Granted, encoding this kind of information in variable names is probably not the best idea, but not all systems have namespaces or advanced type systems that more elegantly solve these problems.
</p>

<p>
Big Endian seems to be a better match for domains where speed is more important than accuracy, or where accuracy is more important than precision.
For example, if you want to approximate the sum of a set of numbers, you might pay more attention to the leading digits (and the length of the number) than the trailing digits.
Similarly, if you transmit information over a communication channel, it's good to put the important information first, in case the message is truncated or garbled over time.
</p>

<p>
This strategy is employed in subject lines in emails, headlines in newspapers, and abstracts in scientific writing.
</p>
</div>
</div>
</article>
    </main>

    <footer>
      <div class="license-container">
        <div class="license-item">Designed in 2024 by <a href="https://alexshroyer.com">Alex Shroyer</a></div>
        <div class="license-item"><a href="https://www.gnu.org/licenses/gpl-3.0.html">GNU GPLv3</a> (code)</div>
        <div class="license-item"><a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC-BY-4.0</a> (prose)</div>
      </div>
      <div class="social">
        <a href="https://stackoverflow.com/users/2037637/alex-shroyer" aria-label="my StackOverflow profile">
          <svg role="img" aria-hidden="true" class="svg-icon iconLogoGlyphMd native" width="32" height="37" viewBox="0 0 32 37">
            <path d="M26 33v-9h4v13H0V24h4v9h22z"></path>
            <path d="m21.5 0-2.7 2 9.9 13.3 2.7-2L21.5 0zM26 18.4 13.3 7.8l2.1-2.5 12.7 10.6-2.1 2.5zM9.1 15.2l15 7 1.4-3-15-7-1.4 3zm14 10.79.68-2.95-16.1-3.35L7 23l16.1 2.99zM23 30H7v-3h16v3z"></path>
          </svg>
        </a>
        <a href="https://github.com/hoosieree" aria-label="my GitHub profile">
          <svg role="img" height="32" aria-hidden="true" viewBox="0 0 16 16" version="1.1" width="32" data-view-component="true" class="octicon octicon-mark-github v-align-middle">
            <path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0016 8c0-4.42-3.58-8-8-8z"></path>
          </svg>
        </a>
        <a href="https://www.linkedin.com/in/alex-shroyer" aria-label="my LinkedIn profile"><svg role="img" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34" class="global-nav__logo">
            <g>
              <path d="M34,2.5v29A2.5,2.5,0,0,1,31.5,34H2.5A2.5,2.5,0,0,1,0,31.5V2.5A2.5,2.5,0,0,1,2.5,0h29A2.5,2.5,0,0,1,34,2.5ZM10,13H5V29h5Zm.45-5.5A2.88,2.88,0,0,0,7.59,4.6H7.5a2.9,2.9,0,0,0,0,5.8h0a2.88,2.88,0,0,0,2.95-2.81ZM29,19.28c0-4.81-3.06-6.68-6.1-6.68a5.7,5.7,0,0,0-5.06,2.58H17.7V13H13V29h5V20.49a3.32,3.32,0,0,1,3-3.58h.19c1.59,0,2.77,1,2.77,3.52V29h5Z" > </path>
            </g>
          </svg>
        </a>
      </div>
    </footer>
    <script>
      window.MathJax={
  tex:{ams:{multlineWidth:'85%'},tags:'ams',tagSide:'right',tagIndent:'.8em'},
  chtml:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  svg:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  output:{font:'mathjax-modern',displayOverflow:'overflow'}}
    </script>
  </body>
</html>
