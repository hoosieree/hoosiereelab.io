<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="research and hobbies of a computer engineer">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/main.css">
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <title></title>
    <link rel="canonical" href="alexshroyer.com/2021-10-13-Favorite-Language-Features.html">
  </head>
  <body>
    <header>
      <nav>
        <a href="/index.html">Home</a>
        <a href="/pages/cv.html">CV</a>
        <a href="/pages/projects.html">Projects</a>
        <a href="/rss.xml">RSS</a>
      </nav>
    </header>
    <main>
      <article><h1 class="title">Favorite Language Features</h1><div class="abstract" id="org7aba127">
<p>
A tour of some of my favorite programming language features.
</p>

</div>
<div id="outline-container-org4a69390" class="outline-2">
<h2 id="org4a69390">Function Trains</h2>
<div class="outline-text-2" id="text-org4a69390">
<p>
Sequences of functions in J form "trains" which determine how arguments flow from one function to another.
For 3 functions, it's called a "fork", and it works like this:
</p>
<div class="org-src-container">
<pre class="src src-j">NB. x and y are values, F G H are functions
x F G H y  NB. parsed as (x F y) G (x H y)
  F G H y  NB. parsed as (F y) G (H y)
</pre>
</div>
<p>
Sequences of 2 functions are called "hook", and they work like this:
</p>
<div class="org-src-container">
<pre class="src src-j">x F G y  NB. parsed as x F (G y)
  F G y  NB. parsed as y F (G y)
</pre>
</div>
<p>
Why have this in a language?
Why not just parse as a linear pipeline of functions?
For a long time I didn't have an answer to these questions, but now I think I do.
A recurring theme of the J language is to find the abstract patterns commonly used in computation and encode them in the language, and trains are one way to do that.
</p>

<p>
The most obvious example of "abstracting the pattern" is the fact that most functions in J operate on entire arrays.
The abstract pattern is "loop over each element of an array" and so the language handles this, rather than placing that burden on the programmer.
</p>

<p>
Other examples of J trying to allow the programmer to omit unnecessary detail abound.
For example, "power" operator (<code>F ^:N y</code>) applies the function F to its argument N times, feeding the result of the previous iteration in as the input to the next iteration.
</p>
<div class="org-src-container">
<pre class="src src-j">F =: -:  NB. halve
N =: 4
y =: 100 32 1024
F ^:N y  NB. halve y 4 times
</pre>
</div>
<p>
Without this feature, you could write the equivalent code by explicitly repeating the application yourself:
</p>
<div class="org-src-container">
<pre class="src src-j">F =: -:
y =: 100 32 1024
F (F (F (F y)))
</pre>
</div>
<p>
Or with a loop:
</p>
<div class="org-src-container">
<pre class="src src-j">F =: -:
y =: 100 32 1024
{{ yy =. y
for. i.4 do. yy =. F yy end. yy }} y
</pre>
</div>
<p>
But the handwritten form is tedious, and the loop obscures the meaning (repeat this action N times) with its assignment and control flow.
</p>
</div>
</div>

<div id="outline-container-orgdc361ee" class="outline-2">
<h2 id="orgdc361ee">REPL</h2>
<div class="outline-text-2" id="text-orgdc361ee">
<p>
The read-eval-print-loop, or REPL, is the thing I appreciate most in a language.
Interpreted languages usually rely on a REPL for interactive usage, but some ahead-of-time compiled languages have REPLs too.
</p>

<p>
The best REPL I've used to date is the JavaScript REPL embedded in Chromium-based browsers (such as Brave).
Second best is the ipython shell.
The feature they both share is intelligent history, so typing up-arrow doesn't give you the last <i>line</i> of a previously typed function, but the <i>entire</i> function.
Practically speaking, this means if you're experimenting with a multi-line function and want to make an edit, you can type up-arrow <b>once</b>, make your edit, then type enter to re-evaluate the whole function.
In this situation, simpler REPLs would require you to retype every expression of the function (or at least re-enter each line).
</p>
</div>
</div>
<div id="outline-container-org8bb5b23" class="outline-2">
<h2 id="org8bb5b23">Function Definition</h2>
<div class="outline-text-2" id="text-org8bb5b23">
<p>
In K, write an anonymous function with curly braces:
</p>
<div class="org-src-container">
<pre class="src src-k">{ x + y }
</pre>
</div>
<p>
K also uses <code>x</code> for the first argument and <code>y</code> for the second argument (or <code>z</code> for the third argument, not used in this example).
</p>
</div>
</div>
<div id="outline-container-orgddf6859" class="outline-2">
<h2 id="orgddf6859">Dictionary Construction</h2>
<div class="outline-text-2" id="text-orgddf6859">
<p>
Constructing a dictionary (an associative mapping from keys to values) in JavaScript is convenient, because the language automatically treats bare words inside the curly braces as symbols:
</p>
<div class="org-src-container">
<pre class="src src-js"><span style="font-weight: bold; font-style: italic;">//   </span><span style="font-weight: bold; font-style: italic;">key val  key val</span>
d = {foo:123, bar:456};
</pre>
</div>
<p>
These keys are accessible later using either <code>dict.key</code> or <code>dict['key']</code> notation:
</p>
<div class="org-src-container">
<pre class="src src-js">d.foo     <span style="font-weight: bold; font-style: italic;">// </span><span style="font-weight: bold; font-style: italic;">123</span>
d[<span style="font-style: italic;">'foo'</span>]  <span style="font-weight: bold; font-style: italic;">// </span><span style="font-weight: bold; font-style: italic;">123</span>
</pre>
</div>
<p>
By contrast, JSON requires quotes around the keys:
</p>
<div class="org-src-container">
<pre class="src src-json">{"foo":123, "bar":456}
</pre>
</div>
<p>
K lets you define dictionaires in a similar way (specify one (key, value) pair at a time), but it also allows you to specify all the keys <i>and then</i> all the values.
This alternative syntax is interesting to me because it encourages you to think of dicts as a list of keys and a list of values, rather than a list of (key,value) pairs.
</p>
<div class="org-src-container">
<pre class="src src-k">/   key key  val val
d: `foo`bar!(123;456)
</pre>
</div>
</div>
</div>
<div id="outline-container-org9634312" class="outline-2">
<h2 id="org9634312">Equality</h2>
<div class="outline-text-2" id="text-org9634312">
<p>
Many languages confusingly use <code>=</code> to mean assignment.
Thankfully, J takes a more sensible approach:
</p>
<div class="org-src-container">
<pre class="src src-J">   x = x + 1  NB. no it doesn't
0
</pre>
</div>
<p>
Other languages that also use <code>=</code> for equality include APL, K, SQL.
</p>
</div>
</div>
<div id="outline-container-org7bf9275" class="outline-2">
<h2 id="org7bf9275">Types</h2>
<div class="outline-text-2" id="text-org7bf9275">
<p>
In C, C++, Java, and others, type annotations are written with the <b>type</b> before the <b>name</b>.
Many modern languages reverse the order, by writing <b>name</b> then <b>type</b>.
First, to compare and contrast, here is C:
</p>
<div class="org-src-container">
<pre class="src src-c"><span style="font-weight: bold; text-decoration: underline;">void</span> <span style="font-weight: bold;">foo</span>(<span style="font-weight: bold; text-decoration: underline;">char</span> <span style="font-weight: bold; font-style: italic;">a</span>, <span style="font-weight: bold; text-decoration: underline;">double</span> <span style="font-weight: bold; font-style: italic;">b</span>){<span style="font-weight: bold;">return</span>;} <span style="font-weight: bold; font-style: italic;">/* </span><span style="font-weight: bold; font-style: italic;">function returning nothing, taking parameters of type char, then double</span><span style="font-weight: bold; font-style: italic;"> */</span>
</pre>
</div>
<p>
However, Go, Rust, and Scala all reverse the type/name ordering:
</p>
<div class="org-src-container">
<pre class="src src-rust">fn foo(a:i8, b:f64) -&gt; () {;}
</pre>
</div>
<p>
Haskell goes a step farther, separating the types from the names by putting them on different lines:
</p>
<div class="org-src-container">
<pre class="src src-haskell">foo :: Int -&gt; Float -&gt; ()
foo a b = ()
</pre>
</div>
<p>
Some systems go farther still, and put type information in a separate file.
The example of this that I'm most familiar with is json schema.
When the cost of reading the code is high (e.g. network or rpc applications), this can be helpful.
</p>

<p>
Of course, type <i>inference</i> is the best kind of type annotation.
</p>
</div>
</div>
<div id="outline-container-orgd1ab40c" class="outline-2">
<h2 id="orgd1ab40c">Function Application</h2>
<div class="outline-text-2" id="text-orgd1ab40c">
<p>
In Haskell, juxtaposition is function application:
</p>
<div class="org-src-container">
<pre class="src src-haskell">foo x = show x
foo "asdf"
</pre>
</div>
<p>
This is syntactically "quieter" than languages like C, Java and Python, which require parentheses:
</p>
<div class="org-src-container">
<pre class="src src-c">foo(<span style="font-style: italic;">"asdf"</span>);
</pre>
</div>
</div>
</div>
<div id="outline-container-org14ec432" class="outline-2">
<h2 id="org14ec432">Transpose</h2>
<div class="outline-text-2" id="text-org14ec432">
<p>
APL's symbol for transpose (<code>⍉</code>) really evokes the idea, in my opinion:
</p>
<div class="org-src-container">
<pre class="src src-apl">      ⍝ transpose two rows, 3 columns into 3 rows, 2 columns
      ⍉ 2 3 ⍴ 1 2 3 10 20 30
1 10
2 20
3 30
</pre>
</div>
</div>
</div>
<div id="outline-container-org5942078" class="outline-2">
<h2 id="org5942078">Destructuring Assignment</h2>
<div class="outline-text-2" id="text-org5942078">
<p>
Finally, some love for Python:
</p>
<div class="org-src-container">
<pre class="src src-python"><span style="font-weight: bold; font-style: italic;">a</span>,<span style="font-weight: bold; font-style: italic;">b</span>,<span style="font-weight: bold; font-style: italic;">c</span> = [10, 20, 30]
</pre>
</div>
</div>
</div>
<div id="outline-container-orgb029550" class="outline-2">
<h2 id="orgb029550">Links</h2>
<div class="outline-text-2" id="text-orgb029550">
<p>
If you're curious about the languages and other technologies mentioned in this post and want to learn more, check out some of these links:
</p>
<ul class="org-ul">
<li>apl: <a href="https://tryapl.org/">https://tryapl.org/</a></li>
<li>haskell: <a href="http://learnyouahaskell.com/">http://learnyouahaskell.com/</a></li>
<li>ipython: <a href="https://ipython.org/">https://ipython.org/</a></li>
<li>j: <a href="https://www.jsoftware.com/">https://www.jsoftware.com/</a></li>
<li>javascript: <a href="https://www.javascript.com/">https://www.javascript.com/</a></li>
<li>json schema: <a href="https://json-schema.org/">https://json-schema.org/</a></li>
<li>k: <a href="https://estradajke.github.io/k9-simples/k9/index.html">https://estradajke.github.io/k9-simples/k9/index.html</a></li>
<li>python: <a href="https://www.python.org/">https://www.python.org/</a></li>
</ul>
</div>
</div>
</article>
    </main>

    <footer>
      <div class="license-container">
        <div class="license-item">Designed in 2024 by <a href="https://alexshroyer.com">Alex Shroyer</a></div>
        <div class="license-item"><a href="https://www.gnu.org/licenses/gpl-3.0.html">GNU GPLv3</a> (code)</div>
        <div class="license-item"><a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC-BY-4.0</a> (prose)</div>
      </div>
      <div class="social">
        <a href="https://stackoverflow.com/users/2037637/alex-shroyer" aria-label="my StackOverflow profile">
          <svg role="img" aria-hidden="true" class="svg-icon iconLogoGlyphMd native" width="32" height="37" viewBox="0 0 32 37">
            <path d="M26 33v-9h4v13H0V24h4v9h22z"></path>
            <path d="m21.5 0-2.7 2 9.9 13.3 2.7-2L21.5 0zM26 18.4 13.3 7.8l2.1-2.5 12.7 10.6-2.1 2.5zM9.1 15.2l15 7 1.4-3-15-7-1.4 3zm14 10.79.68-2.95-16.1-3.35L7 23l16.1 2.99zM23 30H7v-3h16v3z"></path>
          </svg>
        </a>
        <a href="https://github.com/hoosieree" aria-label="my GitHub profile">
          <svg role="img" height="32" aria-hidden="true" viewBox="0 0 16 16" version="1.1" width="32" data-view-component="true" class="octicon octicon-mark-github v-align-middle">
            <path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0016 8c0-4.42-3.58-8-8-8z"></path>
          </svg>
        </a>
        <a href="https://www.linkedin.com/in/alex-shroyer" aria-label="my LinkedIn profile"><svg role="img" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34" class="global-nav__logo">
            <g>
              <path d="M34,2.5v29A2.5,2.5,0,0,1,31.5,34H2.5A2.5,2.5,0,0,1,0,31.5V2.5A2.5,2.5,0,0,1,2.5,0h29A2.5,2.5,0,0,1,34,2.5ZM10,13H5V29h5Zm.45-5.5A2.88,2.88,0,0,0,7.59,4.6H7.5a2.9,2.9,0,0,0,0,5.8h0a2.88,2.88,0,0,0,2.95-2.81ZM29,19.28c0-4.81-3.06-6.68-6.1-6.68a5.7,5.7,0,0,0-5.06,2.58H17.7V13H13V29h5V20.49a3.32,3.32,0,0,1,3-3.58h.19c1.59,0,2.77,1,2.77,3.52V29h5Z" > </path>
            </g>
          </svg>
        </a>
      </div>
    </footer>
    <script>
      window.MathJax={
  tex:{ams:{multlineWidth:'85%'},tags:'ams',tagSide:'right',tagIndent:'.8em'},
  chtml:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  svg:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  output:{font:'mathjax-modern',displayOverflow:'overflow'}}
    </script>
  </body>
</html>
