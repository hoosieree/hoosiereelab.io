<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="research and hobbies of a computer engineer">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/main.css">
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <title></title>
    <link rel="canonical" href="alexshroyer.com/2023-06-29-Bytecode-Indexing.html">
  </head>
  <body>
    <header>
      <nav>
        <a href="/index.html">Home</a>
        <a href="/pages/cv.html">CV</a>
        <a href="/pages/projects.html">Projects</a>
        <a href="/rss.xml">RSS</a>
      </nav>
    </header>
    <main>
      <article><h1 class="title">Bytecode Indexing</h1><div class="abstract" id="org71f7221">
<p>
A compact method of decoding bytecode.
</p>

</div>

<div id="outline-container-org4afa83f" class="outline-2">
<h2 id="org4afa83f">Bytecode</h2>
<div class="outline-text-2" id="text-org4afa83f">
<p>
Let's pretend we have a programming language in which mnemonics like <code>push</code> or <code>swap</code> map to small numbers like <code>2</code> and <code>5</code>.
We want to transform the human-written version of the language into the machine-readable numbers.
Here is the mapping of words-to-numbers:
</p>

<div class="org-src-container">
<pre class="src src-js">{
  <span style="font-style: italic;">'push'</span>: 0,
  <span style="font-style: italic;">'pop'</span>: 1,
  <span style="font-style: italic;">'dup'</span>: 3,
  <span style="font-style: italic;">'swap'</span>: 7,
  <span style="font-style: italic;">'sub'</span>: 11,
  <span style="font-style: italic;">'store'</span>: 15
}
</pre>
</div>

<p>
A small example program would look like <code>push pop sub</code>.
Obviously, a high level language compiler could use a lookup table exactly like this and call it a day.
But let's optimize it!
</p>
</div>
</div>

<div id="outline-container-org3113455" class="outline-2">
<h2 id="org3113455">Indexing</h2>
<div class="outline-text-2" id="text-org3113455">
<p>
Imagine for a moment that the numbers had no gaps:
</p>
<div class="org-src-container">
<pre class="src src-js">{
  <span style="font-style: italic;">'push'</span>: 0,
  <span style="font-style: italic;">'pop'</span>: 1,
  <span style="font-style: italic;">'dup'</span>: 2,
  <span style="font-style: italic;">'swap'</span>: 3,
  <span style="font-style: italic;">'sub'</span>: 4,
  <span style="font-style: italic;">'store'</span>: 5
}
</pre>
</div>

<p>
This would be much easier to optimize, because we could simply increment a counter while scanning through the words until we find a match:
</p>
<div class="org-src-container">
<pre class="src src-python"><span style="font-weight: bold; font-style: italic;">words</span> = [<span style="font-style: italic;">'push'</span>, <span style="font-style: italic;">'pop'</span>, <span style="font-style: italic;">'dup'</span>, <span style="font-style: italic;">'swap'</span>, <span style="font-style: italic;">'sub'</span>, <span style="font-style: italic;">'store'</span>]
<span style="font-weight: bold;">for</span> word <span style="font-weight: bold;">in</span> [<span style="font-style: italic;">'push'</span>, <span style="font-style: italic;">'pop'</span>, <span style="font-style: italic;">'dup'</span>, <span style="font-style: italic;">'sub'</span>]:  <span style="font-weight: bold; font-style: italic;">#</span><span style="font-weight: bold; font-style: italic;">example program</span>
  <span style="font-weight: bold;">print</span>(words.index(word))
  <span style="font-weight: bold; font-style: italic;"># </span><span style="font-weight: bold; font-style: italic;">0</span>
  <span style="font-weight: bold; font-style: italic;"># </span><span style="font-weight: bold; font-style: italic;">1</span>
  <span style="font-weight: bold; font-style: italic;"># </span><span style="font-weight: bold; font-style: italic;">2</span>
  <span style="font-weight: bold; font-style: italic;"># </span><span style="font-weight: bold; font-style: italic;">4</span>
</pre>
</div>

<p>
However, the actual numbers are unevenly spaced, but there is a workaround.
We can pad our list so the indexes line up correctly:
</p>
<div class="org-src-container">
<pre class="src src-python"><span style="font-weight: bold; font-style: italic;">words</span> = [<span style="font-style: italic;">'push'</span>, <span style="font-style: italic;">'pop'</span>, <span style="font-weight: bold; text-decoration: underline;">None</span>, <span style="font-style: italic;">'dup'</span>, <span style="font-weight: bold; text-decoration: underline;">None</span>, <span style="font-weight: bold; text-decoration: underline;">None</span>, <span style="font-weight: bold; text-decoration: underline;">None</span>, <span style="font-style: italic;">'swap'</span>,
         <span style="font-weight: bold; text-decoration: underline;">None</span>, <span style="font-weight: bold; text-decoration: underline;">None</span>, <span style="font-weight: bold; text-decoration: underline;">None</span>, <span style="font-style: italic;">'sub'</span>, <span style="font-weight: bold; text-decoration: underline;">None</span>, <span style="font-weight: bold; text-decoration: underline;">None</span>, <span style="font-weight: bold; text-decoration: underline;">None</span>, <span style="font-style: italic;">'store'</span>]
<span style="font-weight: bold;">for</span> word <span style="font-weight: bold;">in</span> [<span style="font-style: italic;">'push'</span>, <span style="font-style: italic;">'pop'</span>, <span style="font-style: italic;">'dup'</span>, <span style="font-style: italic;">'sub'</span>]:
  <span style="font-weight: bold;">print</span>(words.index(word))
  <span style="font-weight: bold; font-style: italic;"># </span><span style="font-weight: bold; font-style: italic;">0</span>
  <span style="font-weight: bold; font-style: italic;"># </span><span style="font-weight: bold; font-style: italic;">1</span>
  <span style="font-weight: bold; font-style: italic;"># </span><span style="font-weight: bold; font-style: italic;">3</span>
  <span style="font-weight: bold; font-style: italic;"># </span><span style="font-weight: bold; font-style: italic;">11</span>
</pre>
</div>
</div>
</div>

<div id="outline-container-org809b690" class="outline-2">
<h2 id="org809b690">Why?</h2>
<div class="outline-text-2" id="text-org809b690">
<p>
For general purpose code, of course you should not do this.
In Python, the <code>list.index</code> method has linear time complexity, but the dictionary lookup is constant time on average, and only linear in <i>worst</i> case.
Also, a small change to the bytecode could cause our padding to explode:
</p>
<div class="org-src-container">
<pre class="src src-js">{
  <span style="font-style: italic;">'push'</span>: 0,
  <span style="font-style: italic;">'pop'</span>: 1,
  <span style="font-style: italic;">'dup'</span>: 3,
  <span style="font-style: italic;">'swap'</span>: 7,
  <span style="font-style: italic;">'sub'</span>: 11,
  <span style="font-style: italic;">'store'</span>: 150
}
</pre>
</div>

<p>
Changing the opcode for <code>store</code> from <code>15</code> to <code>150</code> grows the padded list by almost 10x.
This technique is neither general nor does it have particularly good performance.
</p>

<p>
But there is one situation in which it is optimal.
Code golf.
</p>
</div>
</div>

<div id="outline-container-org1a44adf" class="outline-2">
<h2 id="org1a44adf">Size</h2>
<div class="outline-text-2" id="text-org1a44adf">
<p>
If run-time performance and code readability are not important, the indexing method can result in compact source code.
Note the extra blank spaces in the string, which become padding in the list after calling the <code>split</code> method:
</p>
<div class="org-src-container">
<pre class="src src-python"><span style="font-weight: bold;">for</span> word <span style="font-weight: bold;">in</span> [<span style="font-style: italic;">'push'</span>,<span style="font-style: italic;">'pop'</span>,<span style="font-style: italic;">'dup'</span>,<span style="font-style: italic;">'sub'</span>]:
 <span style="font-weight: bold;">print</span>(<span style="font-style: italic;">'push pop  dup    swap    sub    store'</span>.split(<span style="font-style: italic;">' '</span>).index(word))
</pre>
</div>

<p>
Contrast with the dictionary version (which is actually shorter):
</p>
<div class="org-src-container">
<pre class="src src-python"><span style="font-weight: bold;">for</span> word <span style="font-weight: bold;">in</span> [<span style="font-style: italic;">'push'</span>,<span style="font-style: italic;">'pop'</span>,<span style="font-style: italic;">'dup'</span>,<span style="font-style: italic;">'sub'</span>]:
 <span style="font-weight: bold;">print</span>({<span style="font-style: italic;">'push'</span>:0,<span style="font-style: italic;">'pop'</span>:1,<span style="font-style: italic;">'dup'</span>:3,<span style="font-style: italic;">'swap'</span>:7,<span style="font-style: italic;">'sub'</span>:11,<span style="font-style: italic;">'store'</span>:15}[word])
</pre>
</div>

<p>
You can also use the index of a word itself as an index into a list of all the bytecode values:
</p>
<div class="org-src-container">
<pre class="src src-python"><span style="font-weight: bold;">for</span> word <span style="font-weight: bold;">in</span> [<span style="font-style: italic;">'push'</span>,<span style="font-style: italic;">'pop'</span>,<span style="font-style: italic;">'dup'</span>,<span style="font-style: italic;">'sub'</span>]:
 <span style="font-weight: bold;">print</span>([0,1,3,7,11,15][<span style="font-style: italic;">'push pop dup swap sub store'</span>.split().index(word)])
</pre>
</div>

<p>
We could also look at unique prefixes of the words:
</p>
<pre class="example">
push  pu
pop   po
dup   du
swap  sw
sub   su
store st
</pre>

<p>
&#x2026;and calculate the appropriate indexes from those:
</p>
<div class="org-src-container">
<pre class="src src-python"><span style="font-weight: bold;">for</span> word <span style="font-weight: bold;">in</span> [<span style="font-style: italic;">'push'</span>,<span style="font-style: italic;">'pop'</span>,<span style="font-style: italic;">'dup'</span>,<span style="font-style: italic;">'sub'</span>]:
 <span style="font-weight: bold;">print</span>([0,1,3,7,11,15][<span style="font-style: italic;">'pupoduswsust'</span>.index(word[:2])//2])
</pre>
</div>

<p>
These examples so far are <i>perfect hash</i> functions.
First of all, they are hash functions because they turn a string input into a numeric output.
But because there is exactly one value for each key and no collisions, we can call them <i>perfect</i> hash functions.
</p>
</div>
</div>
</article>
    </main>

    <footer>
      <div class="license-container">
        <div class="license-item">Designed in 2024 by <a href="https://alexshroyer.com">Alex Shroyer</a></div>
        <div class="license-item"><a href="https://www.gnu.org/licenses/gpl-3.0.html">GNU GPLv3</a> (code)</div>
        <div class="license-item"><a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC-BY-4.0</a> (prose)</div>
      </div>
      <div class="social">
        <a href="https://stackoverflow.com/users/2037637/alex-shroyer" aria-label="my StackOverflow profile">
          <svg role="img" aria-hidden="true" class="svg-icon iconLogoGlyphMd native" width="32" height="37" viewBox="0 0 32 37">
            <path d="M26 33v-9h4v13H0V24h4v9h22z"></path>
            <path d="m21.5 0-2.7 2 9.9 13.3 2.7-2L21.5 0zM26 18.4 13.3 7.8l2.1-2.5 12.7 10.6-2.1 2.5zM9.1 15.2l15 7 1.4-3-15-7-1.4 3zm14 10.79.68-2.95-16.1-3.35L7 23l16.1 2.99zM23 30H7v-3h16v3z"></path>
          </svg>
        </a>
        <a href="https://github.com/hoosieree" aria-label="my GitHub profile">
          <svg role="img" height="32" aria-hidden="true" viewBox="0 0 16 16" version="1.1" width="32" data-view-component="true" class="octicon octicon-mark-github v-align-middle">
            <path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0016 8c0-4.42-3.58-8-8-8z"></path>
          </svg>
        </a>
        <a href="https://www.linkedin.com/in/alex-shroyer" aria-label="my LinkedIn profile"><svg role="img" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34" class="global-nav__logo">
            <g>
              <path d="M34,2.5v29A2.5,2.5,0,0,1,31.5,34H2.5A2.5,2.5,0,0,1,0,31.5V2.5A2.5,2.5,0,0,1,2.5,0h29A2.5,2.5,0,0,1,34,2.5ZM10,13H5V29h5Zm.45-5.5A2.88,2.88,0,0,0,7.59,4.6H7.5a2.9,2.9,0,0,0,0,5.8h0a2.88,2.88,0,0,0,2.95-2.81ZM29,19.28c0-4.81-3.06-6.68-6.1-6.68a5.7,5.7,0,0,0-5.06,2.58H17.7V13H13V29h5V20.49a3.32,3.32,0,0,1,3-3.58h.19c1.59,0,2.77,1,2.77,3.52V29h5Z" > </path>
            </g>
          </svg>
        </a>
      </div>
    </footer>
    <script>
      window.MathJax={
  tex:{ams:{multlineWidth:'85%'},tags:'ams',tagSide:'right',tagIndent:'.8em'},
  chtml:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  svg:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  output:{font:'mathjax-modern',displayOverflow:'overflow'}}
    </script>
  </body>
</html>
