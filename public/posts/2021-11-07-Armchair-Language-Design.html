<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="research and hobbies of a computer engineer">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/main.css">
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <title></title>
    <link rel="canonical" href="alexshroyer.com/2021-11-07-Armchair-Language-Design.html">
  </head>
  <body>
    <header>
      <nav>
        <a href="/index.html">Home</a>
        <a href="/pages/cv.html">CV</a>
        <a href="/pages/projects.html">Projects</a>
        <a href="/rss.xml">RSS</a>
      </nav>
    </header>
    <main>
      <article><h1 class="title">Armchair Language Design</h1><div class="abstract" id="orge5d2b1d">
<p>
Documenting features I like to see in a programming language.
</p>

</div>

<div id="outline-container-orgdbe57d5" class="outline-2">
<h2 id="orgdbe57d5">What is Language Design?</h2>
<div class="outline-text-2" id="text-orgdbe57d5">
<p>
Even though I've written a compiler for (a subset of) Racket, and I've written an RPN calculator that lets the user assign variables, I don't consider myself a programming language implementor.
I think to consider yourself a PL implementor, you need to also have <i>designed</i> the language.
Design is about tradeoffs and choices, and what you include is just as important as what you leave out.
</p>

<p>
So, what do I think is most important in a programming language?
The following are some goals and the associated values or tradeoffs that go along with them.
</p>
</div>
</div>

<div id="outline-container-org6c2f164" class="outline-2">
<h2 id="org6c2f164">Ease of Experimentation</h2>
<div class="outline-text-2" id="text-org6c2f164">
<p>
Having a bare-bones REPL is better than not having one, and having a really full-featured REPL is even better.
While it's possible to add REPL support to most languages (even C++ has <a href="https://root.cern/primer/#learn-c-at-the-root-prompt">ROOT</a>), the user experience is better if the language itself is designed with quick prototyping in mind.
</p>

<p>
At a fundamental level, this implies the language <b>prefers terseness to verbosity</b>.
</p>
</div>
</div>

<div id="outline-container-orgc143700" class="outline-2">
<h2 id="orgc143700">Values</h2>
<div class="outline-text-2" id="text-orgc143700">
<p>
The language should have numbers built-in.
This doesn't have to be as rich as J's <a href="https://code.jsoftware.com/wiki/Vocabulary/Constants">"constants" mini-language</a>, but it should support, at minimum:
</p>
<ul class="org-ul">
<li>booleans</li>
<li>integers</li>
<li>real values</li>
<li>ASCII characters</li>
<li>strings</li>
</ul>

<p>
This is sufficient for most languages, but I also think times and dates are fundamental enough to practical programming that they should be built-in to the language rather than provided as a library.
</p>
</div>
</div>

<div id="outline-container-org0d9546d" class="outline-2">
<h2 id="org0d9546d">Types</h2>
<div class="outline-text-2" id="text-org0d9546d">
<p>
Closely related to values is the concept of <i>types</i>, which are a way of constraining what values fit into certain categories.
I prefer a numeric hierarchy where booleans are 0 and 1, which means they're a subset of integers, and integers in turn are a subset of real values.
Mixing values at different positions on the numeric hierarchy should "promote when necessary, demote when possible".
The goal of this strategy is to be mathematically correct first and foremost, but also save space if possible.
</p>

<p>
Complex numbers or higher-dimension numberics (quaternions, octonions), while critical in some fields, are not critical enough to be part of the language, so they should be provided by libraries.
</p>

<p>
Datetimes could be part of the numeric hierarchy, but their rules are different enough to warrant being separate.
I think dates and times <i>should</i> be part of the language itself, to avoid having to convert among conflicting representations.
</p>

<p>
Textual values are as important as numbers in programming (and maybe more important than dates), so I want this language to painlessly support ASCII characters and strings (1d arrays) of them.
ASCII is the lowest common denominator though, and so not just ASCII, but full utf-8 support should be built in.
Unfortunately, this is much more complex than ASCII, and some operations which are cheap to perform on ASCII are dangerous or impossible to do across all of unicode.
A compromise may be to treat ASCII/UTF-8 similarly to bool/int/real: promote when necessary, demote when possible.
But this is outside of my comfort zone, so I fully expect there to be edge cases that complicate this idea.
</p>
</div>
</div>

<div id="outline-container-orgd45b7a2" class="outline-2">
<h2 id="orgd45b7a2">Composite Types</h2>
<div class="outline-text-2" id="text-orgd45b7a2">
<p>
The most essential composite type is the <b>array</b>.
This language should be quite array-oriented, and support the basic operations that users of APL, J, and K take for granted, such as arithmetic operations that work on all mixtures of scalar and array arguments.
</p>

<p>
Specifically, the language should transparently support, at minimum:
</p>
<div class="org-src-container">
<pre class="src src-j">  1 + 2 3 4  NB. scalar + array
3 4 5
  10 20 30 + 10 20 30  NB. array + array
20 40 60
</pre>
</div>

<p>
For this language, an <b>array</b> is defined as a collection of 0 or more elements, arranged along 0 or more dimensions, where each element has the same type.
Further, these elements are stored contiguously, which means arrays support random access to their elements.
</p>

<p>
This type of array is sometimes called "flat", as opposed to "nested", because it is a single container, rather than a container of containers.
</p>

<p>
Since the elements are not nested, a list of dimensions fully describes the shape of this type of array.
For example, a matrix with 2 rows and 3 columns would have shape <code>(2 3)</code>.
</p>

<p>
This type of array can be represented compactly in memory, and many operations on such arrays can be made fast.
</p>

<p>
Now, while it's certainly possible to program using only arrays, there is one more composite data type that makes life a lot more pleasant: <b>associative arrays</b> (also known as dictionaries).
This data type should support keys which are (nearly) any value.
In Python, keys must be <i>hashable</i> and comparable by value.
These restrictions allow the implementation to be reasonably fast, so they're tolerable.
In practice, most people use either strings or integers as dictionary keys, although I have used tuples once or twice.
Being able to look up a value based on a programmatically-generated key is extremely useful.
</p>
</div>
</div>

<div id="outline-container-orgff20516" class="outline-2">
<h2 id="orgff20516">Memory Management</h2>
<div class="outline-text-2" id="text-orgff20516">
<p>
Memory management should be as automatic as possible.
I expand the domain of "memory management" to include things like open file handles and database connections, because the real pain of memory management is that you must remember to <code>free</code> these resources when you're done using them.
</p>

<p>
While Python has the <code>with</code> keyword and ContextManagers to automatically free resources, Rust is actually much better about this because its lifetime rules can ensure a program which forgets to clean up after itself won't compile.
</p>
</div>
</div>

<div id="outline-container-org8652d26" class="outline-2">
<h2 id="org8652d26">Modularity vs. Extensibility</h2>
<div class="outline-text-2" id="text-org8652d26">
<p>
For me, being able to extend the language to suit my particular needs is not as important as being able to reuse functionality across different projects.
While I have experienced the lispy epiphany of being able to extend the language with code that's indistinguishable from the "host" language, I honestly was not all that impressed.
To me, being able to write my own functions that look like the built-in functions is not a huge selling point of a language.
</p>

<p>
On the other hand, I very much appreciate modules that allow me to reuse functionality that someone else wrote.
Maybe the best examples of this would be things like xml or html parsing.
These are commonly-used formats that I have to interact with sometimes, but they're also complex formats with lots of edge cases and quirks.
I'd much rather use a battle-tested library for parsing these types of data than to attempt to write my own.
Granted, I can probably write my own for a particular subset of xml, but I will definitely forget some edge cases that are rare in my experience but not rare in the grand scheme of all xml that I may encounter in the wild.
</p>

<p>
So while extensibility of the language and modularity are somewhat orthogonal, I think they can be lumped together in some ways because they're both dealing with adapting to problems beyond what the language was ostensibly designed to solve.
</p>

<p>
And if I have to pick one or the other, I would pick modularity.
</p>

<p>
As a consequence of this preference, my preferred language does not need a macro system, hygenic or otherwise.
However, it <i>does</i> need a module system for importing (and exporting) code to share between codebases.
</p>
</div>
</div>
<div id="outline-container-org13a9852" class="outline-2">
<h2 id="org13a9852">Performance</h2>
<div class="outline-text-2" id="text-org13a9852">
<p>
It should be fast.
</p>
</div>
</div>
</article>
    </main>

    <footer>
      <div class="license-container">
        <div class="license-item">Designed in 2024 by <a href="https://alexshroyer.com">Alex Shroyer</a></div>
        <div class="license-item"><a href="https://www.gnu.org/licenses/gpl-3.0.html">GNU GPLv3</a> (code)</div>
        <div class="license-item"><a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC-BY-4.0</a> (prose)</div>
      </div>
      <div class="social">
        <a href="https://stackoverflow.com/users/2037637/alex-shroyer" aria-label="my StackOverflow profile">
          <svg role="img" aria-hidden="true" class="svg-icon iconLogoGlyphMd native" width="32" height="37" viewBox="0 0 32 37">
            <path d="M26 33v-9h4v13H0V24h4v9h22z"></path>
            <path d="m21.5 0-2.7 2 9.9 13.3 2.7-2L21.5 0zM26 18.4 13.3 7.8l2.1-2.5 12.7 10.6-2.1 2.5zM9.1 15.2l15 7 1.4-3-15-7-1.4 3zm14 10.79.68-2.95-16.1-3.35L7 23l16.1 2.99zM23 30H7v-3h16v3z"></path>
          </svg>
        </a>
        <a href="https://github.com/hoosieree" aria-label="my GitHub profile">
          <svg role="img" height="32" aria-hidden="true" viewBox="0 0 16 16" version="1.1" width="32" data-view-component="true" class="octicon octicon-mark-github v-align-middle">
            <path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0016 8c0-4.42-3.58-8-8-8z"></path>
          </svg>
        </a>
        <a href="https://www.linkedin.com/in/alex-shroyer" aria-label="my LinkedIn profile"><svg role="img" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34" class="global-nav__logo">
            <g>
              <path d="M34,2.5v29A2.5,2.5,0,0,1,31.5,34H2.5A2.5,2.5,0,0,1,0,31.5V2.5A2.5,2.5,0,0,1,2.5,0h29A2.5,2.5,0,0,1,34,2.5ZM10,13H5V29h5Zm.45-5.5A2.88,2.88,0,0,0,7.59,4.6H7.5a2.9,2.9,0,0,0,0,5.8h0a2.88,2.88,0,0,0,2.95-2.81ZM29,19.28c0-4.81-3.06-6.68-6.1-6.68a5.7,5.7,0,0,0-5.06,2.58H17.7V13H13V29h5V20.49a3.32,3.32,0,0,1,3-3.58h.19c1.59,0,2.77,1,2.77,3.52V29h5Z" > </path>
            </g>
          </svg>
        </a>
      </div>
    </footer>
    <script>
      window.MathJax={
  tex:{ams:{multlineWidth:'85%'},tags:'ams',tagSide:'right',tagIndent:'.8em'},
  chtml:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  svg:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  output:{font:'mathjax-modern',displayOverflow:'overflow'}}
    </script>
  </body>
</html>
