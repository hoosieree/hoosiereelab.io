<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="research and hobbies of a computer engineer">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/main.css">
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <title></title>
    <link rel="canonical" href="alexshroyer.com/2024-10-08-FloatVer-Hardware-Acceleration.html">
  </head>
  <body>
    <header>
      <nav>
        <a href="/index.html">Home</a>
        <a href="/pages/cv.html">CV</a>
        <a href="/pages/projects.html">Projects</a>
        <a href="/rss.xml">RSS</a>
      </nav>
    </header>
    <main>
      <article><h1 class="title">FloatVer Hardware Acceleration</h1><div class="abstract" id="org11d31c2">
<p>
A hardware-accelerated alternative to SemVer.
</p>

</div>


<div id="outline-container-org20b32fb" class="outline-2">
<h2 id="org20b32fb">Version System Hardware Acceleration</h2>
<div class="outline-text-2" id="text-org20b32fb">
<p>
Version systems do not need hardware acceleration.
So here is a versioning system amenable to hardware acceleration.
</p>

<p>
Since FloatVer is a subset of IEEE754 32-bit floating point numbers, it is also inherently hardware-accelerated by most compute platforms (including CPU, GPU, and TPU hardware).
</p>
</div>

<div id="outline-container-org8bd1c1f" class="outline-3">
<h3 id="org8bd1c1f">Advantages of FloatVer</h3>
<div class="outline-text-3" id="text-org8bd1c1f">
<ul class="org-ul">
<li>hardware support</li>
<li>programming language support</li>
<li>serialization format support</li>
</ul>
</div>
</div>

<div id="outline-container-org8bc7d8a" class="outline-3">
<h3 id="org8bc7d8a">Disadvantages of FloatVer</h3>
<div class="outline-text-3" id="text-org8bc7d8a">
<ul class="org-ul">
<li>IEEE754 floats do not include all Real numbers</li>
<li>languages may mask rounding errors (e.g. in Python, <code>0.10000000000000001</code> silently rounds to <code>0.1</code>)</li>
<li>possible to conflate with non-version floats</li>
</ul>
</div>
</div>

<div id="outline-container-org3be9870" class="outline-3">
<h3 id="org3be9870">Why 32 bits?</h3>
<div class="outline-text-3" id="text-org3be9870">
<p>
In practice the most common sizes of floating-point values are 32 or 64 bits, however some platforms support 16-bit, 8-bit, or other sizes for their floats.
This document proposes 32 bits as a compromise between size, portability, and expressiveness.
It should be rare for any software to need more than ~2 billion versions (half of the ~4 billion distinct 32 bits values because we discard the negative numbers), and 16 bits would probably be enough for nearly every software project ever written, but float16 is less common in hardware than float32.
</p>
</div>
</div>
</div>

<div id="outline-container-org2d00059" class="outline-2">
<h2 id="org2d00059">Discussion</h2>
<div class="outline-text-2" id="text-org2d00059">
<p>
The key advantage of FloatVer is mechanical sympathy: it is natively supported by most programming languages and hardware.
On the other hand, SemVer is popular because it provides value as a <a href="https://v5.chriskrycho.com/elsewhere/cutting-edge-of-versioning/">communication tool</a>.
Given these different strengths, when would FloatVer be a better choice than SemVer?
</p>

<ol class="org-ol">
<li>when you have a huge amount of version numbers to compare and need hardware acceleration</li>
<li>when you have many different "flavors" of SemVer such that comparing two versions requires two parsers</li>
<li>for new projects that do not already have any existing ties to a particular flavor of SemVer</li>
<li>for versions starting with <code>0.</code>, where FloatVer is already a subset of SemVer</li>
</ol>

<p>
But when is SemVer a better choice than FloatVer?
</p>

<ol class="org-ol">
<li>when you already use (the same flavor of) SemVer and so do all the other sub-projects in your project</li>
<li>when FloatVer would be so controversial that you'd lose more money wasting precious engineering time in meetings debating the merits of versioning than you'd gain by its computational efficiency</li>
</ol>

<p>
How does FloatVer work as a <i>communication tool</i>?
Let's use an example to demonstrate.
</p>

<p>
Your company has a software product called Foo, which uses FloatVer and is currently version 3.14 (no relation to π).
The developers have some additional features so the next logical version could be 3.15.
However, they feel like 3.15 is a "big" difference from 3.14 and the new features are minor.
So instead the new version is 3.141, which implies that there are 9 more similarly-sized additions before upgrading to 3.15 feels appropriate.
</p>

<p>
If the same team used SemVer, they might instead make a version bump from 3.1.4 to 3.1.5 because the last number is for <code>PATCH</code> releases, which are less significant than <code>MINOR</code> releases.
Or, they might call this next version 3.1.4r1 (using the metadata I said I wouldn't talk about), indicating that this revision is even less significant of a change than a <code>PATCH</code> release.
</p>

<p>
As a communication tool, FloatVer and SemVer offer the same degrees of freedom in this scenario.
When deciding on a new backward-compatible FloatVer number, developers must balance the <i>length</i> of the number with the <i>granularity</i> of the change.
Incrementing the last digit with each revision (and sticking with decimal digits 0-9) means you only have 10 revisions until you must increment the next most significant digit.
But adding another digit makes the version number longer, which goes against a common preference for small, round numbers.
</p>

<p>
As a consumer of a product that uses FloatVer, what does the number tell you?
First, a large <code>BREAK</code> number implies the same thing that it does with SemVer - the product releases breaking changes frequently.
A <i>long</i> <code>NOBREAK</code> value implies that this version of the software has experienced many incremental but backward-compatible upgrades.
A <i>short</i> <code>NOBREAK</code> value could mean the software recently went through a significant milestone or that the software is young.
</p>

<p>
A FloatVer of <code>3.999001</code> might mean the developers <i>thought</i> they were nearly done but then changed their minds multiple times, and eventually hedged their bets by adding two zeros.
I think this is one unexpected area FloatVer shines as a communication tool.
Usually, software version numbers only tell a story when you have two or more of them to compare.
But here, a single number is able to convey some of the history of the project and the thought processes of its developers.
</p>

<p>
In conclusion, FloatVer offers simplicity, performance, and improved usability compared to SemVer.
</p>
</div>
</div>
</article>
    </main>

    <footer>
      <div class="license-container">
        <div class="license-item">Designed in 2024 by <a href="https://alexshroyer.com">Alex Shroyer</a></div>
        <div class="license-item"><a href="https://www.gnu.org/licenses/gpl-3.0.html">GNU GPLv3</a> (code)</div>
        <div class="license-item"><a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC-BY-4.0</a> (prose)</div>
      </div>
      <div class="social">
        <a href="https://stackoverflow.com/users/2037637/alex-shroyer" aria-label="my StackOverflow profile">
          <svg role="img" aria-hidden="true" class="svg-icon iconLogoGlyphMd native" width="32" height="37" viewBox="0 0 32 37">
            <path d="M26 33v-9h4v13H0V24h4v9h22z"></path>
            <path d="m21.5 0-2.7 2 9.9 13.3 2.7-2L21.5 0zM26 18.4 13.3 7.8l2.1-2.5 12.7 10.6-2.1 2.5zM9.1 15.2l15 7 1.4-3-15-7-1.4 3zm14 10.79.68-2.95-16.1-3.35L7 23l16.1 2.99zM23 30H7v-3h16v3z"></path>
          </svg>
        </a>
        <a href="https://github.com/hoosieree" aria-label="my GitHub profile">
          <svg role="img" height="32" aria-hidden="true" viewBox="0 0 16 16" version="1.1" width="32" data-view-component="true" class="octicon octicon-mark-github v-align-middle">
            <path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0016 8c0-4.42-3.58-8-8-8z"></path>
          </svg>
        </a>
        <a href="https://www.linkedin.com/in/alex-shroyer" aria-label="my LinkedIn profile"><svg role="img" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34" class="global-nav__logo">
            <g>
              <path d="M34,2.5v29A2.5,2.5,0,0,1,31.5,34H2.5A2.5,2.5,0,0,1,0,31.5V2.5A2.5,2.5,0,0,1,2.5,0h29A2.5,2.5,0,0,1,34,2.5ZM10,13H5V29h5Zm.45-5.5A2.88,2.88,0,0,0,7.59,4.6H7.5a2.9,2.9,0,0,0,0,5.8h0a2.88,2.88,0,0,0,2.95-2.81ZM29,19.28c0-4.81-3.06-6.68-6.1-6.68a5.7,5.7,0,0,0-5.06,2.58H17.7V13H13V29h5V20.49a3.32,3.32,0,0,1,3-3.58h.19c1.59,0,2.77,1,2.77,3.52V29h5Z" > </path>
            </g>
          </svg>
        </a>
      </div>
    </footer>
    <script>
      window.MathJax={
  tex:{ams:{multlineWidth:'85%'},tags:'ams',tagSide:'right',tagIndent:'.8em'},
  chtml:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  svg:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  output:{font:'mathjax-modern',displayOverflow:'overflow'}}
    </script>
  </body>
</html>
