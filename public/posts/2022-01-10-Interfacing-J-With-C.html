<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="research and hobbies of a computer engineer">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/main.css">
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <title></title>
    <link rel="canonical" href="alexshroyer.com/2022-01-10-Interfacing-J-With-C.html">
  </head>
  <body>
    <header>
      <nav>
        <a href="/index.html">Home</a>
        <a href="/pages/cv.html">CV</a>
        <a href="/pages/projects.html">Projects</a>
        <a href="/rss.xml">RSS</a>
      </nav>
    </header>
    <main>
      <article><h1 class="title">Interfacing J With C</h1><div class="abstract" id="org7763e89">
<p>
Some ways to call a dynamically linked library from J.
</p>

</div>
<div id="outline-container-org50562d5" class="outline-2">
<h2 id="org50562d5">Call a (Simple) Dynamic Library From J</h2>
<div class="outline-text-2" id="text-org50562d5">
<p>
These are notes I've taken while exploring this feature for my own enjoyment, and are not necessarily accurate or complete.
But, if you're in that in-between space where your needs have outgrown the basic uses of J's DLL features, but you're not quite an expert yet, then this page may help save you some time.
</p>
</div>

<div id="outline-container-org657896b" class="outline-3">
<h3 id="org657896b">A C Library</h3>
<div class="outline-text-3" id="text-org657896b">
<div class="org-src-container">
<pre class="src src-C"><span style="font-weight: bold; text-decoration: underline;">void</span> <span style="font-weight: bold;">foo</span>(<span style="font-weight: bold; text-decoration: underline;">int</span> *<span style="font-weight: bold; font-style: italic;">x</span>){x[0] += 3;}
<span style="font-weight: bold; text-decoration: underline;">int</span> <span style="font-weight: bold;">bar</span>(<span style="font-weight: bold; text-decoration: underline;">int</span> *<span style="font-weight: bold; font-style: italic;">a</span>){<span style="font-weight: bold;">return</span> a[1];}
<span style="font-weight: bold; text-decoration: underline;">int</span> <span style="font-weight: bold;">baz</span>(<span style="font-weight: bold; text-decoration: underline;">int</span> *<span style="font-weight: bold; font-style: italic;">a</span>, <span style="font-weight: bold; text-decoration: underline;">int</span> <span style="font-weight: bold; font-style: italic;">b</span>){<span style="font-weight: bold;">return</span> a[b];}
</pre>
</div>
</div>
</div>

<div id="outline-container-org8c879e1" class="outline-3">
<h3 id="org8c879e1">Build as a Dynamic Library</h3>
<div class="outline-text-3" id="text-org8c879e1">
<p>
Note: using MacOS in this example, so file extension is <code>.dylib</code>.
</p>
<div class="org-src-container">
<pre class="src src-bash">mkdir -p src/lib
gcc -c src/foobar.c -o src/lib/foobar.o
gcc -dynamiclib src/lib/foobar.o -o src/lib/libfoobar.dylib
</pre>
</div>
</div>
</div>

<div id="outline-container-org5c2774c" class="outline-3">
<h3 id="org5c2774c">Call From J</h3>
<div class="outline-text-3" id="text-org5c2774c">
<p>
The <code>cd</code> function returns a list of boxes.
The first item is the return value from the foreign function call.
The remaining values are the arguments which were given to <code>cd</code>.
This seemed pointless to me at first, until I realized that C can't return arrays.
Instead, you can pass a pointer to an array, modify the array inside the function, and examine the array after the function returns.
</p>

<p>
This "modify by reference" behavior is demonstrated by this first example:
</p>
<div class="org-src-container">
<pre class="src src-j">a =: &lt;0 1 2
'./src/lib/libfoobar.dylib foo n *i' cd a
</pre>
</div>

<pre class="example" id="org35311e6">
┌─┬─────┐
│0│3 1 2│
└─┴─────┘
</pre>

<p>
Interestingly, the J value <code>a</code> was not actually changed, indicating that J has a second copy.
Perhaps this is what <a href="https://www.jsoftware.com/help/jforc/calling_external_programs.htm">JfC</a> means by "adequate only for simple functions".
</p>

<div class="org-src-container">
<pre class="src src-j">a =: &lt;0 1 2
result =: './src/lib/libfoobar.dylib foo n *i' cd a
(a);&lt;result
</pre>
</div>

<pre class="example" id="orgc82327f">
┌───────┬─────────┐
│┌─────┐│┌─┬─────┐│
││0 1 2│││0│3 1 2││
│└─────┘│└─┴─────┘│
└───────┴─────────┘
</pre>


<p>
In the next example, the return value is a[1]:
</p>
<div class="org-src-container">
<pre class="src src-j">'./src/lib/libfoobar.dylib bar i *i' cd &lt;1 2 3
</pre>
</div>

<pre class="example" id="org61f5bbe">
┌─┬─────┐
│2│1 2 3│
└─┴─────┘
</pre>

<p>
Pass multiple arguments to <code>cd</code> as a list of boxes:
</p>
<div class="org-src-container">
<pre class="src src-j">'./src/lib/libfoobar.dylib baz i *i i' cd 1 2 3;2
</pre>
</div>

<pre class="example" id="org3d41852">
┌─┬─────┬─┐
│3│1 2 3│2│
└─┴─────┴─┘
</pre>
</div>
</div>
</div>

<div id="outline-container-org1ab2318" class="outline-2">
<h2 id="org1ab2318">Printing</h2>
<div class="outline-text-2" id="text-org1ab2318">
<div class="org-src-container">
<pre class="src src-C"><span style="font-weight: bold;">#include</span> <span style="font-style: italic;">&lt;stdio.h&gt;</span>
<span style="font-weight: bold; text-decoration: underline;">void</span> <span style="font-weight: bold;">print</span>(<span style="font-weight: bold; text-decoration: underline;">int</span>* <span style="font-weight: bold; font-style: italic;">x</span>, <span style="font-weight: bold; text-decoration: underline;">int</span> <span style="font-weight: bold; font-style: italic;">len</span>){printf(<span style="font-style: italic;">"( "</span>);<span style="font-weight: bold;">for</span>(<span style="font-weight: bold; text-decoration: underline;">int</span> <span style="font-weight: bold; font-style: italic;">i</span>=0;i&lt;len;++i){printf(<span style="font-style: italic;">"%d "</span>, x[i]);}printf(<span style="font-style: italic;">")\n"</span>);}
</pre>
</div>

<div class="org-src-container">
<pre class="src src-bash">gcc -c src/prnt.c -o src/lib/prnt.o
gcc -dynamiclib src/lib/prnt.o -o src/lib/libprnt.dylib
</pre>
</div>

<p>
The first line is printed by C, the remaining lines are the return value of the foreign function.
</p>
<div class="org-src-container">
<pre class="src src-j">'./src/lib/libprnt.dylib print n *i i' cd a;#a =: 2 4 6
</pre>
</div>

<pre class="example" id="orgf1047cf">
( 2 4 6 )
┌─┬─────┬─┐
│0│2 4 6│3│
└─┴─────┴─┘
</pre>
</div>
</div>

<div id="outline-container-org9a18113" class="outline-2">
<h2 id="org9a18113">Using C Structs</h2>
<div class="outline-text-2" id="text-org9a18113">
<p>
Documentation about how to send/receive data to a dll which uses C structs is scarce.
However, some J programmers pointed me in the right direction.
Thanks to Eric Iverson and "Tangentstorm" for their help!
</p>
</div>

<div id="outline-container-orge54e858" class="outline-3">
<h3 id="orge54e858">Begin with a C file with a struct datatype and a function:</h3>
<div class="outline-text-3" id="text-orge54e858">
<div class="org-src-container">
<pre class="src src-C"><span style="font-weight: bold;">struct</span> <span style="font-weight: bold; text-decoration: underline;">stuff</span> {<span style="font-weight: bold; text-decoration: underline;">double</span> <span style="font-weight: bold; font-style: italic;">a</span>[2]; <span style="font-weight: bold; text-decoration: underline;">int</span> <span style="font-weight: bold; font-style: italic;">i</span>;};
<span style="font-weight: bold; text-decoration: underline;">void</span> <span style="font-weight: bold;">fn</span>(<span style="font-weight: bold;">struct</span> <span style="font-weight: bold; text-decoration: underline;">stuff</span> *<span style="font-weight: bold; font-style: italic;">x</span>) {x-&gt;a[x-&gt;i] += 100;}
</pre>
</div>
</div>
</div>

<div id="outline-container-orgfebb5a6" class="outline-3">
<h3 id="orgfebb5a6">Compile it as a dynamically linked library:</h3>
<div class="outline-text-3" id="text-orgfebb5a6">
<div class="org-src-container">
<pre class="src src-bash">gcc -c src/s.c -o src/lib/s.o
gcc -dynamiclib src/lib/s.o  -o src/lib/stuff.dylib
</pre>
</div>
</div>
</div>

<div id="outline-container-org05b7689" class="outline-3">
<h3 id="org05b7689">Call it from J.</h3>
<div class="outline-text-3" id="text-org05b7689">
<p>
The big difference is that for simple types, J does conversions automatically.
But with structs, you have to do the conversions yourself using <code>fc</code> (float convert, <code>3!:5</code>) and <code>ic</code> (integer convert, <code>3!:4</code>).
These functions convert J floats or integers into a sequence of bytes, or reverse that conversion, depending on the left argument.
Basically, the entire struct will be serialized as a char array, and treated as a pointer to char (<code>*c</code>).
The last piece of the puzzle is to box the right argument of <code>cd</code>.
</p>
<div class="org-src-container">
<pre class="src src-j">a =: 2.3 2.4
i =: 1
s =: (2 fc a), 2 ic i
r =: './src/lib/stuff.dylib fn n *c' cd &lt;s
r =: &gt;1{r
(_2 fc 16{.r)  NB. the first 16 bytes of the struct contain double[2]
(_2 ic _4{.r)  NB. last 4 bytes contain int
</pre>
</div>

<pre class="example" id="org1aa6af3">
2.3 102.4

1
</pre>
</div>
</div>

<div id="outline-container-orga804e5f" class="outline-3">
<h3 id="orga804e5f">IMPORTANT NOTE!</h3>
<div class="outline-text-3" id="text-orga804e5f">
<p>
In this specific case, everything worked out correctly.
However, in general, compilers may add padding or alignment to structured data.
This depends on the data and compiler settings, and maybe other things such as the system's CPU architecture and operating system.
</p>

<p>
Bottom line: do not rely on this kind of simplistic serialization if you need reliable cross-platform code.
</p>
</div>
</div>
</div>
</article>
    </main>

    <footer>
      <div class="license-container">
        <div class="license-item">Designed in 2024 by <a href="https://alexshroyer.com">Alex Shroyer</a></div>
        <div class="license-item"><a href="https://www.gnu.org/licenses/gpl-3.0.html">GNU GPLv3</a> (code)</div>
        <div class="license-item"><a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC-BY-4.0</a> (prose)</div>
      </div>
      <div class="social">
        <a href="https://stackoverflow.com/users/2037637/alex-shroyer" aria-label="my StackOverflow profile">
          <svg role="img" aria-hidden="true" class="svg-icon iconLogoGlyphMd native" width="32" height="37" viewBox="0 0 32 37">
            <path d="M26 33v-9h4v13H0V24h4v9h22z"></path>
            <path d="m21.5 0-2.7 2 9.9 13.3 2.7-2L21.5 0zM26 18.4 13.3 7.8l2.1-2.5 12.7 10.6-2.1 2.5zM9.1 15.2l15 7 1.4-3-15-7-1.4 3zm14 10.79.68-2.95-16.1-3.35L7 23l16.1 2.99zM23 30H7v-3h16v3z"></path>
          </svg>
        </a>
        <a href="https://github.com/hoosieree" aria-label="my GitHub profile">
          <svg role="img" height="32" aria-hidden="true" viewBox="0 0 16 16" version="1.1" width="32" data-view-component="true" class="octicon octicon-mark-github v-align-middle">
            <path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0016 8c0-4.42-3.58-8-8-8z"></path>
          </svg>
        </a>
        <a href="https://www.linkedin.com/in/alex-shroyer" aria-label="my LinkedIn profile"><svg role="img" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34" class="global-nav__logo">
            <g>
              <path d="M34,2.5v29A2.5,2.5,0,0,1,31.5,34H2.5A2.5,2.5,0,0,1,0,31.5V2.5A2.5,2.5,0,0,1,2.5,0h29A2.5,2.5,0,0,1,34,2.5ZM10,13H5V29h5Zm.45-5.5A2.88,2.88,0,0,0,7.59,4.6H7.5a2.9,2.9,0,0,0,0,5.8h0a2.88,2.88,0,0,0,2.95-2.81ZM29,19.28c0-4.81-3.06-6.68-6.1-6.68a5.7,5.7,0,0,0-5.06,2.58H17.7V13H13V29h5V20.49a3.32,3.32,0,0,1,3-3.58h.19c1.59,0,2.77,1,2.77,3.52V29h5Z" > </path>
            </g>
          </svg>
        </a>
      </div>
    </footer>
    <script>
      window.MathJax={
  tex:{ams:{multlineWidth:'85%'},tags:'ams',tagSide:'right',tagIndent:'.8em'},
  chtml:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  svg:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  output:{font:'mathjax-modern',displayOverflow:'overflow'}}
    </script>
  </body>
</html>
