<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="research and hobbies of a computer engineer">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/main.css">
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <title></title>
    <link rel="canonical" href="alexshroyer.com/2022-08-16-Academic-Publishing.html">
  </head>
  <body>
    <header>
      <nav>
        <a href="/index.html">Home</a>
        <a href="/pages/cv.html">CV</a>
        <a href="/pages/projects.html">Projects</a>
        <a href="/rss.xml">RSS</a>
      </nav>
    </header>
    <main>
      <article><h1 class="title">Academic Publishing</h1><div class="abstract" id="org9af5cff">
<p>
Writing an academic article is one thing, getting it published is another.
Now that I have helped write 4 published papers, and assisted my peers with revising their own papers, I feel like I have some insider knowledge to share.
This is the kind of information I wish I had before starting my academic publishing journey.
</p>

</div>

<div id="outline-container-org9b7e8e3" class="outline-2">
<h2 id="org9b7e8e3">Types of Publishing</h2>
<div class="outline-text-2" id="text-org9b7e8e3">
<p>
You can make a big poster, a tutorial, articles of varying length, and others.
If you make a poster, I highly suggest watching <a href="https://www.youtube.com/watch?v=1RwJbhkCA58">this video</a> about the topic.
</p>
</div>
</div>
<div id="outline-container-org40f58e2" class="outline-2">
<h2 id="org40f58e2">Preparation</h2>
<div class="outline-text-2" id="text-org40f58e2">
<p>
Before you write an academic artifact of any kind (I'll use "paper" instead of artifact from here on) you should have something to write about.
This is the biggest part of the entire job.
For me, most steps are only barely visible in the finished paper:
</p>
<ul class="org-ul">
<li>finding relevant data sets</li>
<li>analyzing data sets that you thought might be relevant, but actually aren't</li>
<li>reading published literature about the topic, so you don't rehash something obvious</li>
<li>re-reading published literature, to make sure the thing you're studying is actually novel</li>
<li>coming up with a question/hypothesis</li>
<li>creating an experiment to test the hypothesis</li>
<li>getting results</li>
<li>evaluating those results correctly</li>
<li>learning about kinds of bias and accounting for them in your analysis</li>
</ul>
</div>
</div>
<div id="outline-container-orgc765e24" class="outline-2">
<h2 id="orgc765e24">Find a Venue</h2>
<div class="outline-text-2" id="text-orgc765e24">
<p>
Don't start writing just yet!
You should search for journals, conferences, workshops, or other venues to showcase your work.
Of course you can skip this step and self-publish online.
You can even enlist your friends or work peers to critique your work and incorporate their feedback into your final revision.
</p>

<p>
However, anonymous academic peer review has some good incentives which benefit science overall.
In academic publishing, the peer review process is both a filter (to ensure poor quality work is not published) and a refinement step (to improve the quality of existing work).
Your peers want the publication overall to be of the highest quality possible, either because they respect the publication or because their own work is associated with it and they want to protect their own reputations.
This encourages honest and meaningful feedback, regardless of whether your paper is ultimately accepted or rejected by the publication.
</p>

<p>
So that's <i>why</i> you should publish in an academic setting.
Next is to figure out where you want to submit your work.
You can ask your academic peers (teachers, advisors, other students) or check out where they have published recently for inspiration.
Next you can search for "Calls for Papers" from these venues.
</p>

<p>
One useful tool for publishing in engineering-related fields is IEEE's <a href="https://publication-recommender.ieee.org/">Publication Recommender</a>.
This lets you search periodicals and conferences by keyword, deadline, etc.
Another search tools is <a href="https://edas.info/listConferencesSubmit.php">EDAS</a>, which lets you search for conferences accepting submissions by keyword and filter by deadline, location, etc.
<a href="http://www.wikicfp.com/cfp/">WikiCFP</a> is another tool to search for "Calls for Papers".
Use your own personal network and tools like these to find a venue that closely matches your research topic.
</p>
</div>
</div>
<div id="outline-container-orge0ff7c5" class="outline-2">
<h2 id="orge0ff7c5">Write the Paper</h2>
<div class="outline-text-2" id="text-orge0ff7c5">
<p>
Do it.
</p>
</div>
</div>
<div id="outline-container-orgcdcbbc4" class="outline-2">
<h2 id="orgcdcbbc4">Submission</h2>
<div class="outline-text-2" id="text-orgcdcbbc4">
<p>
Once you've written your paper, you need to submit it to the periodical/conference or whatever venue you targeted.
This may imply:
</p>
<ul class="org-ul">
<li>formatting your paper according to a template</li>
<li>(if the review process is double-blind) anonymizing your paper</li>
<li>paying submission fees</li>
<li>signing over some copyrights to the publication</li>
</ul>

<p>
These steps may be done through the venue itself or a 3rd party like EasyChair or EDAS.
</p>
</div>
</div>
<div id="outline-container-org1f4d996" class="outline-2">
<h2 id="org1f4d996">Peer Review</h2>
<div class="outline-text-2" id="text-org1f4d996">
<p>
You'll get some feedback on your submission.
When you first read this, a natural reaction is to think the reviewer didn't read your paper because you're just <i>sure</i> that you addressed their concern.
So I would recommend reading the feedback, then taking a break to do something totally different - go for a walk, watch a movie, tidy up, etc.
Then come back to the feedback with fresh eyes and a perspective that the reviewers' only goal is to improve the publication as a whole.
</p>

<p>
Here are some scenarios outlining different types of reviewer feedback, and how you should respond to each.
</p>
</div>

<div id="outline-container-orgd05eeba" class="outline-3">
<h3 id="orgd05eeba">If the reviewer didn't understand something you wrote</h3>
<div class="outline-text-3" id="text-orgd05eeba">
<p>
Don't dismiss them, because they probably <i>tried</i> to understand it, but they don't have the same contextual knowledge as you.
Most reviewers are experts in their own field, but not necessarily in the niche you wrote about.
So try to provide enough context so that a smart reader can understand the essence of what you mean.
</p>
</div>
</div>

<div id="outline-container-org5098749" class="outline-3">
<h3 id="org5098749">If the reviewer suggests a specific paper</h3>
<div class="outline-text-3" id="text-org5098749">
<p>
Absoutely check out that paper, and reference it in your revised draft.
Maybe the paper is relevant to your work, and it will give you some new insights.
Or maybe the paper uses a technique that you rejected for some reasons - list those reasons.
In any case, reference the paper.
</p>
</div>
</div>

<div id="outline-container-orgab7c934" class="outline-3">
<h3 id="orgab7c934">If the reviewer asks for elaboration</h3>
<div class="outline-text-3" id="text-orgab7c934">
<p>
This is not the time to be stingy.
If a reviewer asks for more about some topic, it's probably because you didn't provide nearly enough.
One thing to keep in mind is that reviewers are probably a lot like you, with many things to do and limited time to do them.
So if they ask for <i>more</i>, you should take that request very seriously.
</p>
</div>
</div>

<div id="outline-container-orgaa8ffba" class="outline-3">
<h3 id="orgaa8ffba">If the reviewer wonders why you didn't consider <code>XYZ</code></h3>
<div class="outline-text-3" id="text-orgaa8ffba">
<p>
Either you did consider it but didn't think it was relevant, or (more likely) you didn't consider it.
This is a great opportunity to learn something new or reflect on the other information.
In any case, mention <code>XYZ</code> in your revised draft.
</p>
</div>
</div>
</div>

<div id="outline-container-org6f49156" class="outline-2">
<h2 id="org6f49156">Expenses</h2>
<div class="outline-text-2" id="text-org6f49156">
<p>
If you're a PhD student, ask your advisor about who covers costs for publishing, travel, lodging, etc.
If you're a professional, ask your boss.
Often, organizations budget for these things and there are funds available so you may not have to cover all these costs yourself.
</p>
</div>
</div>

<div id="outline-container-orgd0ed3d8" class="outline-2">
<h2 id="orgd0ed3d8">Advertising</h2>
<div class="outline-text-2" id="text-orgd0ed3d8">
<p>
In some sense, academic publishing is the advertising of the research you conducted.
But you can go one step further and promote your own publishing achievements.
After publishing, update your resume/CV/blog with your latest and greatest work.
</p>
</div>
</div>
</article>
    </main>

    <footer>
      <div class="license-container">
        <div class="license-item">Designed in 2024 by <a href="https://alexshroyer.com">Alex Shroyer</a></div>
        <div class="license-item"><a href="https://www.gnu.org/licenses/gpl-3.0.html">GNU GPLv3</a> (code)</div>
        <div class="license-item"><a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC-BY-4.0</a> (prose)</div>
      </div>
      <div class="social">
        <a href="https://stackoverflow.com/users/2037637/alex-shroyer" aria-label="my StackOverflow profile">
          <svg role="img" aria-hidden="true" class="svg-icon iconLogoGlyphMd native" width="32" height="37" viewBox="0 0 32 37">
            <path d="M26 33v-9h4v13H0V24h4v9h22z"></path>
            <path d="m21.5 0-2.7 2 9.9 13.3 2.7-2L21.5 0zM26 18.4 13.3 7.8l2.1-2.5 12.7 10.6-2.1 2.5zM9.1 15.2l15 7 1.4-3-15-7-1.4 3zm14 10.79.68-2.95-16.1-3.35L7 23l16.1 2.99zM23 30H7v-3h16v3z"></path>
          </svg>
        </a>
        <a href="https://github.com/hoosieree" aria-label="my GitHub profile">
          <svg role="img" height="32" aria-hidden="true" viewBox="0 0 16 16" version="1.1" width="32" data-view-component="true" class="octicon octicon-mark-github v-align-middle">
            <path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0016 8c0-4.42-3.58-8-8-8z"></path>
          </svg>
        </a>
        <a href="https://www.linkedin.com/in/alex-shroyer" aria-label="my LinkedIn profile"><svg role="img" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34" class="global-nav__logo">
            <g>
              <path d="M34,2.5v29A2.5,2.5,0,0,1,31.5,34H2.5A2.5,2.5,0,0,1,0,31.5V2.5A2.5,2.5,0,0,1,2.5,0h29A2.5,2.5,0,0,1,34,2.5ZM10,13H5V29h5Zm.45-5.5A2.88,2.88,0,0,0,7.59,4.6H7.5a2.9,2.9,0,0,0,0,5.8h0a2.88,2.88,0,0,0,2.95-2.81ZM29,19.28c0-4.81-3.06-6.68-6.1-6.68a5.7,5.7,0,0,0-5.06,2.58H17.7V13H13V29h5V20.49a3.32,3.32,0,0,1,3-3.58h.19c1.59,0,2.77,1,2.77,3.52V29h5Z" > </path>
            </g>
          </svg>
        </a>
      </div>
    </footer>
    <script>
      window.MathJax={
  tex:{ams:{multlineWidth:'85%'},tags:'ams',tagSide:'right',tagIndent:'.8em'},
  chtml:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  svg:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  output:{font:'mathjax-modern',displayOverflow:'overflow'}}
    </script>
  </body>
</html>
