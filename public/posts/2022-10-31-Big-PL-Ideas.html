<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="research and hobbies of a computer engineer">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/main.css">
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <title></title>
    <link rel="canonical" href="alexshroyer.com/2022-10-31-Big-PL-Ideas.html">
  </head>
  <body>
    <header>
      <nav>
        <a href="/index.html">Home</a>
        <a href="/pages/cv.html">CV</a>
        <a href="/pages/projects.html">Projects</a>
        <a href="/rss.xml">RSS</a>
      </nav>
    </header>
    <main>
      <article><h1 class="title">Big PL Ideas</h1><div class="abstract" id="org9b2170c">
<p>
Big ideas in programming languages, some of which are not yet mainstream.
</p>

</div>

<div id="outline-container-org094563d" class="outline-2">
<h2 id="org094563d">Arrays</h2>
<div class="outline-text-2" id="text-org094563d">
<p>
APL and some other array-oriented languages get arrays right - they are fundamental to the language and using them is easy and direct.
This ease is reflected in dedicated syntax for manipulating arrays in multiple ways.
As mentioned recently on <a href="https://www.arraycast.com/">The Array Cast</a>, most of the array languages have implicit mapping (<code>1 + 2 3 4</code> is <code>3 4 5</code>), but some fundamental operations in lisps have implicit reduction, which is why <code>(+ 1 2 3)</code> evaluates to <code>6</code>.
In my opinion, this also counts toward "getting arrays right", but I'm also biased in favor of APL-style notation.
</p>

<p>
The biggest surprise to me is that neither implicit mapping nor implicit reduction is mainstream yet.
Perhaps the closest mainstream analog to these ideas is NumPy's broadcasting behavior.
</p>
</div>
</div>

<div id="outline-container-org1e644c2" class="outline-2">
<h2 id="org1e644c2">Concurrency</h2>
<div class="outline-text-2" id="text-org1e644c2">
<p>
Some mainstream languages actually do build in concurrency to the language and its run-time environment.
<a href="https://en.wikipedia.org/wiki/JavaScript">JavaScript's</a> approach of having a global event loop to which you can register event handlers certainly counts.
Go's <a href="https://levelup.gitconnected.com/communicating-sequential-processes-csp-for-go-developer-in-a-nutshell-866795eb879d">CSP channels</a> handle concurrency differently, and <a href="https://www.erlang.org/">Erlang</a> or <a href="https://elixir-lang.org/">Elixir's</a> actor model are yet another approach.
But all of these examples build something into the language at a fundamental level, rather than forcing programmers to find and choose libraries to implement these features.
</p>

<p>
Unfortunately, most array languages focus solely on SIMD parallelism (one kind of concurrency) and ignore other kinds of concurrency.
Really my biggest complaint about array languages is that they are overtly batch-oriented, despite most of them being interactive and interpreted languages.
Asynchronous event handling is awkward at best in array languages.
</p>
</div>
</div>

<div id="outline-container-org89ea6d6" class="outline-2">
<h2 id="org89ea6d6">Resilience</h2>
<div class="outline-text-2" id="text-org89ea6d6">
<p>
Errors happen and specifications change over time.
Even a program which was once proven to be correct can eventually become incorrect.
The ability to modify parts of a long-running system without disturbing other parts is how I define resilient.
Similar to how your skin can heal a cut on your hand without requiring a full arm replacement, a resilient software system can heal one sub-part without replacing the larger part.
</p>

<p>
The next software system I hope to learn will be Elixir or Erlang, because of the facilities for resilience provided by the virtual machine they both use (<a href="https://www.erlang.org/blog/a-brief-beam-primer/">BEAM</a>).
From the pure programming language side of things, Erlang and Elixir aren't that interesting to me.
They're pure/functional and use pattern matching and recursion rather than state and loops, which is fine.
My brain prefers mapping and reduction over either looping or recursion, but I know that deep down they're all gotos, so whatever the top layer of syntax looks like, I am confident I will be able to use it to express my intent.
I expect the languages make some things easier but others harder relative to more mainstream imperative/procedural style languages.
But I'm comfortable enough with pure functional languages that I don't expect this to be a terrible learning curve.
</p>
</div>
</div>

<div id="outline-container-orgfc523d8" class="outline-2">
<h2 id="orgfc523d8">Parallelism</h2>
<div class="outline-text-2" id="text-orgfc523d8">
<p>
Here I'm referring to parallelism in the most general sense - redundant computing elements operating concurrently in a distributed system to solve parts of the same problem or even completely separate problems.
</p>

<p>
I'm currently reading <a href="https://dspace.mit.edu/bitstream/handle/1721.1/54635/603543210-MIT.pdf?sequence=2&amp;isAllowed=y">Alexey Radul's Ph.D thesis</a> about <i>propagation</i> which seems to be where we're all heading eventually.
In this design, stateful <i>cells</i> hold information and stateless <i>propagators</i> convey information among cells.
In a hardware system this could be realized by a processor-in-memory architecture, with RAM providing the <i>cell</i> with multiple nearby CPUs providing <i>propagation</i> in multiple directions.
Systolic arrays (like those used in <a href="https://en.wikipedia.org/wiki/Tensor_Processing_Unit">Tensor Processing Units</a>) are not exactly related, but achieve a similar result.
Chuck Moore's <a href="https://www.greenarraychips.com/">GreenArrays</a> company offers a "multi-computer chip" with no global clock in which independent computers act on input/output pins and communicate asynchronously with their immediate neighbors.
This seems like a hybrid between processor-in-memory and systolic array architectures.
</p>

<p>
A key part of Radul's thesis is that cells <i>accumulate</i> information rather than store results directly - why this is important isn't really clear to me yet, but I'll keep reading.
One thing I have come to see as fundamentally true is that all systems are gradually evolving into distributed systems.
Even the chips you can buy from Intel which are ostensibly "a CPU" are not one chip or even one type of chip, but many different types of chips sharing a common substrate and networked together to perform the more general task of being CPU-like.
</p>
</div>
</div>
</article>
    </main>

    <footer>
      <div class="license-container">
        <div class="license-item">Designed in 2024 by <a href="https://alexshroyer.com">Alex Shroyer</a></div>
        <div class="license-item"><a href="https://www.gnu.org/licenses/gpl-3.0.html">GNU GPLv3</a> (code)</div>
        <div class="license-item"><a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC-BY-4.0</a> (prose)</div>
      </div>
      <div class="social">
        <a href="https://stackoverflow.com/users/2037637/alex-shroyer" aria-label="my StackOverflow profile">
          <svg role="img" aria-hidden="true" class="svg-icon iconLogoGlyphMd native" width="32" height="37" viewBox="0 0 32 37">
            <path d="M26 33v-9h4v13H0V24h4v9h22z"></path>
            <path d="m21.5 0-2.7 2 9.9 13.3 2.7-2L21.5 0zM26 18.4 13.3 7.8l2.1-2.5 12.7 10.6-2.1 2.5zM9.1 15.2l15 7 1.4-3-15-7-1.4 3zm14 10.79.68-2.95-16.1-3.35L7 23l16.1 2.99zM23 30H7v-3h16v3z"></path>
          </svg>
        </a>
        <a href="https://github.com/hoosieree" aria-label="my GitHub profile">
          <svg role="img" height="32" aria-hidden="true" viewBox="0 0 16 16" version="1.1" width="32" data-view-component="true" class="octicon octicon-mark-github v-align-middle">
            <path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0016 8c0-4.42-3.58-8-8-8z"></path>
          </svg>
        </a>
        <a href="https://www.linkedin.com/in/alex-shroyer" aria-label="my LinkedIn profile"><svg role="img" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34" class="global-nav__logo">
            <g>
              <path d="M34,2.5v29A2.5,2.5,0,0,1,31.5,34H2.5A2.5,2.5,0,0,1,0,31.5V2.5A2.5,2.5,0,0,1,2.5,0h29A2.5,2.5,0,0,1,34,2.5ZM10,13H5V29h5Zm.45-5.5A2.88,2.88,0,0,0,7.59,4.6H7.5a2.9,2.9,0,0,0,0,5.8h0a2.88,2.88,0,0,0,2.95-2.81ZM29,19.28c0-4.81-3.06-6.68-6.1-6.68a5.7,5.7,0,0,0-5.06,2.58H17.7V13H13V29h5V20.49a3.32,3.32,0,0,1,3-3.58h.19c1.59,0,2.77,1,2.77,3.52V29h5Z" > </path>
            </g>
          </svg>
        </a>
      </div>
    </footer>
    <script>
      window.MathJax={
  tex:{ams:{multlineWidth:'85%'},tags:'ams',tagSide:'right',tagIndent:'.8em'},
  chtml:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  svg:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  output:{font:'mathjax-modern',displayOverflow:'overflow'}}
    </script>
  </body>
</html>
