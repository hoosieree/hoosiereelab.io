<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="research and hobbies of a computer engineer">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/main.css">
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <title></title>
    <link rel="canonical" href="alexshroyer.com/2021-10-26-I-Declare.html">
  </head>
  <body>
    <header>
      <nav>
        <a href="/index.html">Home</a>
        <a href="/pages/cv.html">CV</a>
        <a href="/pages/projects.html">Projects</a>
        <a href="/rss.xml">RSS</a>
      </nav>
    </header>
    <main>
      <article><h1 class="title">I Declare</h1><div class="abstract" id="org16c7880">
<p>
Separate syntax for declaration and assignment is a Good Thing<sup>TM</sup>.
</p>

</div>

<div id="outline-container-orgc400d9e" class="outline-2">
<h2 id="orgc400d9e">Unified Syntax</h2>
<div class="outline-text-2" id="text-orgc400d9e">
<p>
Some languages have a single syntax to either (a) define a variable for the first time or (b) assign a new value to an existing variable.
</p>

<p>
This "unified syntax" generally looks like this:
</p>

<div class="org-src-container">
<pre class="src src-python"><span style="font-weight: bold; font-style: italic;">x</span> = 2  <span style="font-weight: bold; font-style: italic;"># </span><span style="font-weight: bold; font-style: italic;">define a new variable with '='</span>
<span style="font-weight: bold; font-style: italic;">x</span> = 3  <span style="font-weight: bold; font-style: italic;"># </span><span style="font-weight: bold; font-style: italic;">re-assign an existing variable with '='</span>
</pre>
</div>

<p>
The above example uses Python, but many other languages also use a single notation to convey two subtly different things:
</p>
<ul class="org-ul">
<li>create a new variable</li>
<li>modify an existing variable</li>
</ul>

<p>
There are reasons to prefer a single notation for both of these concepts, such as:
</p>
<ul class="org-ul">
<li>less syntax to learn</li>
<li>to make these concepts "feel" more like one concept</li>
</ul>
</div>
</div>

<div id="outline-container-orgaa2c56a" class="outline-2">
<h2 id="orgaa2c56a">When Separate Syntax Helps</h2>
<div class="outline-text-2" id="text-orgaa2c56a">
<p>
On the other hand, requiring separate syntax for declaration and assignment can be helpful.
For example, in Python, the following is not an error, even though I think it <i>should</i> be caught as an error before the program is allowed to run:
</p>

<div class="org-src-container">
<pre class="src src-python"><span style="font-weight: bold; font-style: italic;">color</span> = <span style="font-style: italic;">'blue'</span>
<span style="font-weight: bold;">if</span> <span style="font-weight: bold; text-decoration: underline;">True</span>:
  <span style="font-weight: bold; font-style: italic;">colour</span> = <span style="font-style: italic;">'red'</span> <span style="font-weight: bold; font-style: italic;"># </span><span style="font-weight: bold; font-style: italic;">oops, used British spelling (added 'u')</span>

<span style="font-weight: bold;">print</span>(color)
</pre>
</div>

<pre class="example">
blue
</pre>


<p>
Python does not consider this a syntax error (even though a missspellled word is a syntactic mistake), or even a runtime error.
If you're lucky, this error will be caught by your tests.
If you're unlucky, it will be caught by your audience during a high-pressure demonstration.
</p>

<p>
This would be caught as an error if Python had separate syntax for declaring versus assigning variables.
We can work around this in Python, if we pay the price:
</p>

<div class="org-src-container">
<pre class="src src-python"><span style="font-weight: bold;">def</span> <span style="font-weight: bold;">define</span>(name, val):
  <span style="font-weight: bold; font-style: italic;">g</span> = <span style="font-weight: bold;">locals</span>()
  <span style="font-weight: bold;">if</span> name <span style="font-weight: bold;">in</span> g:
    <span style="font-weight: bold;">raise</span> <span style="font-weight: bold; text-decoration: underline;">NameError</span>
  <span style="font-weight: bold;">else</span>:
    <span style="font-weight: bold; font-style: italic;">g</span>[name] = val

<span style="font-weight: bold;">def</span> <span style="font-weight: bold;">assign</span>(name, val):
  <span style="font-weight: bold; font-style: italic;">g</span> = <span style="font-weight: bold;">locals</span>()
  <span style="font-weight: bold;">if</span> name <span style="font-weight: bold;">in</span> g:
    <span style="font-weight: bold; font-style: italic;">g</span>[name] = val
  <span style="font-weight: bold;">else</span>:
    <span style="font-weight: bold;">raise</span> <span style="font-weight: bold; text-decoration: underline;">NameError</span>

define(<span style="font-style: italic;">'color'</span>, <span style="font-style: italic;">'blue'</span>)
<span style="font-weight: bold;">if</span> <span style="font-weight: bold; text-decoration: underline;">True</span>:
  assign(<span style="font-style: italic;">'colour'</span>, <span style="font-style: italic;">'red'</span>)
<span style="font-weight: bold;">print</span>(color)
</pre>
</div>

<pre class="example" id="org77817bd">
Traceback (most recent call last):
  File "&lt;stdin&gt;", line 17, in &lt;module&gt;
  File "&lt;stdin&gt;", line 13, in assign
NameError
</pre>

<p>
Of course, it's much more typing to write <code>define</code> and <code>assign</code> than it is to write <code>=</code>, and the whole point of the exercise was to avoid simple mistakes.
But if it takes 20x more work to avoid the mistake, no one will want to use these safety features.
</p>
</div>
</div>

<div id="outline-container-org7e9ef02" class="outline-2">
<h2 id="org7e9ef02">Some Mistakes Not Worth Repeating</h2>
<div class="outline-text-2" id="text-org7e9ef02">
<p>
Statically typed languages like C and Java differentiate between declaration and assignment by the presence or absence of type annotations.
</p>
<div class="org-src-container">
<pre class="src src-C"><span style="font-weight: bold; text-decoration: underline;">int</span> <span style="font-weight: bold; font-style: italic;">x</span>; <span style="font-weight: bold; font-style: italic;">/* </span><span style="font-weight: bold; font-style: italic;">declaration</span><span style="font-weight: bold; font-style: italic;"> */</span>
<span style="font-weight: bold; text-decoration: underline;">int</span> <span style="font-weight: bold; font-style: italic;">y</span> = 0; <span style="font-weight: bold; font-style: italic;">/* </span><span style="font-weight: bold; font-style: italic;">combined declaration and assignment, or a "definition"</span><span style="font-weight: bold; font-style: italic;"> */</span>
y = 3; <span style="font-weight: bold; font-style: italic;">/* </span><span style="font-weight: bold; font-style: italic;">assignment only</span><span style="font-weight: bold; font-style: italic;"> */</span>
z = 10; <span style="font-weight: bold; font-style: italic;">/* </span><span style="font-weight: bold; font-style: italic;">error: z must be declared before it can be assigned</span><span style="font-weight: bold; font-style: italic;"> */</span>
</pre>
</div>

<p>
This notation is economically terse in one sense (no extra word in addition to the type) but ultimately is verbose because it requires type annotation for each variable declaration (and makes type inference difficult or impossible to add to the language).
</p>

<p>
Languages with type inference, such as Rust, have keyword syntax for declaring a variable without a type annotation:
</p>
<div class="org-src-container">
<pre class="src src-rust">let mut x = 3; // type of x inferred, omitting 'mut' makes x immutable
</pre>
</div>

<p>
Recent C++ has limited forms of type inference, which reduces a lot of the clutter, but still requires a keyword (<code>auto</code>):
</p>
<div class="org-src-container">
<pre class="src src-C++"><span style="font-weight: bold;">for</span> (<span style="font-weight: bold;">auto</span> <span style="font-weight: bold; font-style: italic;">i</span> : v) {...}
</pre>
</div>

<p>
Type inference is great, and C++ and Rust are moving in the right direction, but they would be even better if <code>let</code> or <code>auto</code> was not required.
</p>
</div>
</div>

<div id="outline-container-org5593f3a" class="outline-2">
<h2 id="org5593f3a">Better Alternatives</h2>
<div class="outline-text-2" id="text-org5593f3a">
<p>
The language <a href="https://mlochbaum.github.io/BQN/doc/syntax.html#assignment">BQN</a> has a nice solution to this problem.
It is a dynamically typed language and has no type annotation syntax, so it could borrow from JavaScript's <code>var</code> syntax to distinguish definition from modification.
Instead, it uses two different glyphs:
</p>

<div class="org-src-container">
<pre class="src src-bqn">x ← 1 # define
x ↩ 3 # change
</pre>
</div>

<p>
I find the ↩ glyph evokes its meaning ("change"), since it reminds me of a browser's refresh icon.
</p>

<p>
For a language whose variables don't have special attributes like type or storage class, I think this 2-glyph solution is optimal.
On the other hand, if a language supports other variable attributes, such as mutable/immutable or whether the variable should outlive the current scope, then I think additional syntax becomes necessary.
</p>
</div>
</div>

<div id="outline-container-org7a6fb5c" class="outline-2">
<h2 id="org7a6fb5c">Typed, ASCII Version</h2>
<div class="outline-text-2" id="text-org7a6fb5c">
<p>
My knowledge of the (experimental) Jai language may be out of date, but at one point I think it used what I would call a "separated" syntax for declaration and assignment, which looked like this:
</p>
<div class="org-src-container">
<pre class="src src-jai">w : int;      /* declare w, explicitly annotate type */
x : int = 10; /* declare x, explicitly annotate type, and assign at the same time */
y := 42;      /* declare+assign y at the same time, but this time infer the type */
z = 10;       /* assign only, an error unless z was declared previously */
</pre>
</div>

<p>
I think this offers the programmer a good spectrum of choices between type inference and explicit type annotations.
I also appreciate the visual distinctiveness that separates declaration from assignment, and the symmetry between <code>name : type = value</code> and <code>name := value</code>.
</p>

<p>
You can imagine extensions to this that provide the compiler more information or give the programmer more flexibility:
</p>
<div class="org-src-container">
<pre class="src src-jai">a : const int = 10; /* add "const" keyword */
b : arc int = 10; /* add keyword to denote the variable will be automatically reference counted */
</pre>
</div>

<p>
For a language with optional type annotations, this is a nice solution.
</p>

<p>
As an aside, Haskell lets you write type annotations on a separate line from the function you're annotating, which can sometimes really help with readability.
You can often omit the type annotation entirely, which of course is as minimal as you can get in terms of type annotations.
It looks like this:
</p>
<div class="org-src-container">
<pre class="src src-haskell">foo :: Int -&gt; Int
foo x = x + 1
</pre>
</div>

<p>
But for the purposes of discussing mutable variable syntax, Haskell is no help, because its values are <i>all</i> immutable.
</p>
</div>
</div>

<div id="outline-container-orgabe32e7" class="outline-2">
<h2 id="orgabe32e7">Summary</h2>
<div class="outline-text-2" id="text-orgabe32e7">
<p>
I think new languages should use <i>separate</i> syntax to distinguish between creating a new variable and updating an existing one.
It transforms a very common class of bugs (typos) into syntax errors, rather than silent runtime errors waiting to ruin your day.
</p>
</div>
</div>
</article>
    </main>

    <footer>
      <div class="license-container">
        <div class="license-item">Designed in 2024 by <a href="https://alexshroyer.com">Alex Shroyer</a></div>
        <div class="license-item"><a href="https://www.gnu.org/licenses/gpl-3.0.html">GNU GPLv3</a> (code)</div>
        <div class="license-item"><a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC-BY-4.0</a> (prose)</div>
      </div>
      <div class="social">
        <a href="https://stackoverflow.com/users/2037637/alex-shroyer" aria-label="my StackOverflow profile">
          <svg role="img" aria-hidden="true" class="svg-icon iconLogoGlyphMd native" width="32" height="37" viewBox="0 0 32 37">
            <path d="M26 33v-9h4v13H0V24h4v9h22z"></path>
            <path d="m21.5 0-2.7 2 9.9 13.3 2.7-2L21.5 0zM26 18.4 13.3 7.8l2.1-2.5 12.7 10.6-2.1 2.5zM9.1 15.2l15 7 1.4-3-15-7-1.4 3zm14 10.79.68-2.95-16.1-3.35L7 23l16.1 2.99zM23 30H7v-3h16v3z"></path>
          </svg>
        </a>
        <a href="https://github.com/hoosieree" aria-label="my GitHub profile">
          <svg role="img" height="32" aria-hidden="true" viewBox="0 0 16 16" version="1.1" width="32" data-view-component="true" class="octicon octicon-mark-github v-align-middle">
            <path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0016 8c0-4.42-3.58-8-8-8z"></path>
          </svg>
        </a>
        <a href="https://www.linkedin.com/in/alex-shroyer" aria-label="my LinkedIn profile"><svg role="img" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34" class="global-nav__logo">
            <g>
              <path d="M34,2.5v29A2.5,2.5,0,0,1,31.5,34H2.5A2.5,2.5,0,0,1,0,31.5V2.5A2.5,2.5,0,0,1,2.5,0h29A2.5,2.5,0,0,1,34,2.5ZM10,13H5V29h5Zm.45-5.5A2.88,2.88,0,0,0,7.59,4.6H7.5a2.9,2.9,0,0,0,0,5.8h0a2.88,2.88,0,0,0,2.95-2.81ZM29,19.28c0-4.81-3.06-6.68-6.1-6.68a5.7,5.7,0,0,0-5.06,2.58H17.7V13H13V29h5V20.49a3.32,3.32,0,0,1,3-3.58h.19c1.59,0,2.77,1,2.77,3.52V29h5Z" > </path>
            </g>
          </svg>
        </a>
      </div>
    </footer>
    <script>
      window.MathJax={
  tex:{ams:{multlineWidth:'85%'},tags:'ams',tagSide:'right',tagIndent:'.8em'},
  chtml:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  svg:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  output:{font:'mathjax-modern',displayOverflow:'overflow'}}
    </script>
  </body>
</html>
