<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="research and hobbies of a computer engineer">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/main.css">
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <title></title>
    <link rel="canonical" href="alexshroyer.com/2018-06-01-J-is-Readable.html">
  </head>
  <body>
    <header>
      <nav>
        <a href="/index.html">Home</a>
        <a href="/pages/cv.html">CV</a>
        <a href="/pages/projects.html">Projects</a>
        <a href="/rss.xml">RSS</a>
      </nav>
    </header>
    <main>
      <article><h1 class="title">J is Readable</h1><div class="abstract" id="org37b3d4f">
<p>
What makes some code readable and other code unreadable? I'll argue that readability, at least when it comes to source code, is not so much about how the code is arranged (in folders, on the page) or how it looks (whitespace, indentation, naming conventions). Instead, readability hinges on how easy it is to discover and modify the behavior of the running software.
</p>

</div>

<div id="outline-container-org1574d6e" class="outline-2">
<h2 id="org1574d6e"></h2>
<div class="outline-text-2" id="text-org1574d6e">
<p>
Code is not poetry, prose, or a recipe in a cookbook. Source code is more like sheet music. For musicians, reading a page of sheet music pales in comparison to playing the piece, or listening to it. For programmers, merely reading source code is far less worthwhile than interacting with the running program while poking and prodding the source to see what happens.
</p>

<p>
Based on this definition, what makes code readable? My answer: minimal barriers to interacting with the running program, modifying its source, and inspecting what makes it tick. Notice that this answer contains nothing about naming conventions, or the correct length of a function. Nothing about indentation or whitespace around punctuation. No comment about comments. To me, reading code is synonymous with discovering its behavior, and style considerations only come into play when they distract or mislead away from that goal.
</p>

<p>
What makes J readable? Several design decisions stand out.
</p>
</div>
</div>

<div id="outline-container-orgdda30ec" class="outline-2">
<h2 id="orgdda30ec">Interactive Session</h2>
<div class="outline-text-2" id="text-orgdda30ec">
<p>
J is an interpreted language, and the way I tend to write code in J is by incrementally building a transformation of some data until it works, then testing with more varied inputs until it's robust. Sometimes I'll benchmark the code and try to make it faster. Here's an example session (user-entered lines are indented, interpreter responses are unindented):
</p>

<div class="org-src-container">
<pre class="src src-j">   par =: 'this is (a string (with some (nested) parentheses))'
   '()'i.par
2 2 2 2 2 2 2 2 0 2 2 2 2 2 2 2 2 2 0 2 2 2 2 2 2 2 2 2 2 0 2 2 2 2 2 2 1 2 2 2 2 2 2 2 2 2 2 2 2 1 1
   1 _1 0{~'()'i.par
0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 _1 0 0 0 0 0 0 0 0 0 0 0 0 _1 _1
   +/\1 _1 0{~'()'i.par
0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 2 2 2 2 2 2 2 2 2 2 2 3 3 3 3 3 3 3 2 2 2 2 2 2 2 2 2 2 2 2 2 1 0
   ,":"0+/\1 _1 0{~'()'i.par
000000001111111111222222222223333333222222222222210
   par,:,":"0+/\1 _1 0{~'()'i.par
this is (a string (with some (nested) parentheses))
000000001111111111222222222223333333222222222222210
</pre>
</div>
<p>
What's happening here? First, I define some data that I want to work on. I assign a string to a variable: <code>par =: 'this is (a string (with some (nested) parentheses))'</code>. My goal is to determine the nesting depth of its parentheses.
</p>

<p>
Next, I use dyadic <code>i.</code> (index-of) to get the locations of each type of paren:
</p>

<div class="org-src-container">
<pre class="src src-j">   '()'i.par
2 2 2 2 2 2 2 2 0 2 2 2 2 2 2 2 2 2 0 2 2 2 2 2 2 2 2 2 2 0 2 2 2 2 2 2 1 2 2 2 2 2 2 2 2 2 2 2 2 1 1
</pre>
</div>
<p>
For each element in the right argument, it returns the index of the match in the left argument.
As an aside, it might seem odd for index-of to return 2, especially if you're used to something like C or JS where similar functions usually return -1 to signify the element was not found.
In J, negative numbers index into arrays starting at the end, so <code>_1{4 5 6</code> is 6.
Other languages might throw an exception in this case or return a non-numeric sentinel value like nil or NaN. But in the next step we'll see that in J, returning the length of the array permits a cute behavior when composing a longer expression:
</p>
<div class="org-src-container">
<pre class="src src-j">   1 _1 0{~'()'i.par
0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 _1 0 0 0 0 0 0 0 0 0 0 0 0 _1 _1
</pre>
</div>
<p>
Here, since I know the results of the previous step will be an array containing only 0, 1, or 2, I can remap these values to something else using <code>{</code> (from).
This is like bracket-indexing in other languages, except that it accepts arrays as well as scalars.
For example <code>1{'hello'</code> is <code>'e'</code>, and <code>3 4 3{'hello'</code> is <code>'lol'</code>.
Since the selecting values are on the right side of { and the new mapping is on the left, I can use ~ (passive) to flip the arugments.
<code>a-~b</code> is the same as <code>b-a</code>.
Altogether, this results in a new array containing 1 for each open paren, negative 1 (written <code>_1</code> in J) for each close paren, and 0 otherwise.
In this format, we can do a sum-scan (<code>+/\</code>) to determine the nesting depth of each character:
</p>
<div class="org-src-container">
<pre class="src src-j">   +/\1 _1 0{~'()'i.par
0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 2 2 2 2 2 2 2 2 2 2 2 3 3 3 3 3 3 3 2 2 2 2 2 2 2 2 2 2 2 2 2 1 0
</pre>
</div>
<p>
Finally, I want to display this nesting depth alongside the s-expression string.
But since J arrays have to contain elements of the same type, I have to first convert the numeric array to a string using <code>":</code> (default format).
However, I don't want to get the default format of the array with all the spaces, but a compact array of numbers only, so I use the rank conjunction to tell default format to operate on the individual items (<code>"0</code>), and then ravel (<code>,</code>) all of these characters together to make a single string:
</p>
<div class="org-src-container">
<pre class="src src-j">   ,":"0+/\1 _1 0{~'()'i.par
000000001111111111222222222223333333222222222222210
</pre>
</div>
<p>
And finally, put it together with the s-expression string using <code>,:</code> (laminate).
</p>

<div class="org-src-container">
<pre class="src src-j">   par,:,":"0+/\1 _1 0{~'()'i.par
this is (a string (with some (nested) parentheses))
000000001111111111222222222223333333222222222222210
</pre>
</div>
<p>
Being able to build up an expression like this interactively allows for easy experimentation and keeps me in-tune with the data.
The fact that J is strictly interpreted isn't a benefit in itself (in fact, I'd prefer if J had the option of producing a compiled binary executable) but what's really beneficial is having the option of running code in a REPL session.
</p>
</div>
</div>

<div id="outline-container-orgbcbce3b" class="outline-2">
<h2 id="orgbcbce3b">Minimal Indirection</h2>
<div class="outline-text-2" id="text-orgbcbce3b">
<p>
The second major aid to readability is the fact that you can enter the name of a verb in the interpreter and see its definition:
</p>

<div class="org-src-container">
<pre class="src src-j">   fread
3 : 0
if. 1 = #y=. boxopen y do.
  1!:1 :: _1: fboxname y
else.
  1!:11 :: _1: (fboxname {.y),{:y
end.
:
x freads y
)
</pre>
</div>
<p>
Here, I typed the word <code>fread</code> and the interpreter printed its definition.
Inside the definition are more words like <code>boxopen</code> and <code>freads</code>, and I can repeat the process for those to find their definitions as well.
On occasion, you might find a word which is defined in some other namespace, so to see its definition takes another step, but it's still trivial compared to e.g. Python, where the standard library and your own user-supplied definitions are bytecode-compiled and essentially hidden from you as you use the interpreter.
It's almost like having ctags built in to the interpreter, but the mechanism behind it is much simpler - what you see is what's actually there.
The definition isn't bytecode-compiled, it's always interpreted.
</p>

<p>
Check out the standard library verb <code>nl</code> (short for "name list"), which displays all of the names in a particular locale:
</p>

<div class="org-src-container">
<pre class="src src-j">   'rx' nl_z_ verb
┌───┬─────┬──────┬─────┬────┬───────┬───────┬──────┬──────┬─────────┬────┬───────┬──────┬───────┬─────────┬──────┬──────┐
│rxE│rxall│rxcomp│rxcut│rxeq│rxerror│rxfirst│rxfree│rxfrom│rxhandles│rxin│rxindex│rxinfo│rxmatch│rxmatches│rxrplc│rxutf8│
└───┴─────┴──────┴─────┴────┴───────┴───────┴──────┴──────┴─────────┴────┴───────┴──────┴───────┴─────────┴──────┴──────┘
</pre>
</div>
<p>
There's kind of a lot going on here.
Since J evaluates from right to left, let's break it down in that order.
verb is a cover word in the standard library which evaluates to 3.
noun evaluates to 0, adverb evaluates to 1, and so on.
These are codes for the parts of speech used by the interpreter.
Next is <code>nl_z_</code> which is calling the verb nl in locale z.
This is the "lookup in namespace" step mentioned earlier.
Finally we see the left argument to <code>nl_z_</code> is the string 'rx'.
This tells <code>nl</code> to return only those names which start with "rx", and we can see that in the result.
</p>

<p>
Other words like <code>load</code> and <code>hfd</code> (hex from decimal) are in the standard library, rather than built into the language as keywords.
This makes it easy to peek into the definitions of library verbs you may already be using, and perhaps extract parts of them for some similar-but-slightly-different purpose of your own.
</p>

<p>
For me, these aspects of J's design make for a pleasant development experience, without the need for powerful IDEs or editor plugins.
A plain text editor and a terminal are all I need.
</p>
</div>
</div>
</article>
    </main>

    <footer>
      <div class="license-container">
        <div class="license-item">Designed in 2024 by <a href="https://alexshroyer.com">Alex Shroyer</a></div>
        <div class="license-item"><a href="https://www.gnu.org/licenses/gpl-3.0.html">GNU GPLv3</a> (code)</div>
        <div class="license-item"><a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC-BY-4.0</a> (prose)</div>
      </div>
      <div class="social">
        <a href="https://stackoverflow.com/users/2037637/alex-shroyer" aria-label="my StackOverflow profile">
          <svg role="img" aria-hidden="true" class="svg-icon iconLogoGlyphMd native" width="32" height="37" viewBox="0 0 32 37">
            <path d="M26 33v-9h4v13H0V24h4v9h22z"></path>
            <path d="m21.5 0-2.7 2 9.9 13.3 2.7-2L21.5 0zM26 18.4 13.3 7.8l2.1-2.5 12.7 10.6-2.1 2.5zM9.1 15.2l15 7 1.4-3-15-7-1.4 3zm14 10.79.68-2.95-16.1-3.35L7 23l16.1 2.99zM23 30H7v-3h16v3z"></path>
          </svg>
        </a>
        <a href="https://github.com/hoosieree" aria-label="my GitHub profile">
          <svg role="img" height="32" aria-hidden="true" viewBox="0 0 16 16" version="1.1" width="32" data-view-component="true" class="octicon octicon-mark-github v-align-middle">
            <path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0016 8c0-4.42-3.58-8-8-8z"></path>
          </svg>
        </a>
        <a href="https://www.linkedin.com/in/alex-shroyer" aria-label="my LinkedIn profile"><svg role="img" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34" class="global-nav__logo">
            <g>
              <path d="M34,2.5v29A2.5,2.5,0,0,1,31.5,34H2.5A2.5,2.5,0,0,1,0,31.5V2.5A2.5,2.5,0,0,1,2.5,0h29A2.5,2.5,0,0,1,34,2.5ZM10,13H5V29h5Zm.45-5.5A2.88,2.88,0,0,0,7.59,4.6H7.5a2.9,2.9,0,0,0,0,5.8h0a2.88,2.88,0,0,0,2.95-2.81ZM29,19.28c0-4.81-3.06-6.68-6.1-6.68a5.7,5.7,0,0,0-5.06,2.58H17.7V13H13V29h5V20.49a3.32,3.32,0,0,1,3-3.58h.19c1.59,0,2.77,1,2.77,3.52V29h5Z" > </path>
            </g>
          </svg>
        </a>
      </div>
    </footer>
    <script>
      window.MathJax={
  tex:{ams:{multlineWidth:'85%'},tags:'ams',tagSide:'right',tagIndent:'.8em'},
  chtml:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  svg:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  output:{font:'mathjax-modern',displayOverflow:'overflow'}}
    </script>
  </body>
</html>
