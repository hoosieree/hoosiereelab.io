<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="research and hobbies of a computer engineer">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/main.css">
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <title></title>
    <link rel="canonical" href="alexshroyer.com/2023-01-01-Low-Impedance-Pickup.html">
  </head>
  <body>
    <header>
      <nav>
        <a href="/index.html">Home</a>
        <a href="/pages/cv.html">CV</a>
        <a href="/pages/projects.html">Projects</a>
        <a href="/rss.xml">RSS</a>
      </nav>
    </header>
    <main>
      <article><h1 class="title">Low Impedance Pickup</h1><div class="abstract" id="org10c3ee7">
<p>
Building a low impedance guitar pickup prototype.
</p>

</div>

<div id="outline-container-orga82e0df" class="outline-2">
<h2 id="orga82e0df">Pickup Types</h2>
<div class="outline-text-2" id="text-orga82e0df">
<p>
Electric guitars have a pickup to convert the motion of the strings to an electrical signal.
The most common type of pickup consists of thousands of turns of fine wire around a magnetic core.
The purpose of the magnet is to magnetize the steel strings, so that when they vibrate they induce currents in the coil.
You can find equations for inductors (coils of wire) to find out what number of turns and core material result in what inductance value, but in simple terms, more turns produce more output.
</p>

<p>
The large number of turns makes the output impedance of this kind of guitar pickup high compared to other audio equipment.
"High" output impedance roughly equivalent to having a high voltage to current ratio.
To achieve a good signal transfer with minimal noise, a guitar pickup should be plugged into a very high impedance input.
</p>

<p>
To get a sense of the numbers associated with "high" and "low" in this context, a normal guitar pickup may have an output resistance (sort of like impedance, but easier to measure) around 7000 ohms (7k).
A "high-output" pickup might have something like 12k-18k ohms of output resistance, and be capable of delivering higher output voltages for the same amount of string vibrating.
</p>

<p>
Guitar amplifier inputs, built with this kind of guitar pickup in mind, have input resistance values around 100k to 1 million ohms.
As with other musical things, this is not an iron law and some amplifiers or effects have much lower input resistances, down to around 1k-10k for some fuzz effects.
Such low values "load" a high-impedance pickup in a way that colors the sound.
For intentional distortion effects like fuzz, this is beneficial because it pre-filters the sound in a nice-sounding way, generally by reducing high frequency energy.
</p>

<p>
This general rule of high-impedance pickups and even-higher-impedance amplifier inputs is the way electric guitars and their amplifiers have been built for decades, and is a part of their characteristic tonal qualities.
</p>
</div>
</div>

<div id="outline-container-orge597765" class="outline-2">
<h2 id="orge597765">Low Impedance Pickups</h2>
<div class="outline-text-2" id="text-orge597765">
<p>
But thousands of turns of fine wire is just one way to make a pickup.
We can also make a pickup using the same basic principles, but with few turns of heavy wire.
Or taken to the limit, a single turn of heavy wire.
</p>

<p>
That's the basic idea used in the designs discussed in this forum discussion<sup><a id="fnr.1" class="footref" href="#fn.1" role="doc-backlink">1</a></sup>, and also the basis for the Lace Music Products' Alumitone pickup model<sup><a id="fnr.2" class="footref" href="#fn.2" role="doc-backlink">2</a></sup>.
</p>
</div>
</div>

<div id="outline-container-org4df962a" class="outline-2">
<h2 id="org4df962a">The Prototype</h2>
<div class="outline-text-2" id="text-org4df962a">
<p>
After thinking about the various designs in the forum thread, I went with a design that optimizes ease of construction with a limited set of tools.
</p>


<div id="orgc7d6f44" class="figure">
<p><img src="../images/pup-assembled-top.jpg" alt="pup-assembled-top.jpg">
</p>
<p><span class="figure-number">Figure 1: </span>Prototype</p>
</div>

<p>
The body is made from two pieces of 2 inch wide aluminum plate.
I cut 2 slots into the top, and inserted steel "rails" in the slots, covered in black electrical tape and taped into position on the underside of the pickup.
</p>


<div id="org6cde846" class="figure">
<p><img src="../images/pup-assembled-bottom.jpg" alt="pup-assembled-bottom.jpg">
</p>
<p><span class="figure-number">Figure 2: </span>Bottom Side</p>
</div>

<p>
The rails guide the magnetic field through the slots so the magnets can be mounted on the underside.
This lets the pickup be mounted closer to the strings without the magnetic field being so strong that it interferes with the string vibration (also known as "wolf tones").
</p>

<p>
The top plate with the slots provides a two paths for current to flow, and by orienting one row of magnets North-South and the other row South-North, this should encourage the current to flow clockwise in one path and counterclockwise in the other path.
The center strip is wider, because both the clockwise and counterclockwise currents flow through this same path and should sum together, so I guessed that they should have a lower resistance path.
This orientation of magnets and coil direction, and using their sum, is known as a "humbucker" pickup, because two pickups in close proximity with opposite phase reject common-mode noise like hum from power electronics.
</p>

<p>
This center path continues through a toroidal current transformer<sup><a id="fnr.3" class="footref" href="#fn.3" role="doc-backlink">3</a></sup> with a 1000:1 ratio and a DC resistance of 41.80 ohms.
Three aluminum rods connect the top plate to a bottom plate, and I treated these as rivets by hammering them into place so the rod material expanded in the drilled holes on the top and bottom plates.
To prevent this hammering from damaging the current transformer, I turned a "shoulder" rivet on a makeshift lathe, using a Dremel to grind material away as the rod spun in the drill chuck.
This is a terrible way to make a lathe, but the second try produced adequate results.
</p>


<div id="org279b141" class="figure">
<p><img src="../images/bad-lathe.png" alt="bad-lathe.png">
</p>
<p><span class="figure-number">Figure 3: </span>Cursed Lathe</p>
</div>
</div>
</div>

<div id="outline-container-org046bf07" class="outline-2">
<h2 id="org046bf07">SCAD Model</h2>
<div class="outline-text-2" id="text-org046bf07">
<p>
Over the holidays I learned just enough OpenSCAD to produce this model, which I then printed out to scale and traced onto some plate aluminum to make the flat components.
</p>


<div id="orgd668fde" class="figure">
<p><img src="../images/pup-exploded.png" alt="pup-exploded.png">
</p>
<p><span class="figure-number">Figure 4: </span>Exploded View</p>
</div>

<p>
Overall I'm happy with using OpenSCAD for this sort of thing.
For some reason, creating a model with code feels more precise than using something like SketchUp or Fusion360.
</p>


<div class="org-src-container">
<pre class="src src-scad">// units: mm
$fn = 10; //larger number for more fine radii
centers = [0,10.30,20.69,31.23,42.02,53.09,64.46,76.24]; //string centers
dmag = mm(1/4)+1;
drivet = mm(3/8);
r1 = mm(3/16); // small rivet
e = 0.02; //epsilon
width = 84.24; //bridge width
length = mm(2); //2 inch wide plate
thickness = 3.175;
border = 30;

function mm(inches) = inches * 25.4;

module magnet_cuts(){
 for(j=[-1,1]){
  for(i=centers){
   translate([-width/2+dmag/2+i,j*length/4,0])
    // cylinder(h=thickness+e,d=dmag,center=true);
    rotate(45) cube([3,3,thickness+e],center=true); //diamonds for alignment
  }
  translate([centers[1]*1,j*length/4,0])
   cube([width+centers[1],1,thickness+e],center=true);
 }
}

module rivet_holes(h){
 for(i=[-1,0,1]){
  translate([width*0.56,i*(length/3+3),h])
   cylinder(h=thickness+e,d=r1,center=true);
 }
}

module upper(){
 difference(){
  cube([width+border,length,thickness],center=true);
  magnet_cuts();
  rivet_holes(0);
 }
}

module lower(spacing){
 difference(){
  translate([width-border-7,0,spacing])
   cube([20,length,thickness],center=true);
  rivet_holes(spacing);
 }
}

module ac1005(){
 d1 = 23.8;
 d2 = 9.8;
 d3 = (d1-15.24);
 h = 11.12;
 wl = 8;
 difference(){
  union(){
   cylinder(d=d1,h=h,center=true);
   translate([0,d1/4,0]) cube([d1,d1/2,h],center=true);
   translate([0,15,h/2-1.75]) rotate([90,0,0]) cylinder(d=1,h=wl,center=true);
   translate([0,d1/2,h/2-1.75]) rotate([90,0,0]) cylinder(d=3,h=1,center=true);
   translate([d3,15,-h/2+1.75]) rotate([90,0,0]) cylinder(d=1,h=wl,center=true);
   translate([-d3,15,-h/2+1.75]) rotate([90,0,0]) cylinder(d=1,h=wl,center=true);
   translate([d3-1,d1/2,0]) cylinder(d=1,h=h,center=true);
   translate([-d3+1,d1/2,0]) cylinder(d=1,h=h,center=true);
  }
  cylinder(d=d2,h=h+e,center=true);
 }
}

module rivet(h){
 translate([width/2+5,0,h])
  union(){
  cylinder(d=r1,h=mm(1/4)+12,center=true);
  cylinder(d=drivet,h=12,center=true);
 }
 for(i=[-1,1]){
  translate([width/2+5,i*(length/3+3),h])
   cylinder(d=r1,h=mm(1/4)+12,center=true);
 }
}

module main(explode,plates){
 h2 = explode ? -30 : -mm(1/4)-1;
 h1 = explode ? -40 : h2*2;
 h3 = explode ? -13 : -7.4;
 union(){

  // Display only the upper and lower plates, and use projection()
  // and export as PDF, then print to scale.
  upper();
  translate([plates?21:0,0,0]) lower(h1);

  // Uncommment these to see more components:
  // translate([plates?38:0,0,0]) rivet(h3);
  // translate([plates?64:0,0,0])
  //  translate([width-border-7,0,h2])
  //   rotate(90) ac1005();
 }
}
projection() // comment this line for the 3d view
translate([-width/2,-length,0]) rotate(90) main(false,true);
</pre>
</div>
</div>
</div>

<div id="outline-container-org6ab2796" class="outline-2">
<h2 id="org6ab2796">Sound Test</h2>
<div class="outline-text-2" id="text-org6ab2796">
<p>
A quick sound test, with first ceramic and then neodymium magnets, demonstrates basic functionality.
It seems to be about half the loudness of a P90 pickup, which is not great.
On the other hand, the noise seems to be so low as to be inaudible above the hiss of the amplifier.
Handling noise from the pickup is about the same as a P90.
</p>
</div>
</div>

<div id="outline-container-orge87f6c3" class="outline-2">
<h2 id="orge87f6c3">Next Steps</h2>
<div class="outline-text-2" id="text-orge87f6c3">
<p>
There are some design choices that I will do differently next time.
First, instead of using the relatively large current transformer with the 3/8 inch bore, I will use two or more smaller current transformers<sup><a id="fnr.4" class="footref" href="#fn.4" role="doc-backlink">4</a></sup>.
This will not only allow for more wiring options (e.g. adding in series or parallel), but also potentially more output.
Also, the 3-rivet design has a flaw in the sense that I can't tell the quality of the connection of the outside "legs".
It's possible that the output from one coil is much higher than the other, or even that one coil simply does not work.
All I know for sure is that there is a path for current to flow through at least one of the coils.
I do like this current transformer based approach in general, so I think the next revision will still use the same basic idea.
</p>

<p>
Instead of a slotted design with 3 connecting rods, I would like to try a design with just 2 connecting rods.
This would make the quality of the electrical connection of the rivets more apparent.
</p>

<p>
A more involved next step is to mount the pickup more permanently in a guitar to allow for more extensive testing, or at least to allow me to actually play the guitar normally and without the pickup falling off.
</p>
</div>
</div>
<div id="footnotes">
<h2 class="footnotes">Footnotes: </h2>
<div id="text-footnotes">

<div class="footdef"><sup><a id="fn.1" class="footnum" href="#fnr.1" role="doc-backlink">1</a></sup> <div class="footpara" role="doc-footnote"><p class="footpara">
A <a href="https://music-electronics-forum.com/forum/instrumentation/pickup-makers/5622-low-impedance-pickup-research">Music Electronics Forum</a> post with over 30 pages and lots of inspiring designs.
</p></div></div>

<div class="footdef"><sup><a id="fn.2" class="footnum" href="#fnr.2" role="doc-backlink">2</a></sup> <div class="footpara" role="doc-footnote"><p class="footpara">
The <a href="https://lacemusic.com/collections/7/products/alumitone-single-coil-in-chrome-or-gold">Lace Alumitone Pickup</a> line are all humbucking, even in the "single coil" shape.
</p></div></div>

<div class="footdef"><sup><a id="fn.3" class="footnum" href="#fnr.3" role="doc-backlink">3</a></sup> <div class="footpara" role="doc-footnote"><p class="footpara">
<a href="https://talema.com/wp-content/uploads/datasheets/AC.pdf">Talema AC1005 Current Transformer</a>, the smallest of its family.
</p></div></div>

<div class="footdef"><sup><a id="fn.4" class="footnum" href="#fnr.4" role="doc-backlink">4</a></sup> <div class="footpara" role="doc-footnote"><p class="footpara">
<a href="https://talema.com/wp-content/uploads/datasheets/AP.pdf">Talema AP1000 Current Transformer</a>, an even smaller current transformer.
</p></div></div>


</div>
</div>
</article>
    </main>

    <footer>
      <div class="license-container">
        <div class="license-item">Designed in 2024 by <a href="https://alexshroyer.com">Alex Shroyer</a></div>
        <div class="license-item"><a href="https://www.gnu.org/licenses/gpl-3.0.html">GNU GPLv3</a> (code)</div>
        <div class="license-item"><a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC-BY-4.0</a> (prose)</div>
      </div>
      <div class="social">
        <a href="https://stackoverflow.com/users/2037637/alex-shroyer" aria-label="my StackOverflow profile">
          <svg role="img" aria-hidden="true" class="svg-icon iconLogoGlyphMd native" width="32" height="37" viewBox="0 0 32 37">
            <path d="M26 33v-9h4v13H0V24h4v9h22z"></path>
            <path d="m21.5 0-2.7 2 9.9 13.3 2.7-2L21.5 0zM26 18.4 13.3 7.8l2.1-2.5 12.7 10.6-2.1 2.5zM9.1 15.2l15 7 1.4-3-15-7-1.4 3zm14 10.79.68-2.95-16.1-3.35L7 23l16.1 2.99zM23 30H7v-3h16v3z"></path>
          </svg>
        </a>
        <a href="https://github.com/hoosieree" aria-label="my GitHub profile">
          <svg role="img" height="32" aria-hidden="true" viewBox="0 0 16 16" version="1.1" width="32" data-view-component="true" class="octicon octicon-mark-github v-align-middle">
            <path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0016 8c0-4.42-3.58-8-8-8z"></path>
          </svg>
        </a>
        <a href="https://www.linkedin.com/in/alex-shroyer" aria-label="my LinkedIn profile"><svg role="img" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34" class="global-nav__logo">
            <g>
              <path d="M34,2.5v29A2.5,2.5,0,0,1,31.5,34H2.5A2.5,2.5,0,0,1,0,31.5V2.5A2.5,2.5,0,0,1,2.5,0h29A2.5,2.5,0,0,1,34,2.5ZM10,13H5V29h5Zm.45-5.5A2.88,2.88,0,0,0,7.59,4.6H7.5a2.9,2.9,0,0,0,0,5.8h0a2.88,2.88,0,0,0,2.95-2.81ZM29,19.28c0-4.81-3.06-6.68-6.1-6.68a5.7,5.7,0,0,0-5.06,2.58H17.7V13H13V29h5V20.49a3.32,3.32,0,0,1,3-3.58h.19c1.59,0,2.77,1,2.77,3.52V29h5Z" > </path>
            </g>
          </svg>
        </a>
      </div>
    </footer>
    <script>
      window.MathJax={
  tex:{ams:{multlineWidth:'85%'},tags:'ams',tagSide:'right',tagIndent:'.8em'},
  chtml:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  svg:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  output:{font:'mathjax-modern',displayOverflow:'overflow'}}
    </script>
  </body>
</html>
