<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="research and hobbies of a computer engineer">
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/main.css">
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <title></title>
    <link rel="canonical" href="alexshroyer.com/2022-07-11-Obviously-Better.html">
  </head>
  <body>
    <header>
      <nav>
        <a href="/index.html">Home</a>
        <a href="/pages/cv.html">CV</a>
        <a href="/pages/projects.html">Projects</a>
        <a href="/rss.xml">RSS</a>
      </nav>
    </header>
    <main>
      <article><h1 class="title">Obviously Better</h1><div class="abstract" id="org00d0b58">
<p>
An idea which is "obviously better" to some people and yet obviously worse to others.
</p>

</div>

<p>
On the most recent <a href="https://www.arraycast.com/episodes/episode31-jeremy-howard">episode</a> of ArrayCast, host Conor recalled a strong reaction from peers to his observation about operator precedence in APL:
</p>

<blockquote>
<p>
I just made this offhand remark that&#x2026; the evaluation order in APL is a much simpler model than what we learned in school, &#x2026;it was the most controversial thing
</p>
</blockquote>

<p>
Here's the thing - there is no natural or obvious precedence to mathematical operators, functions, or any other programming notation.
There are some conventions which are more common across domains than others, such as <a href="https://www.mathsisfun.com/operation-order-pemdas.html">PEMDAS/BODMAS</a>.
These conventions can be memorized, but I think this does more harm than good.
</p>

<p>
The key problem with adopting these conventions in programming languages is that most programming revolves around the act of abstraction - creating new terminology to address a new problem.
This is so central to programming that it's facilitated by keywords like <code>import</code> or <code>#include</code>.
These keywords are roughly equivalent to a kind of copy-paste to incorporate other code from some other source into a new context.
So programming languages are not only creating new vocabulary regularly, but their vocabularies tend to grow ever larger with inclusion of additional code via import/include mechanisms.
Because programming involves abstraction, conventions about order of operations or operator precedence do not scale.
</p>

<p>
Most programming languages draw a line in the sand.
On one side of the line you have operators with special precedence rules.
On the other side of the line: user defined functions.
</p>

<p>
Different kinds of functions (asynchronous or not, effectful or not, etc.) have wildly different semantics, yet in most programming languages they can't be distinguished by their syntax alone.
It might make sense to have different precedence rules for these different types of functions.
But you might see the flaw in this logic - there are too many different kinds of functions to justify learning a new rule for each new type.
</p>

<p>
Then why do we tolerate precedence rules for arithmetic operators?
</p>

<div id="outline-container-org044c36e" class="outline-2">
<h2 id="org044c36e">PEMDAS only</h2>
<div class="outline-text-2" id="text-org044c36e">
<p>
I don't have a problem with higher precedence (in order) for addition, multiplication, and exponentiation.
But I think it's healthy to admit that the reason behind this ordering is to reduce parentheses and make written expressions easier to read (after you've memorized the rules).
</p>

<p>
This is very true when writing arithmetic expressions by hand.
It's much easier to write <code>(ab + cd)</code> than a fully-parenthesized <code>((a*b) + (c*d))</code> or lisp-style prefix notation like <code>(+ (* a b) (* c d))</code>.
I seem to find infix clearer than postfix: <code>a b * c d * +</code>, although this is probably just my own upbringing and bias talking.
</p>

<p>
But the problem is what to do with the other operators.
In advanced math papers, authors define new notation all the time.
The only requirement is that it's readable by their peers, or at least the people reviewing the work for publication.
In programming languages, the problem is slightly worse because languages tend to be designed by a few people but used by many more people.
This means that all the users of the language must learn whatever rules the designers thought made sense.
</p>

<p>
And - surprise surprise - different language designers have different ideas about what makes sense.
So learning the arbitrary rules for one language may or may not help you when you need to learn another language.
</p>

<p>
Maybe this is some of the appeal of lisp-family languages.
They basically don't draw a line in the sand.
Instead, the only syntax to learn is parentheses.
But I suspect this is also part of why other people dislike reading or writing lisp code.
Because there are only parentheses, there's basically no syntax to guide your eye toward the meaning of the code.
Instead you build the parse tree in your mind and keep track of functions, macros, and variables.
</p>
</div>
</div>

<div id="outline-container-org692b90d" class="outline-2">
<h2 id="org692b90d">How APL works</h2>
<div class="outline-text-2" id="text-org692b90d">
<p>
In most APL-family languages, evaluation proceeds from right to left.
So an expression like <code>(ab + cd)</code> from earlier would be written (in J) as <code>(a*b) + c * d</code>.
This expression evaluates as <code>(a*b) + (c*d)</code>.
The parentheses around <code>(a*b)</code> are required because without them the expression would evaluate as <code>a * (b + c * d)</code>.
</p>

<p>
Here's a similar example of how evaluation proceeds in J, this time swapping <code>+</code> and <code>*</code>:
</p>

<pre class="example">
(a+b) * c + d
</pre>


<p>
This evaluates as:
</p>

<pre class="example">
(a+b) * (c+d)
</pre>


<p>
Without parentheses, it would evaluate as
</p>

<pre class="example">
a + (b * (c + d))
</pre>


<p>
The beauty of this evaluation order is that (like lisps and forths) the number of rules you need to learn is minimized.
Also like lisps and forths, it extends seamlessly to other operators (not just the ones described by PEMDAS) or even user-defined operators.
I don't have any evidence that infix notation is inherently better than prefix or suffix notation, but anecdotally I enjoy the fact that infix expressions map more closely to how I say the expressions out loud in English: "a plus b times c plus d" is still ambiguous, but for me it's easier to understand than the unambiguous "a b plus c d plus times".
</p>

<p>
And like in APL or J, we can verbally add parentheses to disambiguate: "paren a plus b paren times c plus d".
Of course this falls apart when the expressions grow, but hopefully you aren't relying solely on verbal communication for conveying complex math.
</p>
</div>
</div>

<div id="outline-container-org6bc12fc" class="outline-2">
<h2 id="org6bc12fc">Why we hold on to PEMDAS</h2>
<div class="outline-text-2" id="text-org6bc12fc">
<p>
As the Array Cast discussion hinted, perhaps most grown-ups simply dislike being asked to unlearn something which they spent a lot of time and effort learning.
That's completely understandable.
But I hope people can at least consider opinions that run counter to what they already know.
If you're a Python programmer, or a C programmer, and you've never tried a language in any of the APL, lisp, or forth varieties, then maybe write some toy program in one of them just to get a feel for the language.
The hardest part will be remembering what it was like to learn Python or C for the first time in order to fairly judge which language family was easier to learn.
</p>
</div>
</div>
</article>
    </main>

    <footer>
      <div class="license-container">
        <div class="license-item">Designed in 2024 by <a href="https://alexshroyer.com">Alex Shroyer</a></div>
        <div class="license-item"><a href="https://www.gnu.org/licenses/gpl-3.0.html">GNU GPLv3</a> (code)</div>
        <div class="license-item"><a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC-BY-4.0</a> (prose)</div>
      </div>
      <div class="social">
        <a href="https://stackoverflow.com/users/2037637/alex-shroyer" aria-label="my StackOverflow profile">
          <svg role="img" aria-hidden="true" class="svg-icon iconLogoGlyphMd native" width="32" height="37" viewBox="0 0 32 37">
            <path d="M26 33v-9h4v13H0V24h4v9h22z"></path>
            <path d="m21.5 0-2.7 2 9.9 13.3 2.7-2L21.5 0zM26 18.4 13.3 7.8l2.1-2.5 12.7 10.6-2.1 2.5zM9.1 15.2l15 7 1.4-3-15-7-1.4 3zm14 10.79.68-2.95-16.1-3.35L7 23l16.1 2.99zM23 30H7v-3h16v3z"></path>
          </svg>
        </a>
        <a href="https://github.com/hoosieree" aria-label="my GitHub profile">
          <svg role="img" height="32" aria-hidden="true" viewBox="0 0 16 16" version="1.1" width="32" data-view-component="true" class="octicon octicon-mark-github v-align-middle">
            <path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0016 8c0-4.42-3.58-8-8-8z"></path>
          </svg>
        </a>
        <a href="https://www.linkedin.com/in/alex-shroyer" aria-label="my LinkedIn profile"><svg role="img" xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34" class="global-nav__logo">
            <g>
              <path d="M34,2.5v29A2.5,2.5,0,0,1,31.5,34H2.5A2.5,2.5,0,0,1,0,31.5V2.5A2.5,2.5,0,0,1,2.5,0h29A2.5,2.5,0,0,1,34,2.5ZM10,13H5V29h5Zm.45-5.5A2.88,2.88,0,0,0,7.59,4.6H7.5a2.9,2.9,0,0,0,0,5.8h0a2.88,2.88,0,0,0,2.95-2.81ZM29,19.28c0-4.81-3.06-6.68-6.1-6.68a5.7,5.7,0,0,0-5.06,2.58H17.7V13H13V29h5V20.49a3.32,3.32,0,0,1,3-3.58h.19c1.59,0,2.77,1,2.77,3.52V29h5Z" > </path>
            </g>
          </svg>
        </a>
      </div>
    </footer>
    <script>
      window.MathJax={
  tex:{ams:{multlineWidth:'85%'},tags:'ams',tagSide:'right',tagIndent:'.8em'},
  chtml:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  svg:{scale:1.0,displayAlign:'center',displayIndent:'0em'},
  output:{font:'mathjax-modern',displayOverflow:'overflow'}}
    </script>
  </body>
</html>
