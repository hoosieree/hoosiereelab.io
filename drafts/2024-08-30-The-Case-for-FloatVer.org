#+title: The Case for Float Ver
#+date: 2024 08 30
#+filetags: floatver software design theory practice

#+begin_abstract
FloatVer is a modern software version system based on an old idea. This article makes the case for why you should use it instead of common alternatives.
#+end_abstract

* What is a Software Versioning System?
Software developers use a public-facing versioning system to communicate to users when something about their software has changed.
These changes might be bugfixes or patched security holes, additional features, or sometimes even the removal of old features.

Some norms have developed around version schemes - concepts like "breaking changes" are meaningful (a.k.a. /semantic/) to communicate to users that some feature they were using is different in this new version, which means the user has some work to do to accommodate the change.
By contrast, "non-breaking" changes convey to the user that they can (and /should/) apply the new version right away.
Their existing features will continue to work, and there may be new features or important security updates included with the non-breaking change, so the arguments against upgrading are weak.
Another norm is that older versions use smaller numbers than newer versions, or (if the version system is date-based) older versions use older dates.

* There are Many Version Systems
The most popular version system in use today is SemVer, typically in the form of =MAJOR.MINOR.PATCH= but sometimes with extra information added to the end.
Another popular version system is probably CalVer, a date-based system usually expressed as =YYYY.MM.DD= (4-digit year, 2-digit month and day) but which also includes other formats like =YY.0W= (2-digit year, zero-padded week).

While these systems are frequently used, they are not without problems.

** (Minor) Communication Problems
- When a person with a typical math education first encounters a "number" with more than one decimal point, it may be confusing.

This is a rather weak argument against SemVer or CalVer, because people in general are fast learners, and with a little bit of explanation they can learn to recognize something like =0.13.4= as a version number and no longer confuse it with something like a real or complex number.

- Is =1999.12.31= CalVer or SemVer?

Without more context it's not possible to tell.
Again this is not a strong argument against using either SemVer or CalVer, but it does warn against mixing the two systems, as the same number can mean different things.

- Is =0.9= bigger or smaller (or older/newer) than =0.10=?

In SemVer, =0.9.9 < 0.9.10=.
Significant trailing zeros in SemVer versions may be surprising to people expecting real number semantics (=9.9 > 9.10=).

** (Major) Communication Problems
